import { Component, ViewChild } from '@angular/core';
import { Global } from 'asap-crud';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Global]
})
export class AppComponent {

  @ViewChild('checkout',{static:true}) checkout;

  title = 'SandBox ASAP Components';

  picture = "https://material.angular.io/assets/img/examples/shiba2.jpg";


  constructor(public global: Global) {

    // Credenciais do ASTA
    let user = {"data":{"id":21028,"client":22368,"name":"Yuri Carvalho","email":"yuricarvalho48@gmail.com","role":"Usuario","client_id":"3","resource":null,"resource_sub":null,"resource_sub_down":null,"picture":null,"analytics":null,"extras":[{"value":""}],"groups":[],"created_at":{"date":"2019-07-11 00:03:33.000000","timezone_type":3,"timezone":"America/Sao_Paulo"},"link":"https://asta.twcreativs.stream/api/","gestor":false,"publication":false}};
    sessionStorage.setItem("token","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJlNmYyZTk2OWQwMTE3MWVlOTdlZGZjYTJjNWE4M2RlYmQ1YTg1N2EwNjI5ZDUwZWYxYTQwYmEwZjI3ZjZhYzYyNTExMmJjYjdhYjQyMzdjIn0.eyJhdWQiOiIxIiwianRpIjoiYmU2ZjJlOTY5ZDAxMTcxZWU5N2VkZmNhMmM1YTgzZGViZDVhODU3YTA2MjlkNTBlZjFhNDBiYTBmMjdmNmFjNjI1MTEyYmNiN2FiNDIzN2MiLCJpYXQiOjE1NjM5Nzc1NjQsIm5iZiI6MTU2Mzk3NzU2NCwiZXhwIjoxNTk1NTk5OTY0LCJzdWIiOiIyMTAyOCIsInNjb3BlcyI6W119.nVndtu3jq4ryUChmdhE-SoRxUYPlrvTNshg_w4ykdVn2REq2DCEHXciTPcXv7pJpEw5qHOYoKRtkKFoSdm4cf-37VBSJFlnwMBzD9oCbxcobOcGdF7yt-aVWOvIs8lQBOjbFwIJ9ke_l4EPsv0UYYXxntnTS1OsVlAp_6BPmnximgR4UuZgAsWShdIRrrWD9MyOlV6mCT8QgmQrHOriBRT48Qjjt7ReEIQLsINOmt5Q2vCPGGxRIeUn9_nPldFgr3zXyNY5X9iYI0Xl0KndP91KGXAsGGhEJVOtjdOLu1aT9oH4ybsRCzgj-qmfEQ9sq4jtx_QsBrMVHymY1xgG3NFuriBnjBpmIsKZhrIXXMKf4a2NK3Q-7GrX2AOace_PPLq7lRxF21Z6J3eF8jPaes4VSg0qIcsAJj9kAZtiMCRWQpIs0Xg9DHXII8hLIYOmirO4OS-r1pecCVy6YvSjX4XEr_y6DMtVTBANw5ZJe0UHQsSMMYFWUUWc5wnAQ2J4wTaY7HIUBGqq1K117iakWZVvIMdTmp4VlmpYAknT6ziLD1RrD_TpKE3jGS1Dmktow8nWtsG5m4C5pUYDrLMGsdQQ8DLSL4klXGwDCfkGc1YPJvcfbid5nK8ph5c9g4FXtWjw6m_BMkCXvirna9ebTpJRjKt7xED5NnPLL5XWyPoo");
    sessionStorage.setItem("client","3");

    // Credeciais da Smart
    // let user = {"data":{"id":2220,"client":1991,"name":"Universidade Smart Fit","email":"universidadesmartfit@plataformaasap.com.br","role":"client","client_id":"20","resource":[{"enabled":true,"tag":"configurar"},{"enabled":true,"tag":"documentos"},{"enabled":true,"tag":"permissoes"},{"enabled":false,"tag":"historias"},{"enabled":true,"tag":"galeria"},{"enabled":false,"tag":"tutoriais"},{"enabled":true,"tag":"relatorios"},{"enabled":true,"tag":"comunicacao"},{"enabled":true,"tag":"gerenciadores"},{"enabled":true,"tag":"cadastros"}],"resource_sub":[{"enabled":true,"tag":"tutor","parent":"gerenciadores"},{"enabled":true,"tag":"groups","parent":"cadastros"},{"enabled":true,"tag":"canais","parent":"gerenciadores"},{"enabled":true,"tag":"menus","parent":"gerenciadores"},{"enabled":true,"tag":"faq","parent":"comunicacao"},{"enabled":false,"tag":"customization","parent":"configurar"},{"enabled":true,"tag":"template","parent":"configurar"},{"enabled":true,"tag":"edital","parent":"gerenciadores"},{"enabled":true,"tag":"voucher","parent":"gerenciadores"},{"enabled":true,"tag":"banners","parent":"gerenciadores"},{"enabled":true,"tag":"trilhas-artigos","parent":"gerenciadores"},{"enabled":true,"tag":"regional","parent":"gerenciadores"},{"enabled":true,"tag":"trilhas-develop","parent":"gerenciadores"},{"enabled":true,"tag":"trilhas-mini","parent":"gerenciadores"},{"enabled":true,"tag":"forum","parent":"gerenciadores"},{"enabled":true,"tag":"suporte","parent":"comunicacao"},{"enabled":true,"tag":"help-desk","parent":"comunicacao"},{"enabled":true,"tag":"gameficacao","parent":"comunicacao"},{"enabled":true,"tag":"email-marketing","parent":"comunicacao"},{"enabled":true,"tag":"videos","parent":"gerenciadores"},{"enabled":true,"tag":"trilhas","parent":"gerenciadores"},{"enabled":true,"tag":"configurar","parent":"cadastros"},{"enabled":true,"tag":"usuarios","parent":"cadastros"},{"enabled":true,"tag":"documentos","parent":"gerenciadores"}],"resource_sub_down":null,"picture":"https://firebasestorage.googleapis.com/v0/b/admin-17e4f.appspot.com/o/images%2Fcpmdygxsmartfit.jpg?alt=media&token=c2633929-4738-4d5a-9927-a87b60ba1f70","analytics":null,"extras":[{"value":"{\"uf\":\"Distrito Federal\",\"cidade\":\"Bras\\u00edlia\",\"sexo\":null,\"cpf\":\"33333\",\"rg\":\"33333\",\"cep\":\"72135-240\",\"logradouro\":null,\"bairro\":null,\"localidade\":null,\"nasciento\":null,\"cargos\":\"professor\",\"unidades\":\"Brasilia\",\"regiao\":null}"}],"groups":[],"created_at":{"date":"2018-09-08 11:51:37.000000","timezone_type":3,"timezone":"America/Sao_Paulo"},"link":"https://smartfit-br.twcreativs.stream/api/","gestor":false,"publication":true}};
    // sessionStorage.setItem("token","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdlZWYyZDU2Y2VlMGE1NWUyMDA4NWZjYzZlNDY0ZGYzNmQ3MjYzNDgxZjZmNjk5MzdkYzZhNzcyNmI3MjQzODljNGEzNWExOWZkNmU2ZDM1In0.eyJhdWQiOiIxIiwianRpIjoiN2VlZjJkNTZjZWUwYTU1ZTIwMDg1ZmNjNmU0NjRkZjM2ZDcyNjM0ODFmNmY2OTkzN2RjNmE3NzI2YjcyNDM4OWM0YTM1YTE5ZmQ2ZTZkMzUiLCJpYXQiOjE1NjM5MTM4NTMsIm5iZiI6MTU2MzkxMzg1MywiZXhwIjoxNTk1NTM2MjUzLCJzdWIiOiIyMjIwIiwic2NvcGVzIjpbXX0.UKqSCjsARt3SpE8_446TeriOtaR6BlneURRenYABgUzu1gMDIUjkNou_MshlB__McvgNkwQRC6C1NcQekGR4OZ8UZHwGI0nrHlpkjT1kTVpzPDFl7Q_YwXtOJdB5rMsNCuc7Rgc7zLPI6cmGw7_9PybdoXj9HD3zuOH7tMfP_MJxHrYxv45ViJvaR9A_VQg8t8vpEYFg-ITkq0LzVZswFdF_Xmc0bV0GXzdrE3gxCw2nrM4r40I_cJQYk5Msyi2Re0__vPtxi-3ndEbgqxk1Emnl_1h6ifuNHCx7ldqJnj1JWjatP_lErN8QLPv8UmLOeG2jXlLSQH6B61P584eKNdbPah7PZAA94n_H1MmWvbqM5lnn8m1C9PmuZ_BSQKqAGyMzApho4JjkLf5ZwakrByd1Y7fUY2t8t_7JZEKDTNAL-KhMs3KPZudy9aTDRWMTviXCtEIJ4frvRS04reZVUvTd4hR9KFcTYoG01rpYBbL3ITd9BW2_NDT6xWcAV6amy1VFndjyR9F-m9DyCfqVrCABDwpzcseG4XxM7PF_ysJtmsKYhFLIUdevQ5MPi6-Dv9r1cE98gPoU0RjQPw8_59YagFb4iBIubZTT7DrbttysuNnhUH9miNHdHF0O75Cldywt5CUxYPWfeLVQLry9l8bmJRdhiXMgAKZqy3W1owY");
    // sessionStorage.setItem("client","20");

    sessionStorage.setItem("apiURL","https://apiv3.plataformaasap.com.br")
    sessionStorage.setItem("loggedUser",JSON.stringify(user));
  }

  public select() {
      // this.global.searchUser({
      //     multiselect: true
      // }).subscribe((response) => {
      //   console.log("Select",response);
      // });
      this.checkout.checkout();
  }

  public AddPhoto(archive) {
    console.log("AddPhoto",archive);
  }
}
