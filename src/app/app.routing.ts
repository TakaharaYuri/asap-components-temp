import { Routes } from '@angular/router';
import { OtherComponent } from './other.component';

export const AppRoutes: Routes = [
    {
        path: 'home',
        component: OtherComponent
    }
];
