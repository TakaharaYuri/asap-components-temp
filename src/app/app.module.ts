import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { OtherComponent } from './other.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule, MatMenuModule, MatButton, MatButtonModule, MatFormFieldModule, MatAutocompleteModule, MatButtonToggleModule, MatCardModule, MatChipsModule, MatDatepickerModule, MatCheckboxModule, MatExpansionModule, MatGridListModule, MatInputModule, MatListModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatStepperModule, MatOptionModule } from '@angular/material';
import { NgxNouisliderModule } from 'ngx-nouislider';
import { AppRoutes } from './app.routing';
import { HttpModule } from '@angular/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AsapCommentsModule } from 'projects/asap-comments/src/public_api';
import { AsapCrudModule, Global } from 'asap-crud';
//import { AsapCommentsModule } from 'asap-comments';

// import { AsapImagePipeModule } from 'projects/asap-image-pipe/src/public_api';
//import { AsapImagePipeModule } from 'asap-image-pipe';

//import { AsapUploadModule } from 'asap-upload';
//import { AsapUploadModule } from 'projects/asap-upload/src/public_api';


//import { AsapCheckoutModule } from 'asap-checkout';
import { AsapCheckoutModule } from 'projects/asap-checkout/src/public_api';
import { AsapFeedModule } from '../../projects/asap-feed/src/lib/asap-feed.module';
import { AsapViewerModule } from 'projects/asap-viewer/src/public_api';
// import { AsapUploadModule } from 'projects/asap-upload/src/public_api';
import { AsapImagePipeModule } from 'projects/asap-image-pipe/src/public_api';

import { AsapSkeletonModule } from 'projects/asap-skeleton/src/public_api';
import { AsapLimitToModule } from 'projects/asap-limit-to/src/projects';
//import { AsapSkeletonModule } from 'asap-skeleton';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    // MatDialogModule,
    // MatIconModule,
    // MatMenuModule,
    // MatButtonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    MatOptionModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AngularFireModule.initializeApp(
      // {
      //   apiKey: "AIzaSyDpcFrHt_pPei3MkccLM011LaeCrdrI8Vo",
      //   authDomain: "asta-e860d.firebaseapp.com",
      //   databaseURL: "https://asta-e860d.firebaseio.com",
      //   projectId: "asta-e860d",
      //   storageBucket: "asta-e860d.appspot.com",
      //   messagingSenderId: "576204749170"
      // },
      {
        apiKey: "AIzaSyDcVaYzLKZKPFwL31F099rp3p8s30XYlQ0",
        authDomain: "universidade-smart-fit.firebaseapp.com",
        databaseURL: "https://universidade-smart-fit.firebaseio.com",
        projectId: "universidade-smart-fit",
        storageBucket: "universidade-smart-fit.appspot.com",
        messagingSenderId: "706681009423"
      }
    ),
    AngularFirestoreModule,
    AngularFireStorageModule,
    SimpleNotificationsModule.forRoot(),
    NgxNouisliderModule,
    // ---------------- ASAP ----------------------
    AsapCrudModule,
    AsapFeedModule,
    AsapSkeletonModule,
    AsapCheckoutModule,
    AsapViewerModule,
    // AsapUploadModule,
    // AsapImagePipeModule,
    AsapLimitToModule,
    AsapCommentsModule
    // ----------------------------------------------
  ],
  declarations: [
    AppComponent,
    OtherComponent
  ],
  exports: [
    AsapCrudModule
  ],
  providers: [
    NotificationsService,
    Global,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
