import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapChatComponent } from './asap-chat.component';

describe('AsapChatComponent', () => {
  let component: AsapChatComponent;
  let fixture: ComponentFixture<AsapChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
