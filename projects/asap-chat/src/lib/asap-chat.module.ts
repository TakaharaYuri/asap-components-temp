import { NgModule } from '@angular/core';
import { AsapChatComponent } from './asap-chat.component';

@NgModule({
  declarations: [AsapChatComponent],
  imports: [
  ],
  exports: [AsapChatComponent]
})
export class AsapChatModule { }
