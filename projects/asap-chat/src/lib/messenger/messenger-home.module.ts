import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MessengerHomeRoutes } from './messenger-home.routing';
import { SharedModule } from '../../shared/shared.module';

import { MessengerHomeComponent } from './messenger-home.component';
import { MessengerConversationComponent } from './conversation/messenger-conversation.component';
import { MomentModule } from 'ngx-moment';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MessengerHomeRoutes),
        MomentModule,
        SharedModule
    ],
    declarations: [
        MessengerHomeComponent,
        MessengerConversationComponent
    ],
    exports: [  ],

    bootstrap: [  ]
})

export class MessengerHomeModule { }
