import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { Global } from 'src/app/@core/asap-base/global';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
    selector: 'app-messenger-home',
    templateUrl: './messenger-home.component.html'
})

export class MessengerHomeComponent {

    public searchUser;
    public searchStatus;

    public users;
    public conversation;
    public selectedUser;
    public msgDate;

    constructor(
        public global: Global,
        public firestore: AngularFirestore
    ) {
        this.users = firestore.collection("/status", (ref) => ref.orderBy("status","desc").orderBy("user_name")).valueChanges();
        console.log("Users",this.users);
    }

    public filteredUser(user) {
        if (this.searchStatus != null) {
            return user.status == this.searchStatus;
        }
        if (this.searchUser) {
            if (user.user_name.toUpperCase().indexOf(this.searchUser.toUpperCase()) < 0) {
                return false;
            }
        }
        return true;
    }

    public selectUser(user) {
        this.selectedUser = user;
        this.conversation = this.firestore.collection(`/messages/user_${this.global.loggedUser().id}/user_${this.selectedUser.uid}`, (ref) => ref.orderBy("timesamp")).valueChanges();
        this.conversation.subscribe((message) => {
            setTimeout(() => {
                window.scrollTo(0,document.body.scrollHeight);
            }, 100);
        });
        console.log(`messages/user_${this.global.loggedUser().id}/user_${this.selectedUser.uid}`);
        console.log("Conversation",this.conversation);
        this.msgDate = null;
    }

    public showDate(msg) {
        //console.log("showDate(msg)",msg);
        if (msg.showDate) {
            return msg.showDate;
        }
        let timestamp = (new Date(msg.timesamp)).toISOString().substr(0,10);
        //console.log("showDate",msg.showDate,timestamp);
        if (this.msgDate != timestamp) {
            this.msgDate = timestamp;
            msg.showDate = true;
            return true;
        }
        msg.showDate = false;
        return false;
    }

    public sendMessage(msgInput) {
        let message = msgInput.value;
        if (message != "") {
            let from = this.global.loggedUser().id;
            let to = this.selectedUser.uid
            let msg = {
              message: message,
              picture: this.global.loggedUser().picture,
              sentby: this.global.loggedUser().id,
              timesamp: Date.now()
            };

            this.firestore.collection(`/messages/user_${from}/user_${to}`).add(msg);
            this.firestore.collection(`/messages/user_${to}/user_${from}`).add(msg);
            this.firestore.collection(`notification/chat/user_${to}`).add({user: from});
            this.firestore.collection(`history/chat/user_${to}`).add({
                user: from,
                status: 'unread',
                msg
            });
            
            msgInput.value = "";
        }
    }

}
