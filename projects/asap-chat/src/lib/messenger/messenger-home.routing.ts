import { Routes } from '@angular/router';
import { MessengerHomeComponent } from './messenger-home.component';
import { MessengerConversationComponent } from './conversation/messenger-conversation.component';


export const MessengerHomeRoutes: Routes = [
  {
     path: '',
     children: [
       {
         path: '',
         component: MessengerHomeComponent
       },
       {
         path: 'conversation',
         component: MessengerConversationComponent
       }
   ]
 }
];
