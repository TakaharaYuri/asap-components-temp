import { TestBed } from '@angular/core/testing';

import { AsapChatService } from './asap-chat.service';

describe('AsapChatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapChatService = TestBed.get(AsapChatService);
    expect(service).toBeTruthy();
  });
});
