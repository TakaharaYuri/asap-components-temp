# ASAP SKELETON LOADING
Componente para montar *Skeletons* de pre-loading
###### _ASAP GROUP_
----

#### Como utilizar?
-----
Declare a importação do módulo no *app.module.ts*
```javascript

import { AsapSkeletonModule } from 'asap-skeleton';

@NgModule({
    imports: [
        AsapSkeletonModule
    ]
})

export class AppModule {}

```

Inicialize uma váriavel de  *loading* para que você possa passar o estado atual da sua **requisição** no caso de um **request**
```javascript
import { Component, OnInit } from '@angular/core';

declare const $: any;

@Component({
   
})
export class AppComponent implements OnInit {

    public trails;
    public loading = true;

    constructor(
        public trailService: TrailService
    ) {

    }

    ngOnInit() {
        this.trailService.trails().subscribe((response:any) => {
            this.trails = response;
            this.loading = false;
        });
    }

}

```

Agora organize as sessões do **skeleton** como desejar 

```html
<asap-skeleton type="box" [lines]="7" boxHeigth="130px" boxWidth="130px"></asap-skeleton>
```
![Exemplo](https://res.cloudinary.com/midia9-group/image/upload/v1563021048/download_v57jja.png)


Parametros aceitos

| Parametro        | Aeito           | Acao  |
| ---------------- |:---------------:| ------:|
| type             | box ou line     | Cria sessões em linhas ou caixas |
| lines            | numero     | Quantidade de repetições |
| boxHeigth        | px ou % | Altura do Component |
| boxWidth         | px ou % | Largura do Component|