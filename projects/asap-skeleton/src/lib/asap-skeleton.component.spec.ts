import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapSkeletonComponent } from './asap-skeleton.component';

describe('AsapSkeletonComponent', () => {
  let component: AsapSkeletonComponent;
  let fixture: ComponentFixture<AsapSkeletonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapSkeletonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
