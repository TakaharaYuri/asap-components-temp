import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'asap-skeleton',
  templateUrl: './asap-skeleton.component.html',
  styleUrls:['./asap-skeleton.component.scss']
})
export class AsapSkeletonComponent implements OnInit, OnChanges {

  @Input() lines:number = 1;
  @Input() type:string = 'lines';
  @Input() boxWidth:any = '100%';
  @Input() boxHeight:any = '150px';
  @Input() boxStyle: any = 'flex';
  public repeat = [];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes:SimpleChanges) {
    if (changes) {
      this.repeat = new Array(this.lines);
    }
  }

}
