import { NgModule } from '@angular/core';
import { AsapSkeletonComponent } from './asap-skeleton.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser'

@NgModule({
  declarations: [AsapSkeletonComponent],
  imports: [CommonModule],
  exports: [AsapSkeletonComponent]
})
export class AsapSkeletonModule { }
