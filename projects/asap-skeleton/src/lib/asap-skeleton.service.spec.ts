import { TestBed } from '@angular/core/testing';

import { AsapSkeletonService } from './asap-skeleton.service';

describe('AsapSkeletonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapSkeletonService = TestBed.get(AsapSkeletonService);
    expect(service).toBeTruthy();
  });
});
