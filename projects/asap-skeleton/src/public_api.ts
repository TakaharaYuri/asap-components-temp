/*
 * Public API Surface of asap-skeleton
 */

export * from './lib/asap-skeleton.service';
export * from './lib/asap-skeleton.component';
export * from './lib/asap-skeleton.module';
