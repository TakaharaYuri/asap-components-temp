import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({name: 'asapLimitTo'})
export class asapLimitToPipe implements PipeTransform {
    constructor(private domSanitizer: DomSanitizer) {}

    transform(string: any, size: number): any {
        if (string) {
          if (size) {
            return (string.length > size)? string.slice(0, size) + '...':string;
          }
        }
        return null;
    }
}

