import { TestBed } from '@angular/core/testing';

import { AsapLimitToService } from './asap-limit-to.service';

describe('AsapLimitToService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapLimitToService = TestBed.get(AsapLimitToService);
    expect(service).toBeTruthy();
  });
});
