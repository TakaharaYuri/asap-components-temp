import { NgModule } from '@angular/core';
import { asapLimitToPipe } from './asap-limit-to.component';

@NgModule({
  declarations: [asapLimitToPipe],
  imports: [
  ],
  exports: [asapLimitToPipe]
})
export class AsapLimitToModule { }
