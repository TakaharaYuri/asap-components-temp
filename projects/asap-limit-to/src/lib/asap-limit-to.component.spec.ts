import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { asapLimitToPipe } from './asap-limit-to.component';

describe('asapLimitToPipe', () => {
  let component: asapLimitToPipe;
  let fixture: ComponentFixture<asapLimitToPipe>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ asapLimitToPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(asapLimitToPipe);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
