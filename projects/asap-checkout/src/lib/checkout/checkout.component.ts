import { Component, Input, Inject, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Global } from 'asap-crud';
import { AsapCheckoutService } from '../asap-checkout.service';

declare var $:any;

@Component({
  selector: 'asap-checkout-component',
  templateUrl: './checkout.component.html',
  styleUrls: [ './checkout.component.scss' ],
  providers: [ AsapCheckoutService ]
})

export class CheckoutComponent implements OnChanges {

    @Input() data;
    @Output() onCheckout = new EventEmitter<any>();

    public checkout;
    public loading = false;
    public paid = false;
    public error = false;
    public response;
    public payment:any = {
        method: 1,
        installments: 1,
        District: ""
    };

    constructor(
        public global: Global,
        public service: AsapCheckoutService
    ) {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.data) {
            this.checkout = this.data;
            this.payment.target_id = null;
            this.payment.target_type = "";
            this.payment.valor = this.data.value;
        }
    }

    public canSave() {
        return ($('.ng-invalid').length <= 0);
    }

    public proceedCheckout() {
        if (this.canSave()) {
            this.loading = true;
            setTimeout(() => {
                if (this.payment.method == 1) {
                    this.payment.bandeira = this.service.cardBrand(this.payment.nunCard).toUpperCase();
                    this.service.checkoutCreditCard(this.payment).subscribe(
                        (response) => {
                            if (response.Payment) {
                                this.error = false;
                                if (response.Payment.Status != 1) this.error = true;
                                this.response = response;
                                this.paid = true;
                                this.onCheckout.emit(response);
                            }
                            else {
                                this.global.alert("Erro","Code:" + response[0].Code + " - " + response[0].Message,"error");
                            }
                            this.loading = false;
                        },
                        (error) => {
                            this.global.alert("Erro","Ocorreu um erro ao realizar a solicitação, tente novamente!","error");
                            console.log(error);
                            this.loading = false;
                        }
                    );
                }
                else {
                    this.service.checkoutBankSlip(this.payment).subscribe(
                        (response) => {
                            console.log("Boleto: ", response);
                            this.response = response;
                            this.paid = true;
                            this.loading = false;
                            this.onCheckout.emit(response);
                        },
                        (error) => {
                            this.global.alert("Erro","Ocorreu um erro ao realizar a solicitação, tente novamente!","error");
                            console.log(error);
                            this.loading = false;
                        }
                    ); 
                }                
            }, 100);
        }
        else {
            this.global.alert("Pagamento","Verifique os campos e preencha os dados corretamente!","error");
        }
    }




}
