import { Component, ViewChild, Input, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { AsapCheckoutService } from './asap-checkout.service';
import { MatDialog } from '@angular/material';
import { ModalCheckoutComponent } from './modal/modal-checkout.component';

@Component({
  selector: 'asap-checkout',
  templateUrl: './asap-checkout.component.html',
  styleUrls: [ './asap-checkout.component.scss' ],
  providers: [ AsapCheckoutService ],
  encapsulation: ViewEncapsulation.None
})

export class AsapCheckoutComponent {

    @Input() product:string;
    @Input() description:string;
    @Input() value:number;
    @Input() picture:string;

    @Input() modal = true;

    @Output() onCheckout = new EventEmitter<any>();

    constructor(
        public dialog: MatDialog
    ) {

    }

    public data() {
        return {
            product: this.product,
            description: this.description,
            value: this.value,
            picture: this.picture
        }
    }

    public checkout() {
        const dialogRef = this.dialog.open(ModalCheckoutComponent, {
            width: '700px',
            data: this.data()
        });
    
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed', result);
            this.doCheckout(result);
        });
    }

    public doCheckout(data) {
        this.onCheckout.emit(data);
    }

}
