import { Component, Input, Inject, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Global } from 'asap-crud';
import { AsapCheckoutService } from '../asap-checkout.service';

declare var $:any;

@Component({
  selector: 'asap-checkout-modal',
  templateUrl: './modal-checkout.component.html',
  providers: [  ]
})

export class ModalCheckoutComponent {

    public data;

    constructor(
        public dialogRef: MatDialogRef<ModalCheckoutComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData:any,
    ) {
        this.data = modalData;
    }

}
