import { TestBed } from '@angular/core/testing';

import { AsapCheckoutService } from './asap-checkout.service';

describe('AsapCheckoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapCheckoutService = TestBed.get(AsapCheckoutService);
    expect(service).toBeTruthy();
  });
});
