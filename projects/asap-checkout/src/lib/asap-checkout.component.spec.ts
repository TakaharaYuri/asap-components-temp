import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapCheckoutComponent } from './asap-checkout.component';

describe('AsapCheckoutComponent', () => {
  let component: AsapCheckoutComponent;
  let fixture: ComponentFixture<AsapCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
