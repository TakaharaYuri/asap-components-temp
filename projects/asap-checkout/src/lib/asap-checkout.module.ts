import { NgModule } from '@angular/core';
import { AsapCheckoutComponent } from './asap-checkout.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule, MatRadioModule, MatFormFieldModule, MatOptionModule, MatSelectModule, MatInputModule, MatCardModule } from '@angular/material';
import { CurrencyMaskConfig, NgxCurrencyModule } from 'ngx-currency';
import { CheckoutComponent } from './checkout/checkout.component';
import {NgxMaskModule, IConfig} from 'ngx-mask'
import { ModalCheckoutComponent } from './modal/modal-checkout.component';
 
export const customCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: 'R$',
    suffix: '',
    thousands: '.',
    nullable: true
};

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      MatStepperModule,
      MatRadioModule,
      MatFormFieldModule,
      MatSelectModule,
      MatOptionModule,
      MatInputModule,
      MatCardModule,
      NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
      NgxMaskModule.forRoot()
  ],
  declarations: [
      AsapCheckoutComponent,
      CheckoutComponent,
      ModalCheckoutComponent
  ],
  entryComponents: [
    ModalCheckoutComponent
  ],
  providers: [
      
  ],
  exports: [
      AsapCheckoutComponent,
      CheckoutComponent,
      ModalCheckoutComponent
  ]
})
export class AsapCheckoutModule { }
