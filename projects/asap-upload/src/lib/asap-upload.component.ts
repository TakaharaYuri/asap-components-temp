import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalMultiuploadPluginComponent } from './modal-multi-upload/modal-multi-upload.component';
import { ModalUploadPluginComponent } from './modal-upload/modal-upload.component';

@Component({
  selector: 'asap-upload',
  templateUrl: './asap-upload.component.html',
  styles: ['./asap-upload.component.css']
})
export class AsapUploadComponent implements OnInit {


  @Input() horizontal;
  @Input() aspectRatio;
  @Input() resizeToWidth;
  @Input() type;
  @Input() multiple;
  @Input() onlyType;
  @Output() AddPhoto: EventEmitter<any> = new EventEmitter<any>();
  @Output() AddArchive: EventEmitter<any> = new EventEmitter<any>();
  @Input() btnClass = 'btn btn-default btn-xs';
  @Input() btnTitle = 'ADICIONAR ARQUIVO';

  constructor(
      public dialog: MatDialog
  ) { }

  ngOnInit() {
    
  }

  openDialog(): void {
      if (this.multiple) {
          const dialogRef = this.dialog.open(ModalMultiuploadPluginComponent, {
              width: '90%',
              maxWidth: '90%',
              height: '90vh',
              disableClose: true,
              data: {
                type: this.type,
                resizeToWidth: this.resizeToWidth,
                aspectRatio: this.aspectRatio,
                horizontal: this.horizontal,
                multiple: this.multiple,
                onlyType: this.onlyType
              }
          });

          dialogRef.afterClosed().subscribe(result => {
              if (result) {
                  if (this.type === 'image') {
                      this.AddPhoto.emit(result);
                  } else {
                      this.AddArchive.emit(result);
                  }
              }
          });
      } else {
          const dialogRef = this.dialog.open(ModalUploadPluginComponent, {
              width: '90%',
              maxWidth: '90%',
              height: '90vh',
              disableClose: true,
              data: {
                type: this.type,
                resizeToWidth: this.resizeToWidth,
                aspectRatio: this.aspectRatio,
                horizontal: this.horizontal,
                multiple: this.multiple,                  
                onlyType: this.onlyType
              }
          });

          dialogRef.afterClosed().subscribe(result => {
              if (result) {
                  if (this.type === 'image') {
                      this.AddPhoto.emit(result);
                  } else {
                      this.AddArchive.emit(result);
                  }
              }
          });
      }
  }


}
