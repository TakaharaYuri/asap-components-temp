import { NgModule } from '@angular/core';
import { AsapUploadComponent } from './asap-upload.component';
import { ModalUploadPluginComponent } from './modal-upload/modal-upload.component';
import { ModalMultiuploadPluginComponent } from './modal-multi-upload/modal-multi-upload.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatButtonToggleModule, MatCheckboxModule, MatSnackBarModule } from '@angular/material';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatSnackBarModule,
    ImageCropperModule,  
  ],
  declarations: [
    AsapUploadComponent,
    ModalUploadPluginComponent,
    ModalMultiuploadPluginComponent
  ],
  exports: [
    AsapUploadComponent
  ],
  entryComponents: [ 
    ModalUploadPluginComponent,
    ModalMultiuploadPluginComponent
  ],
  providers: [
  ]
})

export class AsapUploadModule { }
