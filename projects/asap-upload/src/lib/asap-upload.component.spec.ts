import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapUploadComponent } from './asap-upload.component';

describe('AsapUploadComponent', () => {
  let component: AsapUploadComponent;
  let fixture: ComponentFixture<AsapUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
