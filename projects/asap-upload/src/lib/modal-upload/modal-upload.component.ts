import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AngularFireStorage } from '@angular/fire/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-upload-plugin',
  templateUrl: './modal-upload.component.html',
  styleUrls: ['./modal-upload.component.css'],
  providers: []
})

export class ModalUploadPluginComponent {

    typeImage = false;
    uploadMode = true;
    fileName: any;
    fileSize: any;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    cropperReady = false;
    percentage = 0;
    storeRef: AngularFireStorage;
    archive: any;
    resizeToWidth: number;
    aspectRatio: string;
    horizontal: boolean;
    format: string;
    onlyType: string;
    maintainAspectRatio = true;

    constructor(
      public sanitizer: DomSanitizer,
      public dialogRef: MatDialogRef<ModalUploadPluginComponent>,
      @Inject(MAT_DIALOG_DATA) public data,
      public fb: AngularFireStorage
    ) {
        this.storeRef = fb;
        this.typeImage = (data.type === 'image') ? true : false;
        this.resizeToWidth = data.resizeToWidth;
        this.aspectRatio = data.aspectRatio;
        this.horizontal = data.horizontal;
        this.onlyType = data.onlyType;
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    reset() {
      this.croppedImage = '';
      this.fileName = '';
      this.imageChangedEvent = '';
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCroppedBase64(image: string) {
        //this.croppedImage = this.sanitizer.bypassSecurityTrustResourceUrl(image);
        this.croppedImage = (image);
    }
    imageLoaded() {
      this.cropperReady = true;
    }
    imageLoadFailed () {
      console.log('Load failed');
    }

    UploadImage(event) {
        if (event.srcElement.files[0]) {
            this.imageChangedEvent = event;
            this.fileName = event.srcElement.files[0].name;
            this.format = event.srcElement.files[0].type;
        }
    }

    loadImageFailed() {

    }


    dataURItoBlob(dataURI) {
        const type = 'image/png';
        const byteString = atob(dataURI.split(',')[1]);
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        const bb = new Blob([ab], { type: type });
        return bb;
    }

    sendImage() {
        const imagee = this.dataURItoBlob(this.croppedImage)
        const caminho = this.storeRef.ref('images/' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10) + this.fileName);
        const tarefa = caminho.put(imagee);
        tarefa.percentageChanges().subscribe((percentage) => {
            this.percentage = Math.trunc(percentage);
        });
        tarefa.snapshotChanges().pipe(
            finalize(() => {
              caminho.getDownloadURL().subscribe((url) => {
                this.dialogRef.close({
                    path: url,
                    name: this.fileName,
                    size: this.fileSize,
                    format: this.format
                });                
              });
            })
        ).subscribe();
        tarefa.snapshotChanges().subscribe((snapshot) => {
            this.fileSize = snapshot.totalBytes;
        });
    }

    UploadArchive(event) {
        if (event && this.whiteListUploads(event.target.files[0].type)) {
            this.fileName = event.srcElement.files[0].name;
            this.archive = event.srcElement.files[0];
            this.format = event.srcElement.files[0].type;
        } else {
            Swal.fire({
              type: 'error',
              title: 'Erro ao carregar o arquivo.',
              text: 'Verifique se o arquivo tem o formato permitido.',
              buttonsStyling: true,
              confirmButtonClass: 'btn btn-success'
            });
        }
    }

    sendArchive() {
        this.sendToFirebase();
    }

    sendToFirebase() {
      const name = 'archives/' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10) + '_' + this.fileName;
      const caminho = this.storeRef.ref(name);
      const tarefa = caminho.put(this.archive);
      tarefa.percentageChanges().subscribe((percentage) => {
          this.percentage = Math.trunc(percentage);
      });
      tarefa.snapshotChanges().pipe(
          finalize(() => {
            caminho.getDownloadURL().subscribe((url) => {
              this.dialogRef.close({
                  path: url,
                  name: this.fileName,
                  size: this.fileSize,
                  format: this.format
              });                
            });
          })
      ).subscribe();
      tarefa.snapshotChanges().subscribe((snapshot) => {
          this.fileSize = snapshot.totalBytes;
      });
    }

    private whiteListUploads(fileType: any): boolean {
        switch (fileType) {
          case 'image/png':
            return true;
          case 'image/jpeg':
            return true;
          case 'image/jpg':
            return true;
          case 'image/gif':
            return true;
          case 'image/png':
            return true;
          case 'application/msword':
            return true;
          case 'application/pdf':
            return true;
          case 'application/zip':
            return true;
          case 'application/vnd.ms-excel':
            return true;
          case 'video/x-flv':
            return true;
          case 'video/mp4':
            return true;
          case 'application/x-mpegURL':
            return true;
          case 'video/MP2T':
            return true;
          case 'video/3gpp':
            return true;
          case 'video/quicktime':
            return true;
          case 'video/x-msvideo':
            return true;
          case 'video/x-ms-wmv':
            return true;
          case '':
            return true;
          default:
            return true;
        }
    }
}
