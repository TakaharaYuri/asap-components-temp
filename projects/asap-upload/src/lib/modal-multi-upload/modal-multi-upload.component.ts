import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { finalize } from 'rxjs/operators';

import Swal from 'sweetalert2';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-modal-multi-upload-plugin',
  templateUrl: './modal-multi-upload.component.html',
  styleUrls: ['./modal-multi-upload.component.css']
})

export class ModalMultiuploadPluginComponent {

    typeImage = false;
    uploadMode = true;
    fileName:any;
    fileSize;
    fileNameArray: any;
    imageChangedEventArray: any;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    cropperReady = false;
    percentage = 0;
    storeRef: AngularFireStorage;
    archive: any;
    resizeToWidth: number;
    aspectRatio: string;
    horizontal: boolean;
    urlsImage = [];
    urlsVideo = [];
    urlsArchive = [];

    constructor(
        public _sanitizer: DomSanitizer,
        public dialogRef: MatDialogRef<ModalMultiuploadPluginComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        public fb: AngularFireStorage
    ) { 
        this.storeRef = fb;
        data.type === 'image' ? this.typeImage = true : this.typeImage = false;
        this.resizeToWidth = data.resizeToWidth;
        this.aspectRatio = data.aspectRatio;
        this.horizontal = data.horizontal;
    }
    
    onNoClick(): void {
        this.dialogRef.close();
    }

    reset() {
      this.croppedImage = '';
      this.fileName = '';
      this.imageChangedEvent = '';
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCroppedBase64(image: string) {
        this.croppedImage = image;
    }
    imageLoaded() {
      this.cropperReady = true;
    }
    imageLoadFailed () {
      console.log('Load failed');
    }

    UploadImage(event) {
      if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          if(event.target.files[i].type == 'image/jpeg' || event.target.files[i].type == 'image/png') {
            var reader = new FileReader();
            reader.onload = (event) => {                  
              this.urlsImage.push(event.target['result']); 
            }            
            reader.readAsDataURL(event.target.files[i]);
          } else if(event.target.files[i].type == 'video/mp4') {
            console.log('aui')
         
            this.urlsVideo.push(window.URL.createObjectURL(event.srcElement.files[i]));            
            console.log(this.urlsVideo)   
          } else  {
            this.fileName = event.srcElement.files[0].name;
            this.urlsArchive.push(event.srcElement.files[i])
          }
          console.log(event.target.files[i])
        }
      }

    }

    getBackground(video) {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${video})`);
    }


    dataURItoBlob(dataURI) {
        let type = 'image/png'
        var byteString = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        var bb = new Blob([ab], { type: type });
        return bb;
    }

    sendImage() {        
        let imagee = this.dataURItoBlob(this.croppedImage)
        let caminho = this.storeRef.ref('images/'+Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10)+this.fileName);
        let tarefa = caminho.put(imagee);
        tarefa.percentageChanges().subscribe((percentage) => {
            this.percentage = percentage;
        });
        tarefa.snapshotChanges().pipe(
            finalize(() => {
              this.dialogRef.close({
                  path: caminho.getDownloadURL(),
                  name: this.fileName,
                  size: this.fileSize
              });
            })
        ).subscribe();
        tarefa.snapshotChanges().subscribe((snapshot) => {
            this.fileSize = snapshot.totalBytes;
        });
    }

    UploadArchive(event) {
        if (event && this.whiteListUploads(event.target.files[0].type)) {
            this.fileName = event.srcElement.files[0].name;
            this.archive = event.srcElement.files[0];
        } else {
            Swal.fire({
              type: 'error',
              title: 'Erro ao carregar o arquivo.',
              text: 'Verifique se o arquivo tem o formato permitido.',
              buttonsStyling: true,
              confirmButtonClass: 'btn btn-success'
            });
        }
    }

    sendArchive() {
        let caminho = this.storeRef.ref('archives/' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10) + '_' + this.fileName);
        let tarefa = caminho.put(this.archive);
        tarefa.percentageChanges().subscribe((percentage) => {
            this.percentage = percentage;
        });
        tarefa.snapshotChanges().pipe(
            finalize(() => {
              this.dialogRef.close({
                  path: caminho.getDownloadURL(),
                  name: this.fileName,
                  size: this.fileSize
              });
            })
        ).subscribe();
        tarefa.snapshotChanges().subscribe((snapshot) => {
            this.fileSize = snapshot.totalBytes;
        });
    }

    private whiteListUploads(fileType: any): boolean {
        switch (fileType) {
          case 'image/png':
            return true;
          case 'image/jpeg':
            return true;
          case 'image/jpg':
            return true;
          case 'image/gif':
            return true;
          case 'image/png':
            return true;
          case 'application/msword':
            return true;
          case 'application/pdf':
            return true;
          case 'application/zip':
            return true;
          case 'video/x-flv':
            return true;
          case 'video/mp4':
            return true;
          case 'application/x-mpegURL':
            return true;
          case 'video/MP2T':
            return true;
          case 'video/3gpp':
            return true;
          case 'video/quicktime':
            return true;
          case 'video/x-msvideo':
            return true;
          case 'video/x-ms-wmv':
            return true;
          default:
            return false;
        }
    }
}
