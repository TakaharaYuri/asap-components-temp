import { TestBed } from '@angular/core/testing';

import { AsapUploadService } from './asap-upload.service';

describe('AsapUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapUploadService = TestBed.get(AsapUploadService);
    expect(service).toBeTruthy();
  });
});
