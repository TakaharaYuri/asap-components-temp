import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { AssessmentService } from '../../../../@core/services/assessment.service';

declare const swal: any;

@Component({
  selector: 'app-detail-assessment-inner',
  templateUrl: './detail-inner.component.html',
  styleUrls: ['./detail-inner.component.scss'],
  providers: [AssessmentService]
})
export class DetailInnerComponent implements OnInit {

  @Input() id: number;
  @Output() public assessmentSendedEvent = new EventEmitter<any>();

  assessment: Assessment;
  noRegisters: boolean;
  isSubmiting: boolean;
  isFormLocked: boolean;
  post: any;

  constructor(private _assessmentService: AssessmentService, private activatedRoute: ActivatedRoute) {
    this.noRegisters = false;
    this.isSubmiting = false;
    this.isFormLocked = false;
    this.getParam();
  }

  ngOnInit() {
    if (this.id) {
      this.getAssessment(this.id);
    }
  }

  getParam(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  getAssessment(id: number): void {
    this.assessment = null;
    this.noRegisters = false;
    this.isFormLocked = false;
    this._assessmentService.find(id).subscribe((response:any) => {
        if (response.data.length === 0) {
          this.noRegisters = true;
          this.assessment = new Assessment;
        } else {
          this.assessment = this.transform(response.data);
          if (response.data[0].resposta.length > 0) {
            this.isFormLocked = true;
          }
        }
      }, err => {
        console.log(err);
      });
  }

  goToNextAssessment(): void {
    this.assessmentSendedEvent.emit(this.id);
  }

  transform(data: Array<any>): Assessment {

    if(data.length == 0) {
      return new Assessment;
    }

    const a = data[0];

    const assessment = new Assessment;

    assessment.id = a.id;
    assessment.title = a.title;
    assessment.subtitle = a.subTitle;

    assessment.questions = [];

    let iA = 0;

    for(let indA of a.pergunta_tp_a) {
      let indicador = indA.nome;
      let pontuacao_indicador = indA.ponderacao;
      for (const q of indA.questoes) {
        const question = new Question;
        question.nome = q.nome;
        question.ponderacao = q.ponderacao;
        question.type = 'A';
        question.indicador = indicador;
        question.pontuacao_indicador = pontuacao_indicador;

        if (a['resposta'].length > 0) {
          question.value = a['resposta'][0].form.pergunta_tp_a[iA].resposta;
        }

        question.options = [];

        for (const o of q.opcoes) {
          const option = new Option;
          option.nome = o.nome;
          option.valorA = o.valor;

          question.options.push(option);
        }

        assessment.questions.push(question);
        iA++;
      }


    }

    let iB = 0;

    for(let indB of a.pergunta_tp_b) {
      let indicador = indB.nome;
      let pontuacao_indicador = indB.ponderacao;

      for (const q of indB.questoes) {
        const question = new Question;
        question.nome = q.nome;
        question.ponderacao = q.ponderacao;
        question.indicador = indicador;
        question.pontuacao_indicador = pontuacao_indicador;
        question.textoUm = q.textoUm;
        question.textoDois = q.textoDois;
        question.type = 'B';

        question.options = [];

        for (const o of q.opcoes) {
          const option = new Option;
          option.nome = o.nome;
          option.valorB1 = o.valorUm;
          option.valorB2 = o.valorDois;

          if (a['resposta'].length > 0) {
            option.valorBSelected = a['resposta'][0].form.pergunta_tp_b[iB].resposta[o.nome];
          }

          question.options.push(option);
        }

        console.log(question);

        assessment.questions.push(question);

      }
      iB++;
    }

    return assessment;
  }

  onSubmit(form: any) {

    const counter = 10;

    if (form.valid) {

      const value = form.value;

      console.log("value", value);

      let answersA = [];
      let answersB = [];

      for(let i=0; i<counter; i++) {
        if(value['questionA.' + i + '.ponderacao']) {
          answersA.push({
            resposta: value['questionA.' + i + '.resposta'],
            ponderacao: value['questionA.' + i + '.ponderacao'],
            indicador: value['questionA.' + i + '.indicador'],
            pontuacao_indicador: value['questionA.' + i + '.pontuacao_indicador'],
          });
        }
        if(value['questionB.' + i + '.ponderacao']) {
          let entity = {};

          entity['ponderacao'] = value['questionB.' + i + '.ponderacao'];
          entity['pontuacao_indicador'] = value['questionB.' + i + '.pontuacao_indicador'];
          entity['indicador'] = value['questionB.' + i + '.indicador'];

          let answers = {};

          for(let i2=0; i2<counter; i2++) {

            if(value['questionB.' + i + '.resposta.' + i2 + '.name']) {
              answers[value['questionB.' + i + '.resposta.' + i2 + '.name']] = value['questionB.' + i + '.resposta.' + i2 + '.value'];
            }

          }

          entity['resposta'] = answers;

          answersB.push(entity);
        }
      }

      const body = {
        title: this.assessment.title,
        subTitle: this.assessment.subtitle,
        pergunta_tp_a: answersA,
        pergunta_tp_b: answersB,
        pergunta_id: this.id
      };

      console.log(body);


      this.isSubmiting = true;
      this._assessmentService.getScore(body)
        .subscribe(res => {
          this.goToNextAssessment();
          this.isSubmiting = false;
          swal({
            title: 'Formulário enviado com sucesso',
            text: 'Obrigado por responder a esse questionário',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          });
        }, err => {
          console.log(err);
          this.isSubmiting = false;
        });

    } else {
      swal({
        title: 'Formulário inválido',
        text: 'Por favor, responda todas as respostas',
        type: 'error',
        confirmButtonClass: 'btn btn-success',
      });
    }

  }




























  onSubmit__old(form: any) {

    if (form.valid) {

      const formValue = this.dicToInterable(form.value);

      const answersA = [];
      const answersB = [];

      for (const value of formValue) {

        const keySplit = value.key.split('.');

        if (keySplit[0] === 'questionA') {
          if (!answersA[keySplit[1]]) {
            answersA[keySplit[1]] = []
          }
          answersA[keySplit[1]][keySplit[2]] = value.val;
        }

        if (keySplit[0] === 'questionB') {
          if (!answersB[keySplit[1]]) {
            answersB[keySplit[1]] = []
          }

          if (keySplit[2] === 'ponderacao') {
            answersB[keySplit[1]]['ponderacao'] = value.val;
          }

          if (keySplit[2] === 'resposta') {
            if (!answersB[keySplit[1]]['resposta']) {
              answersB[keySplit[1]]['resposta'] = {};
            }
            answersB[keySplit[1]]['resposta'][keySplit[3]] = value.val;
          }

        }
      }

      const post = {
        'pergunta_id': this.assessment.id,
        'title': this.assessment.title,
        'subTitle': this.assessment.subtitle,
        'indicador_a': this.assessment.indicadorA,
        'indicador_b': this.assessment.indicadorB,
        'PonderacaoAssociadaUm': this.assessment.indicadorPontuacaoA,
        'PonderacaoAssociadaDois': this.assessment.indicadorPontuacaoB,
        'perguntaUm': [],
        'perguntaDois': []
      };

      for (const answerA of answersA) {
        post['perguntaUm'].push(Object.assign({}, answerA));
      }

      for (const answerB of answersB) {
        if (answerB) {
          post['perguntaDois'].push(Object.assign({}, answerB));
        }
      }

      this.isSubmiting = true;
      this._assessmentService.getScore(post)
        .subscribe(res => {
          this.goToNextAssessment();
          this.isSubmiting = false;
          swal({
            title: 'Formulário enviado com sucesso',
            text: 'Obrigado por responder a esse questionário',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
          });
        }, err => {
          console.log(err);
          this.isSubmiting = false;
        });

    } else {
      swal({
        title: 'Formulário inválido',
        text: 'Por favor, responda todas as respostas',
        type: 'error',
        confirmButtonClass: 'btn btn-success',
      });
    }

  }

  dicToInterable(dict: Object): Array<any> {
    const a = [];
    for (const key in dict) {
      if (dict.hasOwnProperty(key)) {
        a.push({key: key, val: dict[key]});
      }
    }
    return a;
  }

}

class Assessment {

  public id: number;
  public title: string;
  public subtitle: string;

  public indicadorA: string;
  public indicadorPontuacaoA: string;

  public indicadorB: string;
  public indicadorPontuacaoB: string;

  public questions: Array<Question>;

}

class Question {

  public nome: string;
  public ponderacao: string;

  public indicador: string;
  public pontuacao_indicador: string;

  public textoUm: string;
  public textoDois: string;

  public type: string;

  public value: any = null;

  public options: Array<Option>;

}

class Option {

  public nome: string;

  public valorA: string;

  public valorB1: string;
  public valorB2: string;
  public valorBSelected: string;

}
