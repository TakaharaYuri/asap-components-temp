import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

declare const swal: any;

@Component({
  selector: 'app-detail-assessment',
  templateUrl: './detail.component.html'
})
export class DetailComponent {

  formId: number;
  assessmentId: number;
  private sub: any;

  @ViewChild('assessmentDetail',{static:false}) assessmentDetail;
  @ViewChild('assessmentSidebar',{static:false}) assessmentSidebar;

  constructor(private activatedRoute: ActivatedRoute) {
    this.getParam();
  }

  getParam(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      console.log('param', params['id']);
      this.formId = +params['id'];
    });
  }

  goToNextAssessment(event): void {
    this.assessmentSidebar.goToNextAssesment(event);
  }

  getAssessmentSelected(event) {
    this.assessmentId = event;
    this.assessmentDetail.getAssessment(event);
  }

}
