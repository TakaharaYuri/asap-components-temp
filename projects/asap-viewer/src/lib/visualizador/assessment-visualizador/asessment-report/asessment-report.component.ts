import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

declare const swal: any;

@Component({
  selector: 'app-report-assessment',
  templateUrl: './asessment-report.component.html'
})
export class AsessmentReportComponent {

  formId: number;
  assessmentId: number;
  private sub: any;

  @ViewChild('assessmentSidebar',{static:false}) assessmentSidebar;

  constructor(private activatedRoute: ActivatedRoute) {
    this.getParam();
  }

  getParam(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      console.log('param', params['id']);
      this.formId = +params['id'];
    });
  }
  
  goToNextAssessment(event): void {
    this.assessmentSidebar.goToNextAssesment(event);
  }

  getAssessmentSelected(event) {
    if (event) {
      this.assessmentId = event;
    }
  }

}
