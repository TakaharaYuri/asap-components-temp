import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { trigger, style, transition, animate, group } from '@angular/animations';
import { AssessmentService } from '../../../../@core/services/assessment.service';

declare const swal: any;

@Component({
  selector: 'app-report-assessment-inner',
  templateUrl: './assessment-report-inner.component.html',
  styleUrls: ['./assessment-report-inner.component.scss'],
  providers: [AssessmentService],
  animations: [
  trigger('itemAnim', [
    transition(':enter', [
      style({transform: 'translateY(-100%)'}),
      animate(350)
    ]),
    transition(':leave', [
      group([
        animate('0.2s ease', style({
          transform: 'translate(150px,25px)'
        })),
        animate('0.5s 0.2s ease', style({
          opacity: 0
        }))
      ])
    ])
  ])
]
})
export class AssessmentReportInnerComponent implements OnInit {
  total:number=0;
  showModal: boolean;
  icons:any=[];
  data:any={};
  themingColor:string="#fff";
  totalsub:number=0;
  porcentNumber:number=57;
  loggedInUser : string;
  public dashArray: string;
  public nivel: string;
  girls:any=[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private assessmentService: AssessmentService
    ) {
      document.body.style.backgroundColor = "white";
    }

  ngOnInit() {
    this.getUser();
    this.assessmentService.getReportIndicador().subscribe(result=>{
      console.log(result.data.total);
      this.icons = result.data.tema;
      //this.total = result.data.total;
      this.total = parseFloat(result.data.total.toFixed(1));
      this.nivel = result.data.total < 33 ? 'basic' : (result.data.total < 66 ? 'intermediary' : 'advanced');
      this.themingColor = result.data.total < 33 ? '#f27173' : (result.data.total < 66 ? '#f99e49' : '#83c1a1');
      this.icons.map((icon)=>{
        icon.dasharray = "0,100";
        icon.color = icon.percentil < 33 ? '#f27173' : (icon.percentil < 66 ? '#f99e49' : '#83c1a1');
        icon.done=false;
      });
      this.girls = [
        {name:'Básico', color: '#f27173',current:this.nivel=='basic' ? true : false},
        {name:'Intermediário', color: '#f99e49',current:this.nivel=='intermediary' ? true : false},
        {name:'Avançado', color: '#83c1a1',current:this.nivel=='advanced' ? true : false}
      ];
      console.log(this.girls);
      setTimeout(() => {
        this.icons.map((icon)=>{
          let total = icon.percentil / 140 * 100;
          let radius = 9;
          let totalLength = ( Math.PI * 3.2 * radius );
          let pathLength = ( total * 80 / 100 );
          icon.dasharray = `${ pathLength },100`;
          icon.done=true;
        });
      }, 1000);

    },error=>{
      console.log(error);
    })

  }


  getUser(){
    let loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedInUser = loggedUser.data.name;
  }


  openModal(item){
    this.showModal=true;
    this.data=item;
    console.log(this.data)
  }
  closeModal(){
    this.showModal=false;
    this.data={};
  }


}
