import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TagInputModule} from 'ngx-chips';
import {AssessmentVisualizadorRoute} from './assessment-visualizador.routing';
import {DetailInnerComponent} from './detail/detail-inner/detail-inner.component';
import {DetailComponent} from './detail/detail.component';
import {SidebarAssessmentComponent} from './sidebar/sidebar.component';
import {NavbarAssessmentComponent} from './navbar/navbar.component';
import {AsessmentReportComponent} from './asessment-report/asessment-report.component';
import {AssessmentReportInnerComponent} from './asessment-report/assessment-report-inner/assessment-report-inner.component';
import {AssessmentVisualizadorComponent} from './assessment-visualizador.component';
import {RadarComponent} from './graficos/radar/radar.component';
import { NgxNouisliderModule } from 'ngx-nouislider';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AssessmentVisualizadorRoute),
    FormsModule,
    ReactiveFormsModule,
    NgxNouisliderModule,
    TagInputModule
  ],
  declarations: [
    DetailComponent,
    DetailInnerComponent,
    SidebarAssessmentComponent,
    NavbarAssessmentComponent,
    AsessmentReportComponent,
    AssessmentReportInnerComponent,
    RadarComponent,
    AssessmentVisualizadorComponent
  ],
  exports: [
    DetailComponent,
    DetailInnerComponent,
    SidebarAssessmentComponent,
    NavbarAssessmentComponent,
    AsessmentReportComponent,
    AssessmentReportInnerComponent,
    RadarComponent,
    AssessmentVisualizadorComponent
  ]
})

export class AssessmentVisualizadorModule {
}
