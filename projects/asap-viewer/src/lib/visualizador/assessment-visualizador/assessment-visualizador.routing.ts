import {Routes} from '@angular/router';
import {DetailComponent} from './detail/detail.component';
import {AsessmentReportComponent} from './asessment-report/asessment-report.component';
import { AssessmentVisualizadorComponent } from './assessment-visualizador.component';

export const AssessmentVisualizadorRoute: Routes = [
  {
    path: 'assessment',
    component: AssessmentVisualizadorComponent,
    children: [
      {
        path: ':id',
        component: DetailComponent
      }, {
        path: ':id/relatorio',
        component: AsessmentReportComponent
      }
    ]
  }
];
