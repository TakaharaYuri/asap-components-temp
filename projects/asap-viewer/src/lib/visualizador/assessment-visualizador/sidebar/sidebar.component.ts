import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { Router } from '@angular/router';
import { AssessmentService } from '../../../@core/services/assessment.service';

declare const $: any;

@Component({
  selector: 'app-sidebar-assessment-visualizador',
  templateUrl: './sidebar.component.html',
  styleUrls: ['../assessment-visualizador.component.css'],
  providers: [AssessmentService]
})

export class SidebarAssessmentComponent implements OnInit {
  public assessmentArray: Array<Assessment>;

  @Input() form;
  @Output() public event = new EventEmitter<any>();

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  constructor(private _assessmentService: AssessmentService, private router: Router) {
  }

  assessmentSelected(assessment: Assessment) {
    this.event.emit(assessment.id);
  }

  ngOnInit() {
    this.getAssessmentList(this.form);
  }

  compare(a,b) {
    if (a.order < b.order)
      return -1;
    if (a.order > b.order)
      return 1;
    return 0;
  }

  getAssessmentList(id: number): void {
    this._assessmentService.fetch(id)
      .subscribe(res => {
        this.assessmentArray = [];

        res.data.sort(this.compare);

        for (let a of res.data) {
          let assessment = new Assessment;
          assessment.id = a.id;
          assessment.title = a.title;
          assessment.status = a.status;
          this.assessmentArray.push(assessment);
        }
        this.assessmentSelected(this.assessmentArray[0]);
      }, err => {
        console.log(err);
      });
  }

  goToNextAssesment(id: string) {
    let item = this.assessmentArray.find((item) => {
      return item.id == id
    });

    item.status = 1;

    let index = this.assessmentArray.indexOf(item);

    this.getNextAction();
  }

  getNextAction() {
    let assessmentArrayLength = this.assessmentArray.length;

    let i = 0;

    let unchecked = null;

    for (let ass of this.assessmentArray) {
      if (ass.status == 1) {
        i++;
      } else {
        if (unchecked == null) {
          unchecked = ass;
        }
      }
    }

    if (assessmentArrayLength == i) {
      this.router.navigate(['/visualizador/assessment/'+ this.form + '/relatorio']);
    } else {
      let index = this.assessmentArray.indexOf(unchecked);
      this.assessmentSelected(this.assessmentArray[index]);
    }
  }

  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      const ps = new PerfectScrollbar(elemSidebar, {wheelSpeed: 2, suppressScrollX: true});
    }
  }

  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }
}

class Assessment {
  public id: string;
  public title: string;
  public status: number;
}
