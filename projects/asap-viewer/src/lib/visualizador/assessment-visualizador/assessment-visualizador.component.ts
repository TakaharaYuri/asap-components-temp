import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AssessmentService } from '../../@core/services/assessment.service';

declare const $: any;

@Component({
  selector: 'app-login-cmp',
  templateUrl: './assessment-visualizador.component.html',
  styleUrls: ['./assessment-visualizador.component.css'],
  providers: [ AssessmentService ]
})

export class AssessmentVisualizadorComponent implements OnInit {
  test: Date = new Date();
  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;
  public percent: any;

  formId: number;
  assessmentId: number;
  private sub: any;

  @ViewChild('assessmentDetail',{static:false}) assessmentDetail;
  @ViewChild('assessmentSidebar',{static:false}) assessmentSidebar;

  constructor(
    private element: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private _assessmentService: AssessmentService
  ) {
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
    this.sub = this.route.params.subscribe(params => {
      console.log('param', params['id']);
       this.formId = +params['id'];
    });
  }

  ngOnInit() {
    const navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

    setTimeout(function() {
      $('.card').removeClass('card-hidden');
    }, 700);
    this.checkProgressBar();
  }

  getAssessmentSelected(event) {
    this.assessmentId = event;
    this.assessmentDetail.getAssessment(event);
  }

  goToNextAssessment(event) {
    this.assessmentSidebar.goToNextAssesment(event);
  }

  checkProgressBar() {
    const percent = sessionStorage.getItem('percent');

    if (!percent) {
      this.percent = 0;
    } else {
      this.percent = parseInt(percent);
    }
  }

  increasePercent(percent) {
    this.increase(percent);
  }

  increasePercentNav() {
    if (this.percent >= 100) {
      this.percent = 100;
    } else {
      this.percent = this.percent + 25;
    }
    this.increase(this.percent);
  }

  increase(value) {
    sessionStorage.setItem('percent', value);
  }

  sidebarToggle() {
    const toggleButton = this.toggleButton;
    const body = document.getElementsByTagName('body')[0];
    const sidebar = document.getElementsByClassName('navbar-collapse')[0];
    if (this.sidebarVisible === false) {
      setTimeout(function() {
        toggleButton.classList.add('toggled');
      }, 500);
      body.classList.add('nav-open');
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove('toggled');
      this.sidebarVisible = false;
      body.classList.remove('nav-open');
    }
  }

  public eventNavBar($event): any {
    console.log($event);
  }

}
