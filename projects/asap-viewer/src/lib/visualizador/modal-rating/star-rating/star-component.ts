import { Component, Inject, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

// Start Rating Component
@Component({
	selector: 'app-mini-visualizador-modal-rating-star',
	templateUrl: './star-component.html',
    styleUrls: ['../modal-rating.component.css']
})
export class StarRatingComponent implements OnInit {
	@Input() score;
	@Input() maxScore = 5;
	@Input() forDisplay = false;
	@Output() rateChanged = new EventEmitter();
    
  textNivel;
  range = [];
  marked = -1;

	constructor() { }

	ngOnInit() {
        for (var i = 0; i < this.maxScore; i++) {
        this.range.push(i);
        }
    }
  
  public mark = (index) => {
    this.marked = this.marked == index ? index - 1 : index;
    this.score = this.marked + 1;
    console.log(this.score)
    this.rateChanged.next(this.score);
    switch (this.score) {
      case 1:
        return this.textNivel = 'Detestei'
      case 2:
        return this.textNivel = 'Não Gostei' 
      case 3:
        return this.textNivel = 'Razoável'
      case 4:
        return this.textNivel = 'Gostei'
      case 5:
        return this.textNivel = 'Incrivel'
      default:
        return this.textNivel = ''
    }
  }

  public isMarked = (index) => {
    if (!this.forDisplay) {
      if (index <= this.marked) {
        return 'selected-start';
      }
      else {
        return 'no-selected-start';
      }
    }
    else {
      if (this.score >= index + 1) {
        return 'selected-start';
      }
      else if (this.score > index && this.score < index + 1) {
        return 'no-selected-start';
      }
      else {
        return 'no-selected-start';
      }
    }
  }

}
