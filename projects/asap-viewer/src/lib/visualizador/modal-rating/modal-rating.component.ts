import {Component, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TrilhaRatingService } from '../../@core/services/trilha/trilha-rating.service';

// Start Rating Component
@Component({
	selector: 'app-mini-visualizador-modal-rating',
	templateUrl: './modal-rating.component.html',
  styleUrls: ['./modal-rating.component.css'],
  providers: [ TrilhaRatingService ]
})
export class ModalStarRatingComponent {

  score : number = 0;
  
	constructor(
    private _trilhaRatingService: TrilhaRatingService,
    public dialogRef: MatDialogRef<ModalStarRatingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  onRateChange = (score) => {
    this.score = score;
  }

  save() {
    const form = { rating: this.score }
    this._trilhaRatingService.createResource(form, {router: this.data }).subscribe( res=> {
      this.dialogRef.close(true)
    })
  }

}
