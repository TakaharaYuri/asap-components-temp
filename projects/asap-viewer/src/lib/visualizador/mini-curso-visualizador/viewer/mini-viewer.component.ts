import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ModalStarRatingComponent } from '../../modal-rating/modal-rating.component';
import { SafeUrlPipe } from '../../../@core/pipes/safe-url.pipe';
import { VimeoService } from '../../../@core/services/vimeo.service';
import { MiniTrailCommentService } from '../../../@core/services/minicurso/minicurso-comments.service';
import { AmbienteVirtualService } from '../../../@core/services/ambiente-virtual.service';
import { VideoItemComponent, ImageItemComponent, PdfItemComponent, ExerciseComponent, MultiChoiceComponent } from '../../../@core/models/item-component';

declare var swal: any;
declare var $: any;

@Component({
  selector: 'app-mini-viewer',
  templateUrl: './mini-viewer.component.html',
  styleUrls: ['../mini-curso-visualizador.component.css', './mini-viewer.component.scss'],
  providers: [ VimeoService, MiniTrailCommentService, SafeUrlPipe ]
})
export class MiniViewerComponent implements OnInit {

  @Input() module: any;
  @Input() step: any;
  @Input() id;
  @Input() points;
  @Output() eventRefreshSideBar = new EventEmitter<any>();
  @Output() eventNavbar = new EventEmitter<any>();

  @ViewChild('conclusion',{static:false}) conclusion: any;

  componentArray: Array<any>;
  loader: boolean;
  form: FormGroup;
  comment: any[];
  commentArray: any;
  loggedUser: any;

  constructor(
    private _ambienteVirtualService: AmbienteVirtualService,
    private _sanitizer: DomSanitizer,
    private _vimeoService: VimeoService,
    private formBuilder: FormBuilder,
    private _CommentsService: MiniTrailCommentService,
    public dialog: MatDialog
  ) {
    this.form = this.formBuilder.group({
      comments: ['', Validators.required]
    })
  }

  ngOnInit() {
    this.getContent(this.step);

    const lu = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedUser = lu.data;
  }

  postComment() {
    const form = this.form.value;
    this._CommentsService.createResource(form, {router: this.step})
    .subscribe(res => {
      this.commentArray.unshift(res.data);
      this.form.reset();
    });
  }

  onDelete(comm) {

    swal({
      title: 'Você quer apagar esse comentário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.commentArray.indexOf(comm);
      this.commentArray.splice(index, 1);

      this._CommentsService.deleteResource(comm.id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  private sanitizer(url: string, type: string): SafeResourceUrl {

    let uri;

    if (!url) {
      return null;
    }

    if (type === 'youtube') {
      const code = this.getYoutubeID(url);
      uri = 'https://www.youtube.com/embed/' + code + '?rel=0&amp;showinfo=0';
    }

    if (type === 'vimeo') {
      const code = this.getVimeoID(url);
      uri = 'https://player.vimeo.com/video/' + code;
    }

    if (type === 'file') {
      uri = url;
    }

    if (type === 'stream') {
      uri = url;
    }

    if (type === 'image') {
      uri = url;
    }

    return this._sanitizer.bypassSecurityTrustResourceUrl(uri);
  }

  getYoutubeID(url: string): string {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    const match = url.match(regExp);
    return (match && match[7].length === 11) ? match[7] : '';
  }

  getVimeoID(url: string): string {
    // tslint:disable-next-line:max-line-length
    const regExp = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/;
    const match = url.match(regExp);
    return (match && match[3]) ? match[3] : '';
  }

  getContent(step: number) {
    this.loader = true;
    this._ambienteVirtualService.getContent(step)
      .subscribe(res => {
        const data = res.data[0].value;
        this.commentArray = res.data[0].comments.data.reverse();

        if (res['data'][0].value) {
          const itensJson = res['data'][0].value;

          this.componentArray = [];

          for (const item of itensJson) {
            if (item.type === 'video') {
              const videoComponent = new VideoItemComponent(
                item.source, item.source_path, this.sanitizer(item.source_path, item.source), item.title, item.button
              );
              this.componentArray.push(videoComponent);
            }
            if (item.type === 'text') {
              this.componentArray.push(item);
            }
            if (item.type === 'image') {
              const imageComponent = new ImageItemComponent(item.url, item.button);
              imageComponent.title = item.title;
              imageComponent.titleSize = (item.titleSize) ? item.titleSize : 16;
              const mainButtonDefault = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
              imageComponent.mainButton = (item.mainButton) ? item.mainButton : mainButtonDefault;
              this.componentArray.push(imageComponent);
            }
            if (item.type === 'pdf') {
              const pdfComponent = new PdfItemComponent(item.url, this.sanitizer(item.url, 'file'), item.title, item.button);
              this.componentArray.push(pdfComponent);
            }
            if (item.type === 'exercise') {
              const exerciseComponent = new ExerciseComponent(item.exercise);
              this.componentArray.push(exerciseComponent);
            }
            if (item.type === 'multi-choice') {
              let multiChoiceComponent = new MultiChoiceComponent();
              multiChoiceComponent = item;

              if (item.media) {
                multiChoiceComponent.media = item.media;
                multiChoiceComponent.link = this.sanitizer(item.url, item.media.toLowerCase());
              }

              this.componentArray.push(multiChoiceComponent);
            }
            if (item.type === 'intro') {
              this.componentArray.push(item);
            }
            if (item.type === 'conclusion') {
              this.componentArray.push(item);
            }
            if (item.type === 'challenge') {
              for (const m of item.medias) {
                if (m.type === 'youtube') {
                  m.link = this.sanitizer(m.url, 'youtube');
                  m.thumb = 'https://img.youtube.com/vi/' + this.getYoutubeID(m.url) + '/hqdefault.jpg';
                }
                if (m.type === 'vimeo') {
                  m.link = this.sanitizer(m.url, 'vimeo');

                  m.thumb = 'https://i.vimeocdn.com/video/default_295x166.webp';
                  this._vimeoService.getThumb(m.url).then(response => {
                    m.thumb = response;
                  });
                }
              }

              this.componentArray.push(item);
            }

            if (item.type === 'correct-incorrect') {
              this.componentArray.push(item);
            }
            if (item.type === 'multiple-questions') {
              this.componentArray.push(item);
            }
            if (item.type === 'table') {
              this.componentArray.push(item);
            }

            if (item.type === 'star-point') {
              item.points = this.points;
              this.componentArray.push(item);
            }
            if (item.type === 'download') {
              this.componentArray.push(item);
            }
            if (item.type === 'upload') {
              this.componentArray.push(item);
            }
            if (item.type === 'rating') {
              this.componentArray.push(item);
            }

            if (item.type === 'forum') {
              console.log(item);
              this.componentArray.push(item);
            }

          }

        }

        this.loader = false;
      }, err => {
        console.log(err);
      });
  }

  private notificate(): any {
    swal({
      title: 'Erro',
      text: 'Erro ao carregar conteúdo. Recarregue sua página.',
      type: 'warning',
      confirmButtonClass: 'btn btn-success',
    });
  }

  goToAnchor(anchor: string) {
    const $div = $('#' + anchor);
    const top = $div.offset().top + 48;

    $('html, body').animate({ scrollTop: top }, 1000);
  }

  showButton(item: any) {
    return (
      item.button &&
      item.button !== '' &&
      item.type !== 'intro' &&
      item.type !== 'conclusion' &&
      item.type !== 'challenge'
    );
  }

  emitEvent(event: string) {
    this.eventNavbar.emit(event);
  }

  ratingEvent() {
    const dialogRef = this.dialog.open(ModalStarRatingComponent, {
      width: '350px',
      disableClose: true,
      data: this.id
    });

    dialogRef.afterClosed().subscribe(result => {
      this.conclusion.onNext();
    });
  }
}
