import { Component, OnInit } from '@angular/core';
import { AreaPermissionService } from '../../../@core/services/area-permission.service';
import { TutorService } from '../../../@core/services/tutor/tutor.service';

declare const $: any;

@Component({
  selector: 'app-mini-sidemenu',
  templateUrl: './mini-sidemenu.component.html',
  styleUrls: ['./mini-sidemenu.component.css'],
  providers: [ AreaPermissionService ]
})

export class MiniCursoSideMenuComponent implements OnInit {

  public isTutor = false;
  public areas;

  constructor(
      public tutorService:TutorService,
      private _areaPermissionService: AreaPermissionService
  ) {
      this.tutorService.profile().subscribe((response) => {
          this.isTutor = response.tutor;
      })
      this._areaPermissionService.getResources().subscribe( res=> {
        this.areas = res.data;
      })
  }
  
  ngOnInit() {

    $(document).ready(function () {

      $('#sidemenuCollapse').on('click', function () {
        $('.sidemenu, .content, .overlay').toggleClass('active');
      });

      $('.overlay').on('click', function () {
        $('.sidemenu, .content, .overlay').removeClass('active');
      });

      $('.components').on('click', function () {
        $('.sidemenu, .content, .overlay').removeClass('active');
      });

      $('.navbar-brand').on('click', function () {
        $('.sidemenu, .content, .overlay').removeClass('active');
      });

    });

  }

}
