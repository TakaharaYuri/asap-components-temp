import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mini-slider',
  templateUrl: 'slider.component.html',
  styleUrls: ['../mini-curso-visualizador.component.css']
})

export class MiniSliderComponent implements OnInit {

  @Input() data: any;
  @Input() current: any;

  indexes: any;

  // ASAP
  @Output()
  public eventNavbar = new EventEmitter<any>();

  public setIndexes(data, current) {
    this.indexes = {
      current: data.indexOf(current) + 1,
      total: data.length
    }
  }

  ngOnInit() {
    this.setIndexes(this.data, this.current);
    console.log(this.indexes.current)
  }

  goToPrev() {
    this.eventNavbar.emit('previous');
  }

  goToNext() {
    this.eventNavbar.emit('next');
  }

  markAsDone() {
    this.eventNavbar.emit('done');
  }

  back(): void {
    window.history.back();
  }
}
