import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import * as moment from 'moment';
import { ForumService } from '../../../@core/services/forum/forum.service';
import { ForumResponseService } from '../../../@core/services/forum/response.service';
import { ForumVoteService } from '../../../@core/services/forum/forum-vote.service';
import { ForumTrilhaService } from '../../../@core/services/forum/forum-trilha.service';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-modules-visualizar-mini-curso-forum',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
  providers: [ ForumService, ForumResponseService, ForumVoteService, ForumTrilhaService ]
})
export class VisualizarForumComponent implements OnInit {

  @Input() trilha: number;
  id: number;

  form: FormGroup;
  formUpdate: FormGroup;
  loading = true;
  response: any;
  forum: any;
  loggedUser: any;
  pagination: any;

  score = 0;
  displayRatingScore = 0;

  constructor(
    private formBuilder: FormBuilder,
    private _ForumService: ForumService,
    private _ForumResponseService: ForumResponseService,
    private _ForumVoteService: ForumVoteService,
    private _forumTrilhaService: ForumTrilhaService
  ) {
    moment.locale('pt-br');
    this.initForm();
  }

  ngOnInit() {
    this.getForum(this.trilha);
    this.getUserLogged();
  }

  getUserLogged(): any {
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedUser = loggedUser.data;
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [''],
      resposta: ['']
    })
  }

  updatForm() {
    this.formUpdate = this.formBuilder.group({
      id: [''],
      resposta: ['']
    })
  }

  getForum(id) {
    this._forumTrilhaService.getResource(id).subscribe(res => {
      console.log(res)
      if (res.data.length > 0) {
        this.forum = res.data[0];
        this.id = this.forum.id;
        this.getResponse(this.forum.id);
      } else {
        this.loading = false;
      }
    })
  }

  getResponse(id) {
    this._ForumResponseService.getResource(id).subscribe(res => {
      this.response = res.data;
      this.loading = false;
      this.pagination = res.meta.pagination
      this.updatForm();
      console.log('RESP ->', res);
    })
  }

  save() {
    const form = this.form.value;
    this._ForumService.createResource(form, {router: this.id}).subscribe(res => {
      this.getResponse(this.id);
      this.ResetPost();
    })
  }

  ResetPost() {
    this.form.reset();
    $('#answer').collapse('hide');
  }

  onRemove(forum) {
    swal({
        title: 'Você quer remover esta pergunta?',
        text: 'Você não poderá reverter essa ação depois de confirmado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não'
    }).then(
        (result) => {
            this._ForumResponseService.deleteResource(forum.id).subscribe(res => {
              this.getResponse(this.id);
            })
        },
        (cancel) => {}
    );
  }

  nextPage(event) {
    let number = event + 1
    this._ForumResponseService.getResource(this.id, {query:'page='+number}).subscribe(
      res => {
        console.log('EOEWE', res.data);
        
        for(let item of res.data) {
          this.response.push(item);
        }

        this.pagination = res.meta.pagination;        
      }
    )
  }

  onClose(forum) {
    this.formUpdate.reset();
    return forum.disable = false;
  }

  onEdit(forum) {
    this.formUpdate.setValue({id: forum.id, resposta: forum.resposta})
    return forum.disable = true;
  }

  updateForm() {
    const form = this.formUpdate.value;
    this._ForumService.updateResource(form).subscribe( res => {
      this.getResponse(this.id);
      this.ResetPost();
    })
  }

  vote(item, number) {
    const form = { pontos: number }
    this._ForumVoteService.createResource(form, {router: item.id}).subscribe(res => {
      this.getResponse(this.id);
    })
  }

  getHidden(item) {
    return item.hidden = !item.hidden;
  }
}
