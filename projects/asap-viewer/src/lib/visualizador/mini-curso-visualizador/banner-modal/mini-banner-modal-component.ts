import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'banner-modal-component',
  templateUrl: 'mini-banner-modal.html',
})
export class MiniModalTrilhaBanner {

  constructor(
    public dialogRef: MatDialogRef<MiniModalTrilhaBanner>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
