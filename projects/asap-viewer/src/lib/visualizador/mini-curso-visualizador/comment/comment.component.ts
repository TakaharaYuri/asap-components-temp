import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommentsService } from '../../../@core/services/comments/comments.service';

declare var swal: any;

@Component({
  selector: 'app-modules-visualizar-mini-curso-comments',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  providers: [ CommentsService ]
})
export class CommentsComponent implements OnInit  {

  @Input() id;

  comment: any[];
  form: FormGroup;
  onPosting: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private _CommentsService: CommentsService

  ) {
    this.initForm();
  }

  ngOnInit() {
    this.getComments(this.id);
  }

  initForm() {
    this.form = this.formBuilder.group({
      comment: ['', Validators.required]
    })
  }

  getComments(id) {
    this._CommentsService.getResource(id)
      .subscribe(res => {
        this.comment = res.data;
      })
  }

  postComment() {
    this.onPosting = true;

    const form = this.form.value;
    form.type = 'desenvolvimento';

    this._CommentsService.createResource(form, {router: this.id})
      .subscribe(res => {
        this.comment.unshift(res.data);
        this.form.reset();
        this.onPosting = false;
      })
  }

  onRemove(item) {

    swal({
      title: 'Você quer apagar esse comentário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.comment.indexOf(item);
      this.comment.splice(index, 1);

      this._CommentsService.deleteResource(item.id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );




  }
}
