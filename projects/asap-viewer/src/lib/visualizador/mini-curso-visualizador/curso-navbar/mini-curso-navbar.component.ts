import {
  Component,
  OnInit,
  Renderer,
  ViewChild,
  ElementRef,
  Directive,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { NotificationService } from '../../../@core/services/notifications/notifications.service';
import { AmbienteVirtualService } from '../../../@core/services/ambiente-virtual.service';

const misc: any = {
  navbar_menu_visible: 0,
  active_collapse: true,
  disabled_collapse_init: 0,
};

declare var $: any;

@Component({
  selector: 'app-navbar-mini-curso',
  templateUrl: 'mini-curso-navbar.component.html',
  styleUrls: ['./mini-curso-navbar.component.css'],
  providers: [ NotificationService ]
})

export class MiniCursoNavbarComponent implements OnInit {
  private listTitles: any[];
  location: Location;
  private nativeElement: Node;
  private toggleButton: any;
  private sidebarVisible: boolean;
  platform: string;
  urlEscola: string;
  
  public notification: any;
  public total: number;  
  public loggedUser: any;

  @Input() private data: any;
  @Input() private current: any;

  indexes: any;

  // ASAP
  @Output()
  public eventNavbar = new EventEmitter<any>();

  public checkIfAllHaveDoneVar: boolean;

  @ViewChild('app-navbar-curso',{static:false}) button: any;

  constructor(
    location: Location, 
    private renderer: Renderer, 
    private element: ElementRef,
    private ambienteVirtualService: AmbienteVirtualService, 
    private router: Router,    
    private _NotificationService: NotificationService

  ) {
    this.location = location;
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
    this.checkIfAllHaveDoneVar = false;
    this.platform = sessionStorage.getItem('platform');
    this.urlEscola = this.getUrlEscola();
  }

  public setIndexes(data, current) {
    this.indexes = {
      current: data.indexOf(current)+1,
      total: data.length
    }
  }

  getUrlEscola(): string {
    const uris = [
    {
      platform: 'asta',
      uri: '/painel/escola'
    },
    {
      platform: 'fitness-link',
      uri: '/painel/meus-cursos'
    },
    {
      platform: 'lele',
      uri: '/trilhas'
    }];

    let escolaUri = uris.find(item => {
      return item.platform === this.platform;
    })

    return (escolaUri) ? escolaUri.uri : '/';
  }

  ngOnInit() {

    this.getLoggedUser();
    this.getNotification();

    setInterval(() => {
      this.getNotification();
    }, 300000)
    
    this.setIndexes(this.data, this.current);

    const navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    if ($('body').hasClass('sidebar-mini')) {
      misc.sidebar_mini_active = true;
    }
    if ($('body').hasClass('hide-sidebar')) {
      misc.hide_sidebar_active = true;
    }
    $('#minimizeSidebar').click(function () {
      if (misc.sidebar_mini_active === true) {
        $('body').removeClass('sidebar-mini');
        misc.sidebar_mini_active = false;

      } else {
        setTimeout(function () {
          $('body').addClass('sidebar-mini');

          misc.sidebar_mini_active = true;
        }, 300);
      }

      // we simulate the window Resize so the charts will get updated in realtime.
      const simulateWindowResize = setInterval(function () {
        window.dispatchEvent(new Event('resize'));
      }, 180);

      // we stop the simulation of Window Resize after the animations are completed
      setTimeout(function () {
        clearInterval(simulateWindowResize);
      }, 1000);
    });
    $('#hideSidebar').click(function () {
      if (misc.hide_sidebar_active === true) {
        setTimeout(function () {
          $('body').removeClass('hide-sidebar');
          misc.hide_sidebar_active = false;
        }, 300);
        setTimeout(function () {
          $('.sidebar').removeClass('animation');
        }, 600);
        $('.sidebar').addClass('animation');

      } else {
        setTimeout(function () {
          $('body').addClass('hide-sidebar');
          // $('.sidebar').addClass('animation');
          misc.hide_sidebar_active = true;
        }, 300);
      }

      // we simulate the window Resize so the charts will get updated in realtime.
      const simulateWindowResize = setInterval(function () {
        window.dispatchEvent(new Event('resize'));
      }, 180);

      // we stop the simulation of Window Resize after the animations are completed
      setTimeout(function () {
        clearInterval(simulateWindowResize);
      }, 1000);
    });
  }

  isMobileMenu() {
    if ($(window).width() < 991) {
      return false;
    }
    return true;
  };

  sidebarOpen() {
    const toggleButton = this.toggleButton;
    const body = document.getElementsByTagName('body')[0];
    setTimeout(function () {
      toggleButton.classList.add('toggled');
    }, 500);
    body.classList.add('nav-open');

    this.sidebarVisible = true;
  };

  sidebarClose() {
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton.classList.remove('toggled');
    this.sidebarVisible = false;
    body.classList.remove('nav-open');
  };

  sidebarToggle() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  };

  getTitle() {
    return 'Cursos';
  }

  getPath() {
    return this.location.prepareExternalUrl(this.location.path());
  }

  goToPrev() {
    this.eventNavbar.emit('previous');
  }

  goToNext() {
    this.eventNavbar.emit('next');
  }

  markAsDone() {
    this.eventNavbar.emit('done');
  }

  back(): void {
    window.history.back();
  }

  getNotification() {
    this._NotificationService.getResources().subscribe( res=> {
      this.notification = res.data;
      this.total = res.total;
    })
  }

  goTo(item) {
    this._NotificationService.createResource({id: item.id}, {router: item.id}).subscribe( res => {
      if(item.type === 0) {
        this.router.navigate(['/training', item.id]);
      } else if(item.type === 2) {
        this.router.navigate(['/visualizador/mini-etapa', item.id]);
      } else if(item.type === 3) {
        this.router.navigate(['/visualizador/etapa-develop', item.id]);
      }
    })
  }

  getLoggedUser(): any {
    const session = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedUser = session.data;
  }
}
