import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AmbienteVirtualService } from '../../@core/services/ambiente-virtual.service';
import { bannerService } from '../../@core/services/banner.service';
import { MiniTrailService } from '../../@core/services/minicurso/minicurso.service';
import { DevelopmentService } from '../../@core/services/development/development.service';
import { ArticlesService } from '../../@core/services/articles/articles.service';
import { TrilhaService } from '../../@core/services/trilha/trilha.service';
import { TrilhaItemService } from '../../@core/services/trilha/trilha-item.service';

declare const $: any;
declare var swal: any;

@Component({
  selector: 'app-mini-curso-visualizador',
  templateUrl: './mini-curso-visualizador.component.html',
  styleUrls: ['./mini-curso-visualizador.component.css'],
  providers: [
    AmbienteVirtualService,
    bannerService,
    MiniTrailService,
    DevelopmentService,
    ArticlesService,
    TrilhaService,
    TrilhaItemService
  ]
})

export class MiniCursoVisualizadorComponent {

  public id: number;
  public loader: boolean;
  public loaderList: boolean;
  public bannerArray: any;
  public playlist: any;
  public playlistBuffer: any;
  public playlistPage: number;
  public playlistItemFinal: number;
  public score: any;
  public title: string;
  public checkModule;
  public now: Date = new Date();

  // miniStep - 0 == mini-etapa; 1 == etapa-develop; 2 == etapa-articles;
  public miniStep: number;

  @ViewChild('cursoViewer',{static:false}) cursoViewer;
  @ViewChild('cursoNavBar',{static:false}) cursoNavBar;

  public data: any[] = [];
  public trail: any;
  public module: number;

  public current: any;

  public index: number;
  public helperText: string;

  constructor(
    private ambienteVirtualService: AmbienteVirtualService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _trilhaService: TrilhaService,
    public dialog: MatDialog,
    private _MiniTrailService: MiniTrailService,
    private _DevelopmentService: DevelopmentService,
    private _ArticlesService: ArticlesService,
    private _trilhaItemService: TrilhaItemService
  ) {
    this.loader = true;
    this.loaderList = true;
    this.index = 0;
    this.playlist = [];
    this.playlistPage = 0;
    this.activatedRoute.params
      .subscribe(params => {
        this.id = params['id'];
        this.bootstrap();
        this.getStepListByIdTrail(this.id);
      });
  }

  bootstrap() {
    const path = this.activatedRoute.snapshot.routeConfig.path.split('/');
    if (path[0] === 'mini-etapa') {
      this.title = 'influenciadores';
      this.miniStep = 0;
    } else if (path[0] === 'etapa-develop') {
      this.title = 'reflexões  e experiências';
      this.miniStep = 1;
    } else if (path[0] === 'etapa-artigo') {
      this.title = 'Leitura e conhecimento ';
      this.miniStep = 2;
    }
    this.helperText = 'Este painel apresenta o total de pontos do ambiente ' + this.title + '. Clique em relatórios para visualizar seu progresso em todos os ambientes da Universidade Smart Fit.'

  }

  getInfoTrail(id: number): void {
    this._trilhaService.getResource(id)
    .subscribe(res => {
      if (res.data.length > 0) {
        this.trail = res.data[0];
      }
    });
  }

  getStepListByIdTrail(idTrail: number): void {
    console.log('getStepListByIdTrail: idTrail', idTrail);
    this.ambienteVirtualService.getEtapasByTrail(idTrail)
      .subscribe(res => {
          if (res.data.length === 0) {
            swal('Ops...', 'Esse curso ainda não tem conteúdo', 'warning').then(() => {
              this.router.navigate(['/home']);
            });
            return;
          } else {
            this.checkModule = res.data[0];
            if (res.data[this.index]) {
              this.getStepData(res.data[this.index].id);
            } else {
              console.log(res.data);
            }
            if (this.miniStep === 0) {
              this.getPlaylistMini(this.id)
            } else if (this.miniStep === 1) {
              this.getPlaylistDevelop(this.id)
              this.getTrilhaItem(this.id);
            } else if (this.miniStep === 2) {
              this.getPlaylistArticles(this.id);
            }
            this.getInfoTrail(this.id);
          }
        }
      );
  }

  getPointsStepListByIdTrail(idTrail: number): void {
    let resources;
    if (this.miniStep === 0) {
      resources = this._MiniTrailService.getResource(idTrail);
    } else if (this.miniStep === 1) {
      resources = this._DevelopmentService.getResource(idTrail);
    } else if (this.miniStep === 2) {
      resources = this._ArticlesService.getResource(idTrail);
    }
    console.log('getPointsStepListByIdTrail: this.score', this.score);
    resources.subscribe(response => {
      console.log('getPointsStepListByIdTrail: response.pontos', response.pontos);
      this.score = response.pontos;
    });
  }

  getTrilhaItem(id: number): void {
    this._trilhaItemService.getResource(id)
      .subscribe(res => {
        this.playlist = res.data;
        this.loaderList = false;
      });
  }

  compare(a, b) {
    if (a.order < b.order) {
      return -1;
    }
    if (a.order > b.order) {
      return 1;
    }
    return 0;
  }

  getStepData(id): void {
    this.loader = true;
    this.module = id;
    this.ambienteVirtualService.getSubEtapa(id)
      .subscribe(res => {
          res.data.sort(this.compare);
          if (res.data.length > 0) {
            this.data = res.data;
            this.getcurrent();
          }
          this.loader = false;
        }, err => {
          this.notificate();
        });
  }

  getcurrent(): void {
    this.current = this.data.find(item => {
      return item.status === 0;
    });
    if (!this.current) {
      this.current = this.data[0];
    }
  }

  getPlaylistMini(id) {
    this.playlist = [];
    this.playlistPage = 0;

    this._MiniTrailService.getResource(id)
      .subscribe( res => {
        this.playlistBuffer = res.data;
        this.insertVideos();
        this.score = res.pontos;
        this.loaderList = false;
      })
  }

  getPlaylistDevelop(id) {
    this._DevelopmentService.getResource(id)
      .subscribe( res => {
        this.score = res.pontos;
      })
  }

  getPlaylistArticles(id) {
    this.playlist = [];
    this.playlistPage = 0;

    this._ArticlesService.getResource(id)
      .subscribe( res => {
        this.playlistBuffer = res.data;
        this.insertVideos();
        this.score = res.pontos;
        this.loaderList = false;
      })
  }

  notificate(): any {
    swal({
      title: 'Erro',
      text: 'Erro ao carregar conteúdo. Recarregue sua página.',
      type: 'warning',
      confirmButtonClass: 'btn btn-success',
    });
  }

  eventNavBar($event): any {
    if ($event === 'next') {
      this.next();
    } else if ($event === 'previous') {
      this.previous();
    } else if ($event === 'done') {
      this.done();
    }
  }

  currentIndex(): number {
    return this.data.indexOf(this.current);
  }

  next(): any {
    console.log('next');
    this.data[this.currentIndex()].status = 1;
    if (this.data.length === this.currentIndex() + 1) {
      this.done();
    } else {
      const newCurrent = this.data[this.currentIndex() + 1];
      this.getSubEtapaByEvent(newCurrent);
    }
  }

  previous(): void {
    const newCurrent = this.data[this.currentIndex() - 1];
    this.getSubEtapaByEvent(newCurrent);
  }

  done(): any {
    this.checkAsRead(this.data[this.currentIndex()].id);
    const path = this.activatedRoute.snapshot.routeConfig.path.split('/');

    if (this.miniStep === 0) {
      this._MiniTrailService.getResources()
        .subscribe( res => {
          const actual = res.data.find(i => {
            return i.id === +this.id;
          });
          const index = res.data.indexOf(actual);
          if (index + 1 === res.data.length) {
            const newIndex = res.data[0];
            this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
          } else {
            const newIndex = res.data[index + 1];
            this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
          }
        }, err => {
        })
    } else if (this.miniStep === 1) {
      const playlistCounter = this.playlist.length;
      if (this.index + 1 === playlistCounter) {
        this._DevelopmentService.getResources()
        .subscribe( res => {
          const actual = res.data.find(i => {
            return i.id === +this.id;
          });
          const index = res.data.indexOf(actual);
          if (index + 1 === res.data.length) {
            const newIndex = res.data[0];
            this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
          } else {
            const newIndex = res.data[index + 1];
            this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
          }
        }, err => {
        })
      } else {
        this.index++;
        this.getStepData(this.playlist[this.index].id);
      }
    } else if (this.miniStep === 2) {
      this._ArticlesService.getResources()
      .subscribe( res => {
        const actual = res.data.find(i => {
          return i.id === +this.id;
        });
        const index = res.data.indexOf(actual);
        if (index + 1 === res.data.length) {
          const newIndex = res.data[0];
          this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
        } else {
          const newIndex = res.data[index + 1];
          this.router.navigate(['/visualizador/' + path[0], newIndex.id]);
        }
      }, err => {
      })
    }

  }

  getTrails() {
  }

  back(): void {
    window.history.back();
  }

  getUrlEscola(): string {
    const platform = sessionStorage.getItem('platform');

    const uris = [
    {
      platform: 'asta',
      uri: '/painel/escola'
    },
    {
      platform: 'fitness-link',
      uri: '/painel/cursos'
    },
    {
      platform: 'lele',
      uri: '/trilhas'
    }];

    const escolaUri = uris.find(item => {
      return item.platform === platform;
    })

    return (escolaUri) ? escolaUri.uri : '/';
  }

  getSubEtapaByEvent($event): void {
    const oldCurrent = this.current;

    this.checkAsRead(oldCurrent.id);

    this.current = $event;
    this.cursoViewer.getContent(this.current.id);
    this.cursoNavBar.setIndexes(this.data, this.current);
  }

  checkAsRead(idSubStep: number): void {
    if (idSubStep !== undefined) {
      this.ambienteVirtualService.registerNewProgress(idSubStep)
        .subscribe(
          res => {
            this.getPointsStepListByIdTrail(this.id);
            this.getInfoTrail(this.id);
          },
          err => {
          });
    }
  }

  setModule(index: number) {
    this.index = index;
    this.getStepData(this.playlist[index].id);
  }

  checkDateBetween(date) {
    date = new Date(date);
    const result = this.now.getTime() >= date.getTime();
    return result;
  }

  checkDate(date) {
    date = new Date(date);
    const result = this.now.getTime() > date.getTime();
    return result;
  }

  insertVideos() {
    const offset = this.playlistPage * 3;
    const end = offset + 3;
    const partial = this.playlistBuffer.slice(offset, end);

    this.playlistItemFinal = end;

    this.playlist.push(... partial);
    this.playlistPage++;
  }

}
