import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'banner-modal-component',
  templateUrl: 'banner-modal.html',
})
export class modalTrilhaBanner {

  constructor(
    public dialogRef: MatDialogRef<modalTrilhaBanner>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
