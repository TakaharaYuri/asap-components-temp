import {
  Component,
  OnInit,
  Renderer,
  ViewChild,
  ElementRef,
  Directive,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { AmbienteVirtualService } from '../../../@core/services/ambiente-virtual.service';

const misc: any = {
  navbar_menu_visible: 0,
  active_collapse: true,
  disabled_collapse_init: 0,
};

declare var $: any;

@Component({
  selector: 'app-navbar-curso',
  templateUrl: 'curso-navbar.component.html',
  styleUrls: ['../curso-visualizador.component.scss']
})

export class CursoNavbarComponent implements OnInit {
  location: Location;
  platform: string;
  urlEscola: string;

  @Input() private data: any;
  @Input() private current: any;

  indexes: any;

  // ASAP
  @Output()
  public eventNavbar = new EventEmitter<any>();

  public checkIfAllHaveDoneVar: boolean;

  public menuItems: any[];

  @ViewChild('app-navbar-curso',{static:false}) button: any;

  constructor(
    location: Location,
    private renderer: Renderer,
    private element: ElementRef,
    private ambienteVirtualService: AmbienteVirtualService,
    private _router: Router
  ) {
    this.location = location;
    this.checkIfAllHaveDoneVar = false;
    this.platform = sessionStorage.getItem('platform');
    this.urlEscola = this.getUrlEscola();
  }

  public setIndexes(data, current) {
    this.indexes = {
      current: data.indexOf(current) + 1,
      total: data.length
    }
  }

  getUrlEscola(): string {
    const uris = [
    {
      platform: 'asta',
      uri: '/painel/escola'
    },
    {
      platform: 'fitness-link',
      uri: '/painel/meus-cursos'
    },
    {
      platform: 'lele',
      uri: '/trilhas'
    }];

    const escolaUri = uris.find(item => {
      return item.platform === this.platform;
    })

    return (escolaUri) ? escolaUri.uri : '/';
  }

  ngOnInit() {
    this.menuItems = this.data;
    this.setIndexes(this.data, this.current);
}

  back(): void {
    window.history.back();
  }
}
