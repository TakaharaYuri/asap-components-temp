import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { AmbienteVirtualService } from '../../../@core/services/ambiente-virtual.service';

declare const $: any;

// Metadata
export interface RouteInfo {
  path?: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  done?: boolean;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab?: string;
  icontype?: string;
  done?: boolean;
  type?: string;
}

@Component({
  selector: 'app-sidebar-curso-visualizador',
  templateUrl: 'curso-sidebar.component.html',
  styleUrls: ['../curso-visualizador.component.scss']
})

export class CursoSidebarComponent implements OnInit {

  @Input() public data: any;
  @Input() public current: any;

  @Output() public event = new EventEmitter<any>();

  public menuItems: any[];
  urlEscola: string;
  platform: string;
  logo: string;

  constructor(private _ambienteVirtualService: AmbienteVirtualService) {
    this.platform = sessionStorage.getItem('platform');
    this.urlEscola = this.getUrlEscola();

    if(this.platform == 'asta') {
      this.logo = '/assets/img/clientes/asta/logo.png';
    }else if(this.platform == 'fitness-link') {
      this.logo = '/assets/img/fitness-link-logo.png';
    }

  }

  getUrlEscola(): string {
    if(this.platform == 'asta') {
      return '/painel/escola';
    }else if(this.platform == 'fitness-link') {
      return '/painel/curso';
    }else if(this.platform == 'lele') {
      return '/trilhas';
    }
    return '/';
  }

  ngOnInit() {
    this.menuItems = this.data;

    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
        $('#sidemenu, #content').toggleClass('active');
      });
    });

  }

  public menuNavigate(obj): void {
    if(this.current === obj) {
      return;
    }
    this.current.status = '1';
    this.event.emit(obj);
  }

}
