import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {modalTrilhaBanner} from './banner-modal/banner-modal-component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AmbienteVirtualService } from '../../@core/services/ambiente-virtual.service';
import { bannerService } from '../../@core/services/banner.service';

declare const $: any;
declare var swal: any;

@Component({
  selector: 'app-curso-visualizador',
  templateUrl: './curso-visualizador.component.html',
  styleUrls: [ './curso-visualizador.component.scss' ],
  providers: [ AmbienteVirtualService, bannerService ],
  encapsulation: ViewEncapsulation.None
})

export class CursoVisualizadorComponent implements OnInit {

  public id: number;
  public loader: boolean;
  public bannerArray: any;

  @ViewChild('cursoViewer',{static:false}) cursoViewer;
  @ViewChild('cursoNavBar',{static:false}) cursoNavBar;

  public data: any[] = [];

  public current: any;
  public module: number;

  constructor(
    private ambienteVirtualService: AmbienteVirtualService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _bannerService: bannerService,
    public dialog: MatDialog
  ) {
    this.loader = true;
  }

  ngOnInit() {
    this.getParams();
  }

  private getParams(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.getStepData();
      this.verificaBanner();
    });
  }

  verificaBanner(): void {
    this._bannerService.verifyBannerTrilha(this.id)
    .subscribe(res => {
      this.bannerArray = res.data;
      if (this.bannerArray.length > 0 && this.bannerArray[0].status === 0) {
        this.openDialog(this.bannerArray);
      }
    });
  }

  openDialog(data: any): void {

    const dialogRef = this.dialog.open(modalTrilhaBanner, {
      maxWidth: '100%',
      data: data,
      panelClass: 'modalBanner'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  compare(a, b) {
    if (a.order < b.order) {
      return -1;
    }
    if (a.order > b.order) {
      return 1;
    }
    return 0;
  }

  private getStepData(): void {
    this.loader = true;
    this.module = this.id;
    this.ambienteVirtualService.getSubEtapa(this.id).subscribe(
        res => {
          res.data.sort(this.compare);

          if (res.data.length > 0) {
            this.data = res.data;
            this.getcurrent();
          }
          this.loader = false;
        }, err => {
          console.log("Erro",err);
          this.notificate();
        }
    );
  }

  public getcurrent(): void {
    this.current = this.data.find(item => {
      return item.status === 0;
    });
    if (!this.current) {
      this.current = this.data[0];
    }
  }

  private notificate(): any {
    swal({
      title: 'Erro',
      text: 'Erro ao carregar conteúdo. Recarregue sua página.',
      type: 'warning',
      confirmButtonClass: 'btn btn-success',
    });
  }

  public eventNavBar($event): any {
    if ($event === 'next') {
      this.next();
    } else if ($event === 'previous') {
      this.previous();
    } else if ($event === 'done') {
      this.done();
    }
  }

  public currentIndex(): number {
    return this.data.indexOf(this.current);
  }

  public next(): any {
    console.log(this.currentIndex());
    console.log(this.data.length);
    if ((this.currentIndex() + 1) === this.data.length) {
      this.done();
    } else {
      this.data[this.currentIndex()].status = 1;
      const newCurrent = this.data[this.currentIndex() + 1];
      this.getSubEtapaByEvent(newCurrent);
    }
  }

  public previous(): void {
    const newCurrent = this.data[this.currentIndex() - 1];
    this.getSubEtapaByEvent(newCurrent);
  }

  public done(): any {
    this.checkAsRead(this.data[this.currentIndex()].id);
    window.history.back();
    // this.router.navigate([this.getUrlEscola()]);
  }

  back(): void {
    window.history.back();
  }

  getUrlEscola(): string {
    const platform = sessionStorage.getItem('platform');

    const uris = [
    {
      platform: 'asta',
      uri: '/painel/escola'
    },
    {
      platform: 'fitness-link',
      uri: '/painel/cursos'
    },
    {
      platform: 'lele',
      uri: '/trilhas'
    }];

    const escolaUri = uris.find(item => {
      return item.platform === platform;
    })

    return (escolaUri) ? escolaUri.uri : '/';
  }

  getSubEtapaByEvent($event): void {
    const oldCurrent = this.current;

    this.checkAsRead(oldCurrent.id);

    this.current = $event;
    this.cursoViewer.getContent(this.current.id);
    this.cursoNavBar.setIndexes(this.data, this.current);
  }

  public checkAsRead(idSubStep: number): void {
    if (idSubStep !== undefined) {
      this.ambienteVirtualService.registerNewProgress(idSubStep)
        .subscribe(
          res => {
          },
          err => {
          });
    }
  }

}
