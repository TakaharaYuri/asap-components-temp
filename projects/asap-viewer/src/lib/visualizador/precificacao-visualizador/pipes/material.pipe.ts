import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'material',
    pure: false
})
export class MaterialPipe implements PipeTransform {

    transform(value: any, expression:string): any {
        return value.filter(data=> data.material.toLowerCase().indexOf(expression.toLowerCase()) != -1) 
    }
}
