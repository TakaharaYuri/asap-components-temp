import { Component, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PedidoService } from '../../../../@core/services/precifica/pedido/pedido.service';
import { ListaProdutoseService } from '../../../../@core/services/precifica/pedido/lista-produtos.service';
import { ValorService } from '../../../../@core/services/precifica/pedido/valor.service';

@Component({
  selector: 'app-precificacao-modal-resumo',
  templateUrl: './modal-resumo.component.html',
  styleUrls: ['../../precificacao-visualizador.component.css'],
  providers: [ PedidoService, ListaProdutoseService, ValorService ]
})

export class ModalResumoComponent {

  dadosPedidos;
  listas;
  custos;

  constructor(
    public dialogRef: MatDialogRef<ModalResumoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _PedidoService: PedidoService,
    private _ListaProdutoseService: ListaProdutoseService,
    private _ValorService: ValorService
  ) { }
  
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(){
    this.getOrder();
    this.getProductList();
    this.getCoasts();
  }

  getOrder(){
    this._PedidoService.getResource(this.data.id).subscribe(
      data => {
        this.dadosPedidos = data.data[0]
      }
    )
  }

  getProductList(){
    this._ListaProdutoseService.getResource(this.data.id).subscribe(
      data => {
        this.listas = data.data;
      }
    )
  }

  getCoasts(){
    this._ValorService.getResource(this.data.id).subscribe(
      data => {
        this.custos = data.data
      }
    )
  }

}
