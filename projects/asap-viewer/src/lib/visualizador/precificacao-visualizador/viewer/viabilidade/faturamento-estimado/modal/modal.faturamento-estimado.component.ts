import { Component, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ProdutosService } from '../../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-faturamento-estimado-modal',
  templateUrl: './modal.faturamento-estimado.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ ProdutosService ]
})

export class ModalFaturamentoEstimadoComponent {

    productsCtrl: FormControl;
    filteredProducts: any;
    selectedId:number;

    products = [];

    constructor(
      public dialogRef: MatDialogRef<ModalFaturamentoEstimadoComponent>,
      private _ProdutosService: ProdutosService,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: any) {
        this.filter();
      }
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    filterStates(name: string) {
      return this.products.filter(data =>
        data.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    ngOnInit(){
      this.getProducts()
    }

    getProducts(){
      this._ProdutosService.getResources().subscribe(
        data => {
          console.log(data)
          this.products = data.data
          this.filter();
        }
      )
    }

    filter(){
      this.productsCtrl = new FormControl();
      this.filteredProducts = this.productsCtrl.valueChanges
      .pipe(
        startWith(''),
        map(data => data ? this.filterStates(data) : this.products.slice())
      );
    }

    selected(id) {
      this.selectedId = id;
    }

    returnProduct(){
      this.dialogRef.close(this.selectedId)
    }
}
