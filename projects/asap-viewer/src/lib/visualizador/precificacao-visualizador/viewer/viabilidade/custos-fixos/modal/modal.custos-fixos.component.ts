import { Component, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustoService } from '../../../../../../@core/services/precifica/viabilidade/custo.service';
import { ViabilidadeService } from '../../../../../../@core/services/precifica/viabilidade/viabilidade.service';

@Component({
  selector: 'app-precificacao-modal-custos',
  templateUrl: './modal.custos-fixos.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ CustoService, ViabilidadeService ]
})

export class ModalCustosComponent {

    form:FormGroup;
    custos:any = [];
    delete:boolean = false;

    constructor(
        private formBuilder: FormBuilder, 
        public dialogRef: MatDialogRef<ModalCustosComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _CustoService: CustoService,
        private _ViabilidadeService: ViabilidadeService
    ) {
        this.initForm()
     }
    
     ngOnInit(){
         if(this.data.type == "delete"){
            this.getCustos(this.data.id);
            this.delete = true;
         }else if(this.data.type == "update") {
            this.getCustos(this.data.id);
            this.delete = false;
         }
     }

     getCustos(id){
        this._CustoService.getResource(id).subscribe(
            data => {
                this.custos = data
                this.initForm();
                console.log(this.custos)
                this.form.setValue(data);
                
                
            }
        )
     }

    initForm() {
        this.form = this.formBuilder.group({
            custo: [''],
            valor: ['']
        })
    }

    register(){
        const form = this.form.value;
        this._ViabilidadeService.createResource(form).subscribe(
            data => {
                this.cancel();
                return true;
            }
        )
    }

    cancel(): void {
        this.dialogRef.close();
    }

    onNoClick() {
        this.dialogRef.close();
    }

    delClick(){
        this._CustoService.deleteResource(this.data.id).subscribe(
            data => {
                this.cancel();
                return true;
            }
        )
    }

}
