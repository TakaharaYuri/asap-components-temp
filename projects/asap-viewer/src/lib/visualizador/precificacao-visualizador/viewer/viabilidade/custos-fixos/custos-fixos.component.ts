import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustoService } from '../../../../../@core/services/precifica/viabilidade/custo.service';
import { ModalCustosComponent } from './modal/modal.custos-fixos.component';

@Component({
  selector: 'app-precificacao-viabilidade-custos-fixos',
  templateUrl: './custos-fixos.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ CustoService ]
})

export class CustosFixosComponent {

    custos:any = [];

    constructor(
      private route: ActivatedRoute, 
      private router: Router,
      public dialog: MatDialog,
      private _CustoService: CustoService
    ) {}

    ngOnInit(){
      this.getCoasts();
    }

    getCoasts(){
      this._CustoService.getResources().subscribe(
        data => {
          console.log(data.data)
          this.custos = data.data.custo_fixo;
        }
      )
    }

    novo() {
      let dialogRef = this.dialog.open(ModalCustosComponent, {
        width: '400px',
        data: { type: 'register'}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getCoasts();
      });
    }

    delete(id) {
      let dialogRef = this.dialog.open(ModalCustosComponent, {
        width: '500px',
        data: { type: 'delete', id: id}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getCoasts();
      });
    }

    editar(id){
      let dialogRef = this.dialog.open(ModalCustosComponent, {
        width: '500px',
        data: { type: 'update', id: id}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getCoasts();
      });
    }
}
