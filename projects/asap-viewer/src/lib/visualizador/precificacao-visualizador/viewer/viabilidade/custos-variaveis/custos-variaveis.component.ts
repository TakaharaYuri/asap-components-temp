import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViabilidadeService } from '../../../../../@core/services/precifica/viabilidade/viabilidade.service';

declare var $:any;

@Component({
  selector: 'app-precificacao-viabilidade-custos-variaveis',
  templateUrl: './custos-variaveis.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ViabilidadeService ]
})

export class CustosVariaveisComponent {

    constructor(
      private route: ActivatedRoute, 
      private router: Router,
      private _ViabilidadeService: ViabilidadeService) {}

  public data_uniformity = [0];

  ngOnInit(){    
    this.getViability().then(data => {
      this.data_uniformity = data
      this.getChartSimple();
    })
  }

  getViability(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._ViabilidadeService.getResources().subscribe(
        res => {
          resolve(res.total_custo_variavel)
        }, err => {
          reject(err);
        }
      )
    })
  }

  getChartSimple(){
        setTimeout(() => {
          var app = {
            init: function(){
              this.cacheDOM();
              this.handleCharts();
            },
            cacheDOM: function(){
              this.$chart = $(".bar-chart");
            },
            cssSelectors: {
              chartBar: "bar-chart--inner"
            },
            handleCharts: function(){
              /* 
                iterate through charts and grab total value
                then apply that to the width of the inner bar
              */
              $.each(this.$chart, function(){
                var $this = $(this),
                    total = $this.data("total"),
                    $targetBar = $this.find("."+app.cssSelectors.chartBar);
                    $targetBar.css("width","0%"); // zero out for animation
                    setTimeout(function(){
                      $targetBar.css("width",total+"%");
                    },400);     
              });
            }  
          }

          $('.bar-chart--text').each(function() {
            $(this).prop('Counter', 0).animate({
              Counter: $(this).text()
            }, {
              duration: 1500,
              easing: 'swing',
              step: function(now) {
                $(this).text(Math.ceil(now)+"%");
              }
            });
          });

        app.init();
        },1)
    }  
}
