import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-precificacao-viabilidade',
  templateUrl: './viabilidade.component.html',
  styleUrls: ['../../precificacao-visualizador.component.css']
})

export class ViabilidadeComponent {

  constructor(private route: ActivatedRoute, private router: Router) {}

  novo(){
    this.router.navigate(['/painel/precificacao/home/novo']);
  }

  visualizar(id){
    console.log(id)
    this.router.navigate(['/painel/precificacao/home/visualizar/'+id]);
  }
}
