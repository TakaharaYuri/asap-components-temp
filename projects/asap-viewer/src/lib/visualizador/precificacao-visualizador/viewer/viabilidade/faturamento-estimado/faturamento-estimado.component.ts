import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalFaturamentoEstimadoComponent } from './modal/modal.faturamento-estimado.component';
import { ViabilidadeService } from '../../../../../@core/services/precifica/viabilidade/viabilidade.service';

@Component({
  selector: 'app-precificacao-viabilidade-faturamento-estimado',
  templateUrl: './faturamento-estimado.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ViabilidadeService ]
})

export class FaturamentoEstimadoComponent {

    //variables
    value: string;
    viabilidade: any;
    tables = [];

    constructor(
        private route: ActivatedRoute, 
        private router: Router, 
        private dialog: MatDialog,
        private _ViabilidadeService: ViabilidadeService
    ) {}

    openDialog(): void {
      let dialogRef = this.dialog.open(ModalFaturamentoEstimadoComponent, {
          width: '500px'
      });

      dialogRef.afterClosed().subscribe(result => {
          this.value = result;
          this.tables = [];
          this.getQtdProdutos()
      });
    }

    getQtdProdutos(){
        this._ViabilidadeService.getResource('qtd', {router: this.value}).subscribe(
            data => {
                this.tables.push(data.data)
                console.log(this.tables)
            }
        )
    }

    ngOnInit(){
        this.getViability();
    }

    getViability(){
        this._ViabilidadeService.getResources().subscribe(
            data => {
                this.viabilidade = data.faturamento_necessario;
            }
        )
    }
  
}
