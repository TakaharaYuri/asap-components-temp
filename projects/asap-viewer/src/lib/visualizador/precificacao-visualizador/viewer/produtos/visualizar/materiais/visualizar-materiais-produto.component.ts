import { Component, ViewChild, DoCheck, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MaterialService } from '../../../../../../@core/services/precifica/material/material.service';
import { ProdutosService } from '../../../../../../@core/services/precifica/produtos/produtos.service';
import { FornecedorService } from '../../../../../../@core/services/precifica/fornecedor/fornecedor.service';

@Component({
  selector: 'app-precificacao-produtos-visualizar-resumo',
  templateUrl: './visualizar-materiais-produto.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ MaterialService, FornecedorService, ProdutosService ]
})

export class VisualizarMateriaisProdutoComponent {

  //variables
  form:FormGroup;
  public produto_id;
  public produto = [{name: ''}];
  public materiais = [];
  public fornecedores;
  public total = { inter:"0", float:"00" };
  public errors;

  @ViewChild('qtdPecas',{static:false}) qtdPecas: ElementRef;
  @ViewChild('precoUn',{static:false})   precoUn: ElementRef;
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private _MaterialService: MaterialService,
    private _FornecedorService: FornecedorService,
    private _ProdutosService: ProdutosService) {
    this.initForm();
  }
  
  round(number:number, precision:number) {
      let factor = Math.pow(10, precision);
      let tempNumber = number * factor;
      let roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
  };

  change(){
    let qtd = parseFloat(this.qtdPecas.nativeElement.value);
    let preco = parseFloat(this.precoUn.nativeElement.value);
    let total;
    console.log(qtd)
    qtd != undefined ? total = qtd / preco : total = preco
    let round = this.round(total, 2).toFixed(2).split('.');
    let result = {inter: round[0], float: round[1]}
    this.total = result
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [''],      
      produto_id: [''],
      fornecedor_id: [''],
      material: [''],
      unidade: [''],
      preco_unidade: [''],
      qtd_pecas: [''],
      insumo_pecas: ['']
    })
  }

  ngOnInit() {
    this.route.parent.params.forEach((params: Params) => {
      this.produto_id = params['id'];
      this.getProduto(this.produto_id);
      this.getMateriais(this.produto_id);
      this.getFornecedores();
    });
  }

  getProduto(id){
    this._ProdutosService.getResource(id).subscribe(
      data => {
        this.produto = data.data[0]
        console.log(data.data)
      }
    )
  }

  getMateriais(id){
    this._MaterialService.getResource(id).subscribe( data => {
      console.log(data)
      this.materiais = data.Material.data
    })
  }

  getFornecedores(){
    this._FornecedorService.getResources().subscribe( data => {
      this.fornecedores = data.data
    })
  }

  register() {
    let fornecedor = this.form.controls['fornecedor_id'].value;
    if(fornecedor == 0){
      this.form.get('fornecedor_id').patchValue(null)
    }
    
    this.form.get('produto_id').patchValue(this.produto_id)
    const form = this.form.value
    console.log(form)
      this._MaterialService.createResource(form, {router: this.produto_id }).subscribe(
       data => {
          this.getMateriais(this.produto_id);
          this.form.reset();
          this.total = { inter:"0", float:"00" };
          return true;
       },
       error => {        
          this.errors = error.json().error;  
       }
    );
  }

  deletar(material){
    this._MaterialService.deleteResource(material).subscribe(
      data => {
        this.getMateriais(this.produto_id);
        return true;
      }
    )
  }
}
