import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MaterialService } from '../../../../../../@core/services/precifica/material/material.service';
import { MaoObraService } from '../../../../../../@core/services/precifica/mao-obra/mao-obra.service';

@Component({
  selector: 'app-precificacao-produtos-visualizar-resumo',
  templateUrl: './visualizar-resumo-produto.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ MaterialService, MaoObraService ]
})

export class VisualizarResumoProdutoComponent {

  id:any;
  materiais:any;
  mao_obras:any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private _MaterialService: MaterialService,
    private _MaoObraService: MaoObraService
  ) {

  }


  ngOnInit() {

    this.route.params.forEach((params: Params) => {
      this.getMaterial(params['id']);
      this.getMaoObra(params['id'])
    });
  }

  getMaterial(id){
    this._MaterialService.getResource(id).subscribe(
      res => {
        this.materiais = res.Material.data;
        console.log(res)
      }
    )
  }

  getMaoObra(id){
    this._MaoObraService.getResource(id).subscribe(
      res => {
        this.mao_obras = res.MaoDeObra;
        console.log(res)
      }
    )
  }

}
