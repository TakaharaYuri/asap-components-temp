import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-precificacao-produtos-visualizar-resumo',
  templateUrl: './visualizar-fornecedor-produto.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css']
})

export class VisualizarFornecedorProdutoComponent {

  //variables
  form:FormGroup;
  supplierCtrl: FormControl;
  filteredSupplier: Observable<any[]>;

  constructor(private formBuilder: FormBuilder) {
    this.initForm();
  }

  initForm() {
    this.supplierCtrl = new FormControl();
    this.filteredSupplier = this.supplierCtrl.valueChanges
    .pipe(
      startWith(''),
      map(supplier => supplier ? this.filterStates(supplier) : this.supplier.slice())
    );

    this.form = this.formBuilder.group({
      id: [''],
      name: [''],
      email: [''],
      phone: [''],
      cep: [''],
      adress: ['']
    })
  }

  ngOnInit() {
    
  }

  register() {
    
  }

  supplier = [{ name: 'Fornecedor 1' },{ name: 'Fornecedor 2' },{ name: 'Fornecedor 3' }];


  filterStates(name: string) {
    return this.supplier.filter(data =>
      data.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

}
