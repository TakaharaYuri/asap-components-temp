import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProdutosService } from '../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-produtos-novo',
  templateUrl: './novo-produto.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ProdutosService ]
})

export class NovoProdutoComponent {

  //variables
  form:FormGroup;
  private image;
  public file_input: string = 'Selecionar Imagem';
  public errors:string = "";

  @ViewChild('fileInput',{static:false}) fileInput: ElementRef;

  constructor(
    private formBuilder: FormBuilder, 
    public _ProdutosService: ProdutosService) {
    this.initForm();
  }
  
  initForm() {
    let _dimensions = this.formBuilder.group({
      largura: [''],
      cumprimento: [''],
      peso: ['']
    })

    this.form = this.formBuilder.group({
      id: [''],
      name: [''],
      description: [''],
      foto: [this.image],
      type: [''],
      dimensions: _dimensions
    })
  }

  register(){
    const form = this.form.value
      this._ProdutosService.createResource(form).subscribe(
       data => {
          return true;
       },
       error => {        
          this.errors = error.json().error;  
       }
    );
    console.log('register')
  }

  onFileChange(event) {
    let reader:any = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.file_input = file.name;
        //this.form.get('foto').setValue(reader.result.split(',')[1]);
        this.image = reader.result.split(',')[1];
      };
    }
    
  }
}
