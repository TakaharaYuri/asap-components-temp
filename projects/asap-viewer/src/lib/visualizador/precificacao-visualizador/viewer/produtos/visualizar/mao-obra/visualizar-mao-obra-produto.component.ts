import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalMaoObraProdutoComponent } from './modal/modal.mao-de-obra.component';
import { MaoObraService } from '../../../../../../@core/services/precifica/mao-obra/mao-obra.service';
import { ArtesanService } from '../../../../../../@core/services/precifica/artesan/artesan.service';
import { ProdutosService } from '../../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-produtos-visualizar-resumo',
  templateUrl: './visualizar-mao-obra-produto.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ MaoObraService, ArtesanService, ProdutosService ]
})

export class VisualizarMaoObraProdutoComponent {

  //variables
  form:FormGroup;
  produto_id:number;
  value: number;
  type: string;
  calcMobra: boolean = false;
  checkType: boolean;
  public produto:any = [{name: ''}];
  etapas:any;

  checking(value){
    this.type = value;
  }

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private formBuilder: FormBuilder, 
    public dialog: MatDialog,
    private _MaoObraService: MaoObraService,
    private _ArtesanService: ArtesanService,
    private _ProdutosService: ProdutosService) {
    this.initForm();
  }

    openDialog(): void {
      let dialogRef = this.dialog.open(ModalMaoObraProdutoComponent, {
          width: '350px',
          data: { id: this.produto_id, type: this.type }
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          let results = result;
          console.log(results)
          if(results == "producao") {
            this.calcMobra = true;
            this.getMaoObra(this.produto_id)
          }else {
            this.router.navigate(['/visualizador/precificacao/visualizar/'+this.produto_id+'/calcular']);
          }
      });
    }

  initForm() {
    this.form = this.formBuilder.group({
      id: [''],
      name: [''],
      checkbox: ['']
    })
  }
  
  ngOnInit() {
    this.route.parent.params.forEach((params: Params) => {
      this.produto_id = params['id'];
      this. getProduto(this.produto_id);
    });
  }

  getProduto(id){
    this._ProdutosService.getResource(id).subscribe(
      data => {
        this.produto = data.data[0]
      }
    )
  }

  getMaoObra(id){
    this._MaoObraService.getResource(id).subscribe(data => {
      this.etapas = data.MaoDeObra.data;
      console.log(data)
    })
  }

}
