import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from '../../../../../../@core/services/precifica/calculadora/calculadora.service';

@Component({
  selector: 'app-precificacao-produtos-visualizar-resumo',
  templateUrl: './visualizar-calcular-produto.component.html',
  styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ CalculadoraService ]
})

export class VisualizarCalcularProdutoComponent {

  //variables
  form:FormGroup;
  calculated:boolean = false;
  produto_id:number;
  calcule:any = {
    Produto: {
        data: [
            {
                name: ''
            }
        ]
    },
    total_isumos: '',
    total_mao_de_obra: '',
    custo_total: '',
    lucro: '',
    preco_antes_imposto: '',
    impostos: '',
    venda_imposto: '',
    venda_minimo: '',
    referencia_lucro: {
        vinte_porcento: '',
        quarenta_porcento: '',
        sessenta_porcento: '',
        oitenta_porcento: '',
        cem_porcento: ''
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private _CalculadoraService: CalculadoraService) {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      lucro: [''],
      impostos: ['']
    })
  }

  
  ngOnInit() {
    this.route.parent.params.forEach((params: Params) => {
      this.produto_id = params['id'];
      this.getCalcule(this.produto_id)
    });
  }

  getCalcule(id){
    this._CalculadoraService.getResource(id).subscribe(
      data => {
        this.calcule = data;
      }
    )
  }

  update(){
    const form = this.form.value
    this._CalculadoraService.updateResource(form, {router: this.produto_id }).subscribe(
      data => {
        this.calculated = true;
        return true;
      }
    )
  }
}
