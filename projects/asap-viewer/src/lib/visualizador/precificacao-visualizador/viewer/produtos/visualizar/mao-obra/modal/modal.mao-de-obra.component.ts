import { Component, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MaoObraService } from '../../../../../../../@core/services/precifica/mao-obra/mao-obra.service';
import { ArtesanService } from '../../../../../../../@core/services/precifica/artesan/artesan.service';
import { ProdutosService } from '../../../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-produtos-visualizar-modal',
  templateUrl: './modal.mao-de-obra.component.html',
  styleUrls: ['../../../../../precificacao-visualizador.component.css'],
  providers: [ MaoObraService, ArtesanService, ProdutosService ]
})

export class ModalMaoObraProdutoComponent {

  //variables
  formProducao:FormGroup;
  formArtesa:FormGroup;
  formType:boolean = false;

  constructor(
      public dialogRef: MatDialogRef<ModalMaoObraProdutoComponent>,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: any,
      private _MaoObraService: MaoObraService,
      private _ArtesanService: ArtesanService,
      private _ProdutosService: ProdutosService) {
        this.initForm();
       }

    initForm() {    
      this.formArtesa = this.formBuilder.group({
        price: [''],
        type: ['', Validators.required]
      })

      this.formProducao = this.formBuilder.group({
        service: [''],
        time: [''],
        type: ['', Validators.required]
      })
    } 
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    ngOnInit(){
      if(this.data.type == "producao"){
        this.formType = true;
      }else {
        this.formType = false;
      }
    }

    registerArt(){
      const form = this.formArtesa.value
      this._ArtesanService.createResource(form, {router: this.data.id }).subscribe(
        data => {
          this.dialogRef.close(this.formType);
          return true;
        }
      );
    }

    registerProd(){
      const form = this.formProducao.value
      this._MaoObraService.createResource(form, {router: this.data.id }).subscribe(
        data => {
          this.dialogRef.close(this.data.type);
          return true;
        }
      );
    }
}
