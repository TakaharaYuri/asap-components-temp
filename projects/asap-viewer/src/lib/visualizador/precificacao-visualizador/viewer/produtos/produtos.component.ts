import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProdutosService } from '../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['../../precificacao-visualizador.component.css','./produtos.component.css'],
  providers: [ ProdutosService ]
})

export class ProdutosComponent {

  public produtos;
  public pedido;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    public _ProdutosService: ProdutosService) {}
  
  getProducts() {
    this._ProdutosService.getResources().subscribe(
      data => { 
        this.produtos = data.data
        console.log(data)
      }        
    );
  }

  ngOnInit(){
    this.getProducts();
  }

  novo(){
    this.router.navigate(['/visualizador/precificacao/novo']);
  }

  visualizar(id){
    console.log(id)
    this.router.navigate(['/visualizador/precificacao/visualizar/'+id]);
  }

  deletar(id){
    this._ProdutosService.deleteResource(id).subscribe(
      data => { 
        this.getProducts();
        return true;
      }        
    );
  }

  openResumo(id) {

  }

  delete(id) {

  }

}
