import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalResumoComponent } from '../../modal-resumo/modal-resumo.component';
import { PedidoService } from '../../../../../@core/services/loja/pedido.service';

@Component({
  selector: 'app-precificacao-meus-pedidos',
  templateUrl: './meus-pedidos.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css', './meus-pedidos.component.css'],
  providers: [ PedidoService ]
  
})

export class MeusPedidosComponent {

  //variables
  pedidos:any = []

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private _PedidoService: PedidoService
  ) {}

  ngOnInit(){
    this.getOrders()
  }

  getOrders(){
    this._PedidoService.getResources().subscribe(
      data => {
        this.pedidos = data.data
      }
    )
  }

  visualizar(id){
    this.router.navigate(['/visualizador/precificacao/pedidos/']);
    localStorage.setItem('calculadora', id);
    console.log(localStorage)
  }



  openResumo(id) {
    let dialogRef = this.dialog.open(ModalResumoComponent, {
      width: '800px',

      data: {id: id}
    });
  }

  novo(){
    localStorage.removeItem('calculadora');
    this.router.navigate(['/visualizador/precificacao/pedidos/']);
  }

  delete(id){
    this._PedidoService.deleteResource(id).subscribe(
      data => {
        this.getOrders();
        return true;
      }
    )
  }

}	
