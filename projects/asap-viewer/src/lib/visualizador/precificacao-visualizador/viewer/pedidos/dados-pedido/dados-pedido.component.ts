import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { PedidoService } from '../../../../../@core/services/precifica/pedido/pedido.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'L',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-precificacao-dados-pedido',
  templateUrl: './dados-pedido.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ PedidoService,
    {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS} ]
  
})

export class DadosPedidoComponent {

  //variables
  form:FormGroup;
  pedido_id;
  new_register:boolean = false;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private _PedidoService:PedidoService) {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [''],
      name: [''],
      email: [''],
      telefone: [''],
      end: [''],
      cidade: [''],
      uf: [''],
      dt_entrega: [''],
      user_id: [''],
      dt_pedido: ['']
    })
  }

  register(){
    localStorage.removeItem('calculadora');
    const form = this.form.value;
    this._PedidoService.createResource(form).subscribe(
      data => {
        localStorage.setItem('calculadora', data.data.id_calculadora);
      }
    )
  }

  update(){
    const form = this.form.value;
    this._PedidoService.updateResource(form, {router: this.pedido_id}).subscribe(
      data => {
        return true;
      }
    )
  }

  ngOnInit(){
    this.route.params.forEach((params: Params) => {
      if(params['id']){
        console.log('parametro de rota');
        this.pedido_id = params['id'];
        this.new_register = false;
        this.getOrder();
      }else if (localStorage.getItem("calculadora") !== null) {
        console.log('local storage');
        this.pedido_id = localStorage.getItem('calculadora');
        this.new_register = false;
        this.getOrder();
      }else{
        console.log('sem id');
        this.new_register = true;
      }

    });
  }

  getOrder(){
    this._PedidoService.getResource(this.pedido_id).subscribe(
      data => {
        console.log(data)
        this.initForm();
        this.form.setValue(data.data[0]);
      }
    )
  }

}	
