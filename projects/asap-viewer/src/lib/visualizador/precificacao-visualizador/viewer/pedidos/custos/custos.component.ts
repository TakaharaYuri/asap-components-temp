import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ValorService } from '../../../../../@core/services/precifica/pedido/valor.service';
import { TransporteService } from '../../../../../@core/services/precifica/pedido/transporte.service';
import { DescService } from '../../../../../@core/services/precifica/pedido/desc.service';

@Component({
  selector: 'app-precificacao-custos',
  templateUrl: './custos.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ValorService, TransporteService, DescService ]
})

export class CustosComponent {

  //variables
  transp:FormGroup;
  desc:FormGroup;
  calculadora_id:any;
  custos;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private _ValorService: ValorService,
    private _TransporteService: TransporteService,
    private _DescService: DescService) {
    this.initForm();
  }

  initForm() {
    this.transp = this.formBuilder.group({
      transport: [''],
      packing: ['']
    })

    this.desc = this.formBuilder.group({
      discount: ['']
    })
  }

  ngOnInit(){
    this.calculadora_id = localStorage.getItem('calculadora');
    this.getCoasts();
  }

  getCoasts(){
    this._ValorService.getResource(this.calculadora_id).subscribe(
      data => {
        this.custos = data.data
      }
    )
  }

  update(){
    const transp = this.transp.value;
    const desc = this.desc.value;
    if(transp.transport != "" && transp.packing != ""){
      this._TransporteService.updateResource(transp, {router: this.calculadora_id}).subscribe(
        data => {
          return true;
        }
      )
    }

    if(desc.discount != ""){
      this._DescService.updateResource(desc, {router: this.calculadora_id}).subscribe(
        data => {
          return true;
        }
      )
    }
    
    this.getCoasts();
    
  }
}	
