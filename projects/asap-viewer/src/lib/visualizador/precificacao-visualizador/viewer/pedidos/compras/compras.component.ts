import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalResumoComponent } from '../../modal-resumo/modal-resumo.component';
import { ListaService } from '../../../../../@core/services/precifica/pedido/lista.service';

@Component({
  selector: 'app-precificacao-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ListaService ]
})

export class ComprasComponent {

  //variables
  form:FormGroup;
  calculadora_id:any;
  listas = [];
  value_total = 0.00

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private formBuilder: FormBuilder, 
    public dialog: MatDialog,
    private _ListaService: ListaService
  ) { }

  ngOnInit(){
    console.log('aquiokk')
    this.calculadora_id = localStorage.getItem('calculadora');
    this.getBuyList();
  }

  getBuyList(){
    this._ListaService.getResource(this.calculadora_id).subscribe(
      data => {
        this.listas.push(data[0].data)
        let total = 0
        for(let i = 0; i < this.listas.length; i++){
          let value = parseFloat(this.listas[i]['valor_a_ser_gasto'])
          total += value;
        }
        this.value_total = total;
      }
    )
  }

  openResumo() {
    let dialogRef = this.dialog.open(ModalResumoComponent, {
      width: '800px',
      data: {id: this.calculadora_id}
    });
  }
  
}	
