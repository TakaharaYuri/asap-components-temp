import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-precificacao-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['../../precificacao-visualizador.component.css']
})

export class PedidosComponent {

  constructor(private route: ActivatedRoute, private router: Router) {}

  novo(){
    this.router.navigate(['/painel/precificacao/home/novo']);
  }

  visualizar(id){
    console.log(id)
    this.router.navigate(['/painel/precificacao/home/visualizar/'+id]);
  }
}
