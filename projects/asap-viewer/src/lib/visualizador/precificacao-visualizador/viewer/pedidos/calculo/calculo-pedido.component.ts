import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ModalEdit } from './modal/modal.calculo-pedido.component';
import { ValorService } from '../../../../../@core/services/precifica/pedido/valor.service';
import { ListaProdutoseService } from '../../../../../@core/services/precifica/pedido/lista-produtos.service';
import { ProdutosService } from '../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'app-precificacao-calculo-pedido',
  templateUrl: './calculo-pedido.component.html',
  styleUrls: ['../../../precificacao-visualizador.component.css'],
  providers: [ ValorService, ListaProdutoseService, ProdutosService ]
})

export class CalculoPedidoComponent {

  //variables
  form:FormGroup;
  produtos:any;
  listas:any = [];
  calculadora_id:any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private formBuilder: FormBuilder, 
    private _ValorService: ValorService,
    private _ListaProdutoseService: ListaProdutoseService,
    private _ProdutosService: ProdutosService,
    public dialog: MatDialog
  ) {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [''],
      produto_id:'' ,
      name_produto: '',
      qtd_parts: [''],
      price_parts: ['']
    })
  }

  ngOnInit(){
    this.calculadora_id = localStorage.getItem('calculadora');
    this.getProducts();
    this.getProductList();
  }

  getProducts(){
    this._ProdutosService.getResources().subscribe(
      data => {
        this.produtos = data.data
      }
    )
  }
  
  getPropsProduct(id, name){
    this.form.patchValue({
      produto_id: id, 
      name_produto: name
    });
  }

  register(){
    const form = this.form.value;
    this._ValorService.createResource(form, {router: this.calculadora_id}).subscribe(
      data => {
        this.getProductList();
        return true;
      }
    )
  }

  getProductList(){
    this._ListaProdutoseService.getResource(this.calculadora_id).subscribe(
      data => {
        this.listas = data.data;
      }
    )
  }

  edit(id){
    let dialogRef = this.dialog.open(ModalEdit, {
      width: '500px',
      data: { id: id, calculadora: this.calculadora_id }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getProductList();
    });
  }

  delete(id) {

  }

}	
