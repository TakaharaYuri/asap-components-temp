import {Component, Inject} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ValorService } from '../../../../../../@core/services/precifica/pedido/valor.service';
import { ProdutosService } from '../../../../../../@core/services/precifica/produtos/produtos.service';

@Component({
  selector: 'modal-edit',
  templateUrl: 'modal.calculo-pedido.component.html',
   styleUrls: ['../../../../precificacao-visualizador.component.css'],
  providers: [ ValorService, ProdutosService ]
})
export class ModalEdit{

    form:FormGroup;
    produtos:any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder, 
        private _ValorService:ValorService,
        private _ProdutosService:ProdutosService
    ) {
        this.initForm()
    }

    ngOnInit(){
        this.data.calculadora
        console.log(this.data.id)
        this._ValorService.getResource(this.data.id).subscribe(
        data => {
            this.initForm()
            this.form.setValue(data.produtos) 
        })
    }

    initForm() {
        this.form = this.formBuilder.group({
            qtd_parts: [''],
            price_parts: ['']
        })
    }

    update(){
        const form = this.form.value;
        this._ValorService.updateResource(form, {router: this.data.calculadora}).subscribe(
            data => {

            }
        )
    }

    onNoClick() {
        
    }
}