import { Routes } from '@angular/router';
import { GestaoVisualizadorComponent } from './gestao-visualizador.component';
import { GestaoResumoComponent } from './viewer/resumo/resumo.component';
import { Exercicio1Component } from './viewer/exercicio1/exercicio1.component';
import { CalculadoraFinanceiraComponent } from './viewer/calculadora-financeira/calculadora-financeira.component';
import { ComoResolverDividasComponent } from './viewer/como-resolver-dividas/como-resolver-dividas.component';
import { TrabalharPraGanharComponent } from './viewer/trabalhar-pra-ganhar/trabalhar-pra-ganhar.component';
import { MeuClienteComponent } from './viewer/meu-cliente/meu-cliente.component';
import { SemanaComponent } from './viewer/semana/semana.component';
import { Aula10Ex1Component } from './viewer/aula10-ex1/aula10-ex1.component';
import { Aula10Ex3Component } from './viewer/aula10-ex3/aula10-ex3.component';
import { Aula12Ex6Component } from './viewer/aula12-ex6/aula12-ex6.component';
import { ProductionTimeComponent } from './viewer/production-time/production-time.component';


export const ExerciciosRoutes: Routes = [
  {
    path: 'gestao',
    component: GestaoVisualizadorComponent,
    children: [
      // { path: '', redirectTo: 'resumo', component: GestaoResumoComponent },
      { path: 'resumo', component: GestaoResumoComponent },
      { path: 'exercicio1', component: Exercicio1Component },
      { path: 'calculadora-financeira', component: CalculadoraFinanceiraComponent },
      { path: 'como-resolver-dividas', component: ComoResolverDividasComponent },
      { path: 'trabalhar-pra-ganhar', component: TrabalharPraGanharComponent },
      { path: 'meu-cliente', component: MeuClienteComponent },
      { path: 'semana', component: SemanaComponent },
      { path: 'aula10/exercicio1', component: Aula10Ex1Component },
      { path: 'aula10/exercicio3', component: Aula10Ex3Component },
      { path: 'aula12/exercicio6', component: Aula12Ex6Component },
      { path: 'tempo-de-producao', component: ProductionTimeComponent }
    ]
  }

];
