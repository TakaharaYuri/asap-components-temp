import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var $: any;
declare var swal: any;


export const MY_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'L',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-gestao-semana',
  templateUrl: './semana.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'semana.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ExercisesService
  ]
})

export class SemanaComponent {
  public dataArray: any;
  public cenarioSelected: any;
  public inputDate: any;

  public exercise = '6';

  timeArray = [
    { value: 0, label: 'Nenhum Compromisso' },
    { value: 0.5, label: '30 Min.' },
    { value: 1, label: '1 Hora' },
    { value: 2, label: '2 Horas' },
    { value: 3, label: '3 Horas' },
    { value: 4, label: '4 Horas' }
  ];

  morning: Agenda;
  afternoon: Agenda;
  night: Agenda;

  report: any;

  public weekArray: {
    morning: Array<Agenda>,
    afternoon: Array<Agenda>,
    night: Array<Agenda>
  }

  constructor(private _exerciseService: ExercisesService) {
  }

  ngOnInit() {
    this.morning = new Agenda;
    this.afternoon = new Agenda;
    this.night = new Agenda;

    this.weekArray = {
      morning: [],
      afternoon: [],
      night: []
    }

    this.report = {
      me: {hour: 0, minutes: 0},
      others: {hour: 0, minutes: 0},
      business: {hour: 0, minutes: 0}
    }

    this.getData();
  }

  insertInMorning() {
    this.morning.date = new Date(this.inputDate).toLocaleDateString();
    this.weekArray.morning.push(this.morning);
    this.morning = new Agenda;
    this.calc();
  }

  insertInAfternoon() {
    this.afternoon.date = new Date(this.inputDate).toLocaleDateString();
    this.weekArray.afternoon.push(this.afternoon);
    this.afternoon = new Agenda;
    this.calc();
  }

  insertInNight() {
    this.night.date = new Date(this.inputDate).toLocaleDateString();
    this.weekArray.night.push(this.night);
    this.night = new Agenda;
    this.calc();
  }

  calc() {
    this.report = {
      me: {hour: 0, minutes: 0},
      others: {hour: 0, minutes: 0},
      business: {hour: 0, minutes: 0}
    }

    let meInt = 0;
    let othersInt = 0;
    let businessInt = 0;

    for(let ag of this.weekArray.morning) {
      meInt += ag.health.value;
      othersInt += ag.home.value;
      businessInt += ag.work.value;
    }

    for(let ag of this.weekArray.afternoon) {
      meInt += ag.health.value;
      othersInt += ag.home.value;
      businessInt += ag.work.value;
    }

    for(let ag of this.weekArray.night) {
      meInt += ag.health.value;
      othersInt += ag.home.value;
      businessInt += ag.work.value;
    }

    this.report = {
      me: this.intToTime(meInt),
      others: this.intToTime(othersInt),
      business: this.intToTime(businessInt),
    }

  }

  getData() {
    this._exerciseService.all(this.exercise)
      .subscribe(res => {
        this.dataArray = res.data;
      });
  }

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.weekArray = this.cenarioSelected.value;
    this.calc();
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: this.weekArray
    }
    if(!this.cenarioSelected) {
      swal('Sucesso', 'Os horários foram salvos com sucesso', 'success');
      this._exerciseService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal('Sucesso', 'Os horários foram atualizados com sucesso', 'success');
      this._exerciseService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  delete(index: any) {

    swal({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal('Sucesso', 'O cenário foi removido com sucesso!', '');

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exerciseService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  newCenario() {
    this.cenarioSelected = null;
    this.weekArray = {
      morning: [],
      afternoon: [],
      night: []
    }
    this.calc();
  }

  intToTime(int: number): {hour: number, minutes: number} {
    let obj = {hour: 0, minutes: 0};
    obj.hour = Math.trunc(int);
    obj.minutes = Math.round(Number((int - obj.hour).toFixed(2)) * 60);
    return obj;
  }

}

class Agenda {
  public date: string;
  public home: AgendaTime;
  public health: AgendaTime;
  public work: AgendaTime;
  public extra: AgendaTime;
}

class AgendaTime {
  public value: number;
  public label: string;
}