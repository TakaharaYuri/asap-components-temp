import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var swal: any;

@Component({
  selector: 'app-exercicio3',
  templateUrl: './exercicio3.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'exercicio3.component.css'],
  providers: [ExercisesService]
})

export class Exercicio3Component implements OnInit{

  public values;

  public resultA;
  public resultB;
  public resultC;
  public resultD;

  public exercise: string = '3';

  constructor(public _exercisesService: ExercisesService) {
    this.values = {
      A: 0,
      B: 0,
      C: 0,
      D: 0,
      E: 0
    };
  }

  ngOnInit() {
    this.getLast();
    this.calc();
  }

  getLast() {
    this._exercisesService.last(this.exercise).subscribe(res => {
      if(res.data.length > 0 && res.data[0].value) {
        this.values = res.data[0].value;
        this.calc();
      }
    });
  }

  calc() {
    this.resultA = this.filter(this.values.A * this.values.B);
    this.resultB = this.filter(this.values.C + this.values.D + this.resultA);
    this.resultC = Math.round(this.filter(this.resultB / this.values.E));
    this.resultD = Math.round(this.filter(this.resultC / this.values.A));
  }

  filter(value: any): number {
    return isNaN(parseInt(value)) ? 0 : value;
  }

  save() {
    const data = {
      exercises_id: this.exercise, 
      value: this.values
    };

    swal('Salvo com sucesso', 'Seu plano foi salvo com sucesso', 'success');

    this._exercisesService.save(data).subscribe(res => {
      console.log(res);
    });
  }

}
