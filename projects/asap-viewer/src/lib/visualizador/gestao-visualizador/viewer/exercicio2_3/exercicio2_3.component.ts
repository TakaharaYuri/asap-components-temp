import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-exercicio2_3',
  templateUrl: './exercicio2_3.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'exercicio2_3.component.css'],
  providers: [ExercisesService]
})

export class Exercicio2_3Component implements OnInit {

  public debtArray: Array<Debt>;
  public form: Debt;
  public debtSelected: Debt;
  public updateModalSending: boolean;
  public btnBlock: boolean;

  public status: string;

  public exercise = '2_3';

  constructor(private _exerciseService: ExercisesService) {
  }

  ngOnInit() {
    this.updateModalSending = false;
    this.btnBlock = false;
    this.form = new Debt;
    this.debtSelected = new Debt;
    this.status = '';

    this.getDebit();
  }

  getDebit() {
    this._exerciseService.last(this.exercise).subscribe(res => {
      this.debtArray = res.data[0].value;
    });
  }

  update(debt: Debt) {
    this.debtSelected = Object.assign({}, debt);
    $('#updateModal').modal();
  }

  debtUpdate() {
    this.updateModalSending = true;

    const item = this.debtArray.find((item) => {
      return item.id == this.debtSelected.id
    });

    const index = this.debtArray.indexOf(item);

    this.debtArray[index] = this.debtSelected;

    this.updateModalSending = false;

    this.debtSelected = new Debt;

    $('#updateModal').modal('toggle');
  }

  onSubmit() {
    this.btnBlock = true;

    this.form.id = this.debtArray.length + 1;

    this.debtArray.push(this.form);

    this.form = new Debt;

    this.btnBlock = false;
  }

  remove(debt: Debt) {
    swal({
      title: 'Você quer apagar esse débito?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.debtArray.indexOf(debt);
      this.debtArray.splice(index, 1);
    },
    (cancel) => {}
    );
  };

  save() {

    swal('Salvo com sucesso', 'Seu plano para pagamento de dívidas foi salvo', 'success');

    const data = {
      "exercises_id": this.exercise,
      "value": this.debtArray
    };
    this._exerciseService.save(data)
      .subscribe((res) => {
        console.log(res);
      });
  }


}

class Debt {
  public id: number;
  public description: string;
  public amount: string;
  public plan: string;

  populate(data: any) {
    this.id = data.id;
    this.description = data.description;
    this.amount = data.amount;
    this.plan = data.plan;
  }
}
