import { Component, Input, OnInit } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-exercicios-aula10-ex3',
  templateUrl: './aula10-ex3.component.html',
  styleUrls: ['aula10-ex3.component.css']
})

export class Aula10Ex3Component implements OnInit {

  isLinear = false;
  formGroup1: FormGroup;
  formGroup2: FormGroup;
  formGroup3: FormGroup;

  constructor(private _formBuilder: FormBuilder) { }


  ngOnInit() {
    this.formGroup1 = this._formBuilder.group({

      //firstCtrl: ['', Validators.required]
    });
    this.formGroup2 = this._formBuilder.group({
      //secondCtrl: ['', Validators.required]
    });
    this.formGroup3 = this._formBuilder.group({
      //secondCtrl: ['', Validators.required]
    });
  }

   contentVisible: boolean = false;
   objectDisabled: boolean = false;
   questao1: any;


   formGroup: any = [
     {
       name:"formGroup1",
       label: "Pergunta 1",
       editable: "true",
       title: 'Selecione a opção que representa um "Folder"',
       resposta: "Opção 2",
       questao: [
         {
           img:"https://www.criarcartao.com.br/images/modelos/37.png",
           opcao:"Opção 1"
         },
         {
           img:"https://img.elo7.com.br/product/zoom/165246A/folder-arte-digital-flyer.jpg",
           opcao:"Opção 2"
         },
         {
           img:"https://ae01.alicdn.com/kf/HTB1vTPmmPuhSKJjSspaq6xFgFXaW/New-100Pcs-DIY-Blank-price-Hang-tag-Kraft-Gift-Brown-Kraft-Paper-Tags-Label-Luggage-Wedding.jpg_640x640.jpg",
           opcao:"Opção 3"
         }
       ],
     },

     {
       name:"formGroup2",
       label: "Pergunta 2",
       editable: "true",
       title: 'Selecione a opção que representa um "Cartão"',
       resposta: "Opção 1",
       questao: [
         {
           img:"https://image.freepik.com/vetores-gratis/molde-do-folheto-ondulada-verde_1051-844.jpg",
           opcao:"Opção 1"
         },
         {
           img:"https://image.freepik.com/vetores-gratis/molde-do-folheto-ondulada-verde_1051-844.jpg",
           opcao:"Opção 2"
         },
         {
           img:"https://image.freepik.com/vetores-gratis/molde-do-folheto-ondulada-verde_1051-844.jpg",
           opcao:"Opção 3"
         }
       ],
     },

     {
       name:"formGroup3",
       label: "Pergunta 3",
       editable: "true",
       title: 'Selecione a opção que representa uma "Embalagem"',
       resposta: "Opção 3",
       questao: [
         {
           img:"https://image.freepik.com/vetores-gratis/tag-da-venda-com-textura_23-2147503621.jpg",
           opcao:"Opção 1"
         },
         {
           img:"https://image.freepik.com/vetores-gratis/tag-da-venda-com-textura_23-2147503621.jpg",
           opcao:"Opção 2"
         },
         {
           img:"https://image.freepik.com/vetores-gratis/tag-da-venda-com-textura_23-2147503621.jpg",
           opcao:"Opção 3"
         }
       ],
     },
   ];


  opcaoMarcada: any = [];
  resultado: any = '';

  radioChangeHandler(event:any){
    this.opcaoMarcada = event.target.value;
  }







}
