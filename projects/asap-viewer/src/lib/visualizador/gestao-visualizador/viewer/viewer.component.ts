import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewer-gestao',
  templateUrl: './viewer.component.html',
  styleUrls: ['../gestao-visualizador.component.css']
})

export class ViewerGestaoComponent implements OnInit {

  urlArray: Array<string>;
  index: number;

  constructor(private _router: Router ) {
      this.urlArray = [
        '/visualizador/gestao/resumo',

        // '/visualizador/gestao/exercicio1',

        '/visualizador/gestao/calculadora-financeira',
        '/visualizador/gestao/como-resolver-dividas',
        '/visualizador/gestao/trabalhar-pra-ganhar',
        '/visualizador/gestao/meu-cliente',
        '/visualizador/gestao/semana',
        '/visualizador/gestao/tempo-de-producao',
      ]
  }

  next() {
      this._router.navigate([this.urlArray[this.index + 1]]);
      this.index++;
  }

  previous() {
      this._router.navigate([this.urlArray[this.index - 1]]);
      this.index--;
  }

  ngOnInit() {
    this.index = this.urlArray.indexOf(this._router.url);
  }

}
