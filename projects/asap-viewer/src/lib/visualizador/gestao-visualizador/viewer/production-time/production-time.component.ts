import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';
import { StringHelper } from '../../../../@core/helpers/string.helper';

declare var swal: any;
declare var $: any;

@Component({
  selector: 'app-gestao-production-time',
  templateUrl: './production-time.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', './production-time.component.css'],
  providers: [ ExercisesService ]
})

export class ProductionTimeComponent implements OnInit {
  public dataArray: any;
  public cenarioSelected: any;

  public product: Product;
  public productArray: Array<Product>;

  public productSelected: Product;

  maskTime: any[] = [/[0-9]/, /\d/, ':', /\d/, /\d/];

  report: any;

  goal: number;

  public exercise = '7';

  constructor(private _exerciseService: ExercisesService) {
  }

  ngOnInit() {
    this.product = new Product;
    this.productArray = [];
    this.getData();
    this.report = {
      count: 0,
      totalInteger: 0,
      mediaInteger: 0,
      hoursInteger: 0,
      total: {hour: 0, minutes: 0},
      media: {hour: 0, minutes: 0},
      hours: {hour: 0, minutes: 0}
    }
  }

  insert(): void {
    this.product.id = StringHelper.generate(12);
    this.productArray.push(this.product);
    this.product = new Product;

    this.calc();
  }

  remove(index: number) {
    swal({
      title: 'Você quer apagar ' + this.productArray[index].name + '?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      this.productArray.splice(index, 1);
      this.calc();
    },
    (cancel) => {}
    );
  }

  calc() {

    this.report = {
      count: 0,
      totalInteger: 0,
      mediaInteger: 0,
      hoursInteger: 0,
      total: {hour: 0, minutes: 0},
      media: {hour: 0, minutes: 0},
      hours: {hour: 0, minutes: 0}
    }

    this.report.count = this.productArray.length;

    for(let prod of this.productArray) {
      let split = prod.time.split(':');
      this.report.totalInteger += +split[0];
      this.report.totalInteger += +split[1]/60;

      this.report.mediaInteger = this.report.totalInteger / this.report.count;

      this.report.hoursInteger = +this.goal * this.report.mediaInteger;
    }

    this.report.total = this.intToTime(this.report.totalInteger);
    this.report.media = this.intToTime(this.report.mediaInteger);
    this.report.hours = this.intToTime(this.report.hoursInteger);
  }

  intToTime(int: number): {hour: number, minutes: number} {
    let obj = {hour: 0, minutes: 0};
    obj.hour = Math.trunc(int);
    obj.minutes = Math.round(Number((int - obj.hour).toFixed(2)) * 60);
    return obj;
  }

  update(product: Product) {
    this.productSelected = Object.assign({}, product);
    $('#updateModal').modal();
  }

  saveUpdate() {
    const item = this.productArray.find((item) => {
      return item.id == this.productSelected.id
    });

    const index = this.productArray.indexOf(item);

    this.productArray[index] = this.productSelected;

    this.calc();

    $('#updateModal').modal('toggle');
  }

  getData() {
    this._exerciseService.all(this.exercise)
      .subscribe(res => {
        this.dataArray = res.data;
      });
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: {
        goal: this.goal,
        products: this.productArray
      },
    }
    if(!this.cenarioSelected) {
      swal('Sucesso', 'O perfil do cliente foi salvo com sucesso', 'success');
      this._exerciseService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal('Sucesso', 'O perfil do cliente foi atualizado com sucesso', 'success');
      this._exerciseService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  delete(index: any) {

    swal({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal('Sucesso', 'O cenário foi removido com sucesso!', '');

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exerciseService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  newCenario() {
    this.cenarioSelected = null;
    this.productArray = [];
    this.goal = null;

    this.calc();
  }

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.productArray = this.cenarioSelected.value.products;
    this.goal = this.cenarioSelected.value.goal;

    this.calc();
  }

}

class Product {
  id: string;
  name: string;
  time: string;
}