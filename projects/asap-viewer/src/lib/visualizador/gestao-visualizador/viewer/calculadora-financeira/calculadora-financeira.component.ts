import { Component, OnInit, Input, LOCALE_ID } from '@angular/core';
import { BalanceSheetService } from '../../../../@core/services/balance-sheet.service';
import { ExercisesService } from '../../../../@core/services/exercises.service';
import { BalanceSheet } from '../../../../@core/models/balance-sheet';
import { StringHelper } from '../../../../@core/helpers/string.helper';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-gestao-calculadora-financeira',
  templateUrl: './calculadora-financeira.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', './calculadora-financeira.component.css'],
  providers: [{provide: LOCALE_ID, useValue: "en-US"}, BalanceSheetService, ExercisesService]
})

export class CalculadoraFinanceiraComponent implements OnInit {

  // Máscara para campos do form.
  maskCellphone: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskLandline: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskCPF: any[] = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  maskCNPJ: any[] = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /[0]/, /[0]/, /[0]/, /[1]/, '-', /\d/, /\d/];
  maskZipCode: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  maskDate: any[] = [ /[0-9]/, /\d/, '/', /[0-9]/, /\d/, '/', /[0-9]/, /\d/, /\d/, /\d/ ];

  @Input() editor: boolean;

  public inputArray: Array<BalanceSheet>;
  public outputArray: Array<BalanceSheet>;

  public input: BalanceSheet;
  public inputBtnBlock: boolean;

  public output: BalanceSheet;
  public outputBtnBlock: boolean;

  public transactionSelected: BalanceSheet;
  public updateModalSending: boolean;

  public total = {
    input: 0,
    output: 0
  };

  public dataArray: any;
  public balanceArray: any;

  public cenarioSelected: any;

  public exercise: number = 2;

  constructor(
    public _balanceSheetService: BalanceSheetService,
    public _exercisesService: ExercisesService
  ) {
    this.input = new BalanceSheet().populate({
      type: 'input',
    });
    this.inputBtnBlock = false;

    this.output = new BalanceSheet().populate({
      type: 'output',
    });
    this.outputBtnBlock = false;

    this.updateModalSending = false;

    this.transactionSelected = new BalanceSheet();

    this.cenarioSelected = null;
  }

  ngOnInit() {
    this.balanceArray = [];
    this.inputArray = [];
    this.outputArray = [];

    this.getData();

    this.calcBalance();
  }

  getData() {
    this._exercisesService.all(2)
      .subscribe((result) => {
        this.dataArray = result.data;
      });
  }


  calcBalance() {
    this.inputArray = [];
    this.outputArray = [];

    this.total.input = 0;
    this.total.output = 0;

    for(let balancesheet of this.balanceArray) {
      if(balancesheet.type === 'input') {
        this.total.input += balancesheet.amount;
        this.inputArray.push(balancesheet);
      }
      if(balancesheet.type === 'output') {
        this.total.output += balancesheet.amount;
        this.outputArray.push(balancesheet);
      }
    }
  }

  onInputSubmit(form: any) {
    this.input.id = StringHelper.generate(32);

    this.balanceArray.push(this.input);

    this.input = new BalanceSheet().populate({
      type: 'input',
    });

    form.reset();

    this.calcBalance();
  }

  onOutputSubmit(form: any) {
    this.output.id = StringHelper.generate(32);

    this.balanceArray.push(this.output);

    this.output = new BalanceSheet().populate({
      type: 'output',
    });

    form.reset();

    this.calcBalance();
  }

  balanceSheetRemove(balanceSheet: BalanceSheet) {

    swal({
      title: 'Você quer apagar essa transação?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.balanceArray.indexOf(balanceSheet);
      this.balanceArray.splice(index, 1);

      this.calcBalance();
    },
    (cancel) => {}
    );
  }

  balanceSheetUpdate(balanceSheet: BalanceSheet) {
    this.transactionSelected = Object.assign({}, balanceSheet);
    $('#updateModal').modal();
  }

  transactionUpdate() {
    const item = this.balanceArray.find((item) => {
      return item.id === this.transactionSelected.id
    });

    const index = this.balanceArray.indexOf(item);

    this.balanceArray[index] = this.transactionSelected;

    this.calcBalance();

    $('#updateModal').modal('toggle');
  }

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.balanceArray = this.cenarioSelected.value;
    this.calcBalance();
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: this.balanceArray,
    }
    if(this.cenarioSelected === null) {
      swal('Sucesso', 'Seu cenário foi salvo com sucesso!', '');
      this._exercisesService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal('Sucesso', 'Seu cenário foi atualizado com sucesso!', '');
      this._exercisesService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  delete(index: any) {

    swal({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal('Sucesso', 'O cenário foi removido com sucesso!', '');

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exercisesService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  resetBalance() {
    this.getData();
    this.newCenario();
  }

  newCenario() {
    this.cenarioSelected = null;
    this.balanceArray = [];
    this.calcBalance();
  }

}
