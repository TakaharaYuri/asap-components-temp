import { Component } from '@angular/core';

@Component({
  selector: 'app-exercicio1-tab3',
  templateUrl: './exercicio1-tab3.component.html',
  styleUrls: ['../../../gestao-visualizador.component.css', '../exercicio1.component.css']
})

export class Exercicio1Tab3Component {
  channels = [
    { "name":"Feiras Locais e Regionais", "note":"8"},
    { "name":"Loja Itinerante (bike, carro ou expositor móvel)", "note":"3" },
    { "name":"Display", "note":"9"},
    { "name":"Sua loja dentro de outra loja", "note":"5" },
    { "name":"Visitas de Experiência", "note":"6" },
    { "name":"Venda Direta (sacoleiras ou catálogo)", "note":"10"},
    { "name":"Prestação de Serviços (produtos exclusivos)", "note":"5"},
    { "name":"Prestação de Serviços (aulas)", "note":"5" },
    { "name":"Prestação de Serviços (festas e eventos)", "note":"3"},
    { "name":"Prestação de Serviços (cenografia)" , "note":"3" },
    { "name":"Vendas pela Internet ou Shopping Online", "note":"10" },
    { "name":"Loja Própria", "note":"8" },
    { "name":"Venda para Lojas", "note":"4" },
    { "name":"Venda para Navios Turísticos", "note":"5" }
  ]
}
