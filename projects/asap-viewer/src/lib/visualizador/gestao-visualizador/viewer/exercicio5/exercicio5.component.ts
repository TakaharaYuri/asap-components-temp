import { Component } from '@angular/core';

@Component({
  selector: 'app-exercicio5',
  templateUrl: './exercicio5.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'exercicio5.component.css']
})

export class Exercicio5Component{
  domesticas = [
    {value: '30 Min.', viewValue: '30 Min.'},
    {value: '1 Hora', viewValue: '1 Hora'},
    {value: '2 Horas', viewValue: '2 Horas'},
    {value: '3 Horas', viewValue: '3 Horas'}
  ];

  consulta = [
    {value: '30 Min.', viewValue: '30 Min.'},
    {value: '1 Hora', viewValue: '1 Hora'},
    {value: '2 Horas', viewValue: '2 Horas'},
    {value: '3 Horas', viewValue: '3 Horas'}
  ];

  trabalho = [
    {value: '30 Min.', viewValue: '30 Min.'},
    {value: '1 Hora', viewValue: '1 Hora'},
    {value: '2 Horas', viewValue: '2 Horas'},
    {value: '3 Horas', viewValue: '3 Horas'}
  ];

  extras = [
    {value: '30 Min.', viewValue: '30 Min.'},
    {value: '1 Hora', viewValue: '1 Hora'},
    {value: '2 Horas', viewValue: '2 Horas'},
    {value: '3 Horas', viewValue: '3 Horas'}
  ];
}
