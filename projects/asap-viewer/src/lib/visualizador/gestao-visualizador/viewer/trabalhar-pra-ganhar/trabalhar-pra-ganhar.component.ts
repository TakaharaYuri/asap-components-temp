import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var swal: any;

@Component({
  selector: 'app-gestao-trabalhar-pra-ganhar',
  templateUrl: './trabalhar-pra-ganhar.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'trabalhar-pra-ganhar.component.css'],
  providers: [ ExercisesService ]
})

export class TrabalharPraGanharComponent implements OnInit{

  public dataArray: any;
  public cenarioSelected: any;

  public values;

  public resultA;
  public resultB;
  public resultC;
  public resultD;

  public exercise: number = 4;

  constructor(public _exercisesService: ExercisesService) {
    this.values = {
      A: 0,
      B: 0,
      C: 0,
      D: 0,
      E: 0
    };
  }

  ngOnInit() {
    this.getData();
    this.calc();
  }

  getData() {
    this._exercisesService.all(this.exercise)
      .subscribe(res => {
        this.dataArray = res.data;
      });
  }

  calc() {
    this.resultA = this.filter(this.values.A * this.values.B);
    this.resultB = this.filter(this.values.C + this.values.D + this.resultA);
    this.resultC = Math.round(this.filter(this.resultB / this.values.E));
    this.resultD = Math.round(this.filter(this.resultC / this.values.A));
  }

  filter(value: any): number {
    return isNaN(parseInt(value)) ? 0 : value;
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: this.values,
    }

    if(!this.cenarioSelected) {
      swal('Salvo com sucesso', 'Seu plano foi salvo com sucesso', 'success');
      this._exercisesService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal('Salvo com sucesso', 'Seu plano foi atualizado com sucesso', 'success');
      this._exercisesService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.values = this.cenarioSelected.value;
    this.calc();
  }

  delete(index: any) {

    swal({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal('Sucesso', 'O cenário foi removido com sucesso!', '');

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exercisesService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  newCenario() {
    this.cenarioSelected = null;
    this.values = {
      A: 0,
      B: 0,
      C: 0,
      D: 0,
      E: 0
    }
    this.calc();
  }


}
