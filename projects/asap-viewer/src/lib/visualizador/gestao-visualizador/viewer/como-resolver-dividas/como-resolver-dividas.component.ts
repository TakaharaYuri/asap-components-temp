import { Component, OnInit } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-gestao-como-resolver-dividas',
  templateUrl: './como-resolver-dividas.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'como-resolver-dividas.component.css'],
  providers: [ExercisesService]
})

export class ComoResolverDividasComponent implements OnInit {

  public dataArray: any;
  public cenarioSelected: any;

  public debtArray: Array<Debt>;
  public form: Debt;
  public debtSelected: Debt;
  public updateModalSending: boolean;
  public btnBlock: boolean;

  public status: string;

  public total: number;

  public exercise = '3';

  constructor(private _exerciseService: ExercisesService) {
  }

  ngOnInit() {
    this.updateModalSending = false;
    this.btnBlock = false;
    this.form = new Debt;
    this.debtSelected = new Debt;
    this.status = '';
    this.debtArray = [];

    this.getData();
  }

  getData() {
    this._exerciseService.all(this.exercise)
      .subscribe(res => {
        this.dataArray = res.data;
      });
  }

  update(debt: Debt) {
    this.debtSelected = Object.assign({}, debt);
    $('#updateModal').modal();
  }

  calc() {
    this.total = 0;
    this.debtArray.map(item => {
      this.total += +item.amount;
    });
  }

  debtUpdate() {
    this.updateModalSending = true;

    const item = this.debtArray.find((item) => {
      return item.id == this.debtSelected.id
    });

    const index = this.debtArray.indexOf(item);

    this.debtArray[index] = this.debtSelected;

    this.updateModalSending = false;

    this.debtSelected = new Debt;

    $('#updateModal').modal('toggle');

    this.calc();
  }

  onSubmit(form: any) {
    this.form.id = this.debtArray.length + 1;

    this.debtArray.push(this.form);

    this.form = new Debt;

    form.reset();

    this.calc();
  }

  remove(debt: Debt) {
    swal({
      title: 'Você quer apagar esse débito?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.debtArray.indexOf(debt);
      this.debtArray.splice(index, 1);

      this.calc();
    },
    (cancel) => {}
    );
  };

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.debtArray = this.cenarioSelected.value;
    this.calc();
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: this.debtArray,
    }
    if(!this.cenarioSelected) {
      swal('Sucesso', 'O plano para pagamento de dívidas foi salvo com sucesso', 'success');
      this._exerciseService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal('Sucesso', 'O plano para pagamento de dívidas foi atualizado com sucesso', 'success');
      this._exerciseService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  delete(index: any) {

    swal({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal('Sucesso', 'O cenário foi removido com sucesso!', '');

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exerciseService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  resetBalance() {
    this.getData();
    this.newCenario();
  }

  newCenario() {
    this.cenarioSelected = null;
    this.debtArray = [];
    this.calc();
  }


}

class Debt {
  public id: number;
  public description: string;
  public amount: string;
  public plan: string;

  populate(data: any) {
    this.id = data.id;
    this.description = data.description;
    this.amount = data.amount;
    this.plan = data.plan;
  }
}
