import { Component, OnInit, Input, LOCALE_ID } from '@angular/core';
import { BalanceSheet } from '../../../../@core/models/balance-sheet';
import { BalanceSheetService } from '../../../../@core/services/balance-sheet.service';
import { StringHelper } from '../../../../@core/helpers/string.helper';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-exercicio2',
  templateUrl: './exercicio2.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', './exercicio2.component.css'],
  providers: [{provide: LOCALE_ID, useValue: "en-US"}, BalanceSheetService]
})

export class Exercicio2Component implements OnInit {

  // Máscara para campos do form.
  maskCellphone: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskLandline: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskCPF: any[] = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  maskCNPJ: any[] = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /[0]/, /[0]/, /[0]/, /[1]/, '-', /\d/, /\d/];
  maskZipCode: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  maskDate: any[] = [ /[0-9]/, /\d/, '/', /[0-9]/, /\d/, '/', /[0-9]/, /\d/, /\d/, /\d/ ];

  @Input() editor: boolean;

  public inputArray: Array<BalanceSheet>;
  public outputArray: Array<BalanceSheet>;

  public input: BalanceSheet;
  public inputBtnBlock: boolean;

  public output: BalanceSheet;
  public outputBtnBlock: boolean;

  public transactionSelected: BalanceSheet;
  public updateModalSending: boolean;

  public total = {
    input: 0,
    output: 0
  };

  public balanceArray: Array<BalanceSheet>;

  constructor(
    public _balanceSheetService: BalanceSheetService
    ) {
    this.input = new BalanceSheet().populate({
      type: 'input',
    });
    this.inputBtnBlock = false;

    this.output = new BalanceSheet().populate({
      type: 'output',
    });
    this.outputBtnBlock = false;

    this.updateModalSending = false;

    this.transactionSelected = new BalanceSheet();
  }

  ngOnInit() {
    this.balanceArray = [];
    this.inputArray = [];
    this.outputArray = [];

    this._balanceSheetService.collection()
      .then((result) => {
        this.balanceArray = result;
        this.calcBalance();
      });
  }

  calcBalance() {
    this.inputArray = [];
    this.outputArray = [];

    this.total.input = 0;
    this.total.output = 0;

    for(let balancesheet of this.balanceArray) {
      if(balancesheet.type === 'input') {
        this.total.input += balancesheet.amount;
        this.inputArray.push(balancesheet);
      }
      if(balancesheet.type === 'output') {
        this.total.output += balancesheet.amount;
        this.outputArray.push(balancesheet);
      }
    }
  }

  onInputSubmit() {
    this.inputBtnBlock = true;

    this.input.id = StringHelper.generate(32);

    this.balanceArray.push(this.input);

    this.input = new BalanceSheet().populate({
      type: 'input',
    });

    this.inputBtnBlock = false;

    this.calcBalance();
  }

  onOutputSubmit() {
    this.outputBtnBlock = true;

    this.output.id = StringHelper.generate(32);

    this.balanceArray.push(this.output);
    this.output = new BalanceSheet().populate({
      type: 'output',
    });
    this.outputBtnBlock = false;

    this.calcBalance();
  }

  balanceSheetRemove(balanceSheet: BalanceSheet) {

    swal({
      title: 'Você quer apagar essa transação?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      const index = this.balanceArray.indexOf(balanceSheet);
      this.balanceArray.splice(index, 1);

      this.calcBalance();
    },
    (cancel) => {}
    );
  }

  balanceSheetUpdate(balanceSheet: BalanceSheet) {
    this.transactionSelected = Object.assign({}, balanceSheet);
    $('#updateModal').modal();
  }

  transactionUpdate() {
    this.updateModalSending = true;

    const item = this.balanceArray.find((item) => {
      return item.id == this.transactionSelected.id
    });

    const index = this.balanceArray.indexOf(item);

    this.balanceArray[index] = this.transactionSelected;

    this.calcBalance();

    this.updateModalSending = false;

    $('#updateModal').modal('toggle');
  }

  save() {
    this._balanceSheetService.persist(this.balanceArray)
      .subscribe((res) => {
        console.log(res);
      });
  }

}
