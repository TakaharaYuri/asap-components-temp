import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ExercisesService } from '../../../../@core/services/exercises.service';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-gestao-meu-cliente',
  templateUrl: './meu-cliente.component.html',
  styleUrls: ['../../gestao-visualizador.component.css', 'meu-cliente.component.css'],
  providers: [ ExercisesService ]
})

export class MeuClienteComponent implements OnInit {

  @Output() public event = new EventEmitter<any>();
  public dataArray: any;
  public cenarioSelected: any;

  public cliente: Cliente;

  public exercise = '5';

  constructor(private _exerciseService: ExercisesService) {
  }

  ngOnInit() {
    this.cliente = new Cliente;

    this.getData();
  }

  getData() {
    this._exerciseService.all(this.exercise)
      .subscribe(res => {
        this.dataArray = res.data;
      });
  }

  selectCenario(index: number) {
    this.cenarioSelected = this.dataArray[index];
    this.cliente = this.cenarioSelected.value;
  }

  save() {
    const body = {
      exercises_id: this.exercise,
      value: this.cliente,
    }
    if(!this.cenarioSelected) {
      swal.fire('Sucesso', 'O perfil do cliente foi salvo com sucesso', 'success');
      this._exerciseService.save(body)
      .subscribe(res => {
        this.dataArray.unshift(res.data);
        this.selectCenario(0);
      });
    }else{
      swal.fire('Sucesso', 'O perfil do cliente foi atualizado com sucesso', 'success');
      this._exerciseService.update(this.cenarioSelected.id, body)
      .subscribe(res => {
        console.log(res);
      });
    }
  }

  delete(index: any) {

    swal.fire({
      title: 'Você quer apagar esse cenário?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não'
    }).then(
    (result) => {
      swal.fire({
        title:'Sucesso', 
        text: 'O cenário foi removido com sucesso!', 
        type:'success'
      });

      if(this.cenarioSelected === this.dataArray[index]) {
        this.newCenario();
      }

      const id = this.dataArray[index].id;
      
      this.dataArray.splice(index, 1);

      this._exerciseService.delete(id)
      .subscribe(res => {
        console.log(res);
      });
    },
    (cancel) => {}
    );

  }

  resetBalance() {
    this.getData();
    this.newCenario();
  }

  newCenario() {
    this.cenarioSelected = null;
    this.cliente = new Cliente;
  }

  goNext() {
    this.event.emit();
  }


}

class Cliente {
  public id: number;
  public name: string;
  public ocupation: string;
  public education: string;
  public location: string;
  public salary: string;
  public home: string;
  public age: string;
  public gender: string;
  public hobby: string;

  populate(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.ocupation = data.ocupation;
    this.education = data.education;
    this.location = data.location;
    this.salary = data.salary;
    this.home = data.home;
    this.age = data.age;
    this.gender = data.gender;
    this.hobby = data.hobby;
  }
}
