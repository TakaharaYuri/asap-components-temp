import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import { ExerciciosRoutes } from './gestao-visualizador.routing';
import { MatStepperModule, MatFormFieldModule, MatTooltipModule, MatDatepickerModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { GestaoSidebarComponent } from './gestao-sidebar/gestao-sidebar.component';
import { GestaoNavbarComponent } from './gestao-navbar/gestao-navbar.component';
import { GestaoVisualizadorComponent } from './gestao-visualizador.component';

import localePt from '@angular/common/locales/pt';
import { ViewerGestaoComponent } from './viewer/viewer.component';
import { GestaoResumoComponent } from './viewer/resumo/resumo.component';
import { Aula10Ex1Component } from './viewer/aula10-ex1/aula10-ex1.component';
import { Aula10Ex3Component } from './viewer/aula10-ex3/aula10-ex3.component';
import { Aula12Ex6Component } from './viewer/aula12-ex6/aula12-ex6.component';
import { Exercicio1Component } from './viewer/exercicio1/exercicio1.component';
import { CalculadoraFinanceiraComponent } from './viewer/calculadora-financeira/calculadora-financeira.component';
import { ComoResolverDividasComponent } from './viewer/como-resolver-dividas/como-resolver-dividas.component';
import { TrabalharPraGanharComponent } from './viewer/trabalhar-pra-ganhar/trabalhar-pra-ganhar.component';
import { MeuClienteComponent } from './viewer/meu-cliente/meu-cliente.component';
import { SemanaComponent } from './viewer/semana/semana.component';
import { Exercicio1Tab1Component } from './viewer/exercicio1/tab1/exercicio1-tab1.component';
import { Exercicio1Tab2Component } from './viewer/exercicio1/tab2/exercicio1-tab2.component';
import { Exercicio1Tab3Component } from './viewer/exercicio1/tab3/exercicio1-tab3.component';
import { Exercicio1ScenarioComponent } from './viewer/exercicio1/scenario/scenario.component';
import { ProductionTimeComponent } from './viewer/production-time/production-time.component';
import { Exercicio2Component } from './viewer/exercicio2/exercicio2.component';
import { Exercicio2_3Component } from './viewer/exercicio2_3/exercicio2_3.component';
import { Exercicio3Component } from './viewer/exercicio3/exercicio3.component';
import { Exercicio4Component } from './viewer/exercicio4/exercicio4.component';
import { Exercicio5Component } from './viewer/exercicio5/exercicio5.component';
registerLocaleData(localePt);

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ExerciciosRoutes),
    ReactiveFormsModule,
    FormsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatOptionModule,
    MatSelectModule
  ],
  declarations: [
    GestaoVisualizadorComponent,
    GestaoSidebarComponent,
    GestaoNavbarComponent,
    ViewerGestaoComponent,
    GestaoResumoComponent,
    Aula10Ex1Component,
    Aula10Ex3Component,
    Aula12Ex6Component,
    Exercicio1Component,
    CalculadoraFinanceiraComponent,
    ComoResolverDividasComponent,
    TrabalharPraGanharComponent,
    MeuClienteComponent,
    SemanaComponent,
    Exercicio1Tab1Component,
    Exercicio1Tab2Component,
    Exercicio1Tab3Component,
    Exercicio1ScenarioComponent,
    ProductionTimeComponent,
    Exercicio2Component,
    Exercicio2_3Component,
    Exercicio3Component,
    Exercicio4Component,
    Exercicio5Component
  ],
  exports: [
    GestaoVisualizadorComponent,
    GestaoSidebarComponent,
    GestaoNavbarComponent,
    ViewerGestaoComponent,
    GestaoResumoComponent,
    Aula10Ex1Component,
    Aula10Ex3Component,
    Aula12Ex6Component,
    Exercicio1Component,
    CalculadoraFinanceiraComponent,
    ComoResolverDividasComponent,
    TrabalharPraGanharComponent,
    MeuClienteComponent,
    SemanaComponent,
    Exercicio1Tab1Component,
    Exercicio1Tab2Component,
    Exercicio1Tab3Component,
    Exercicio1ScenarioComponent,
    ProductionTimeComponent,
    Exercicio2Component,
    Exercicio2_3Component,
    Exercicio3Component,
    Exercicio4Component,
    Exercicio5Component
  ],
})
export class ExerciciosModule {

}
