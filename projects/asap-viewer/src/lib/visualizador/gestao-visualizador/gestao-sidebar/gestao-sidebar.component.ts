import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: 'resumo',
        title: 'Ínicio',
        type: 'link',
        icontype: 'flaticon-home'
    },
    // desativado a pedido do edu
    /*
    {
        path: 'exercicio1',
        title: 'Ficha de canais de venda',
        type: 'link',
        icontype: 'flaticon-tabs'
    },
    */
    {
        path: 'calculadora-financeira',
        title: 'Calculadora financeira',
        type: 'link',
        icontype: 'flaticon-calculator'
    },{
        path: 'como-resolver-dividas',
        title: 'Como resolver minhas dívidas?',
        type: 'link',
        icontype: 'flaticon-check'
    },{
        path: 'trabalhar-pra-ganhar',
        title: 'Quanto trabalhar para ganhar?',
        type: 'link',
        icontype: 'flaticon-tabs'
    },{
        path: 'meu-cliente',
        title: 'Vamos definir agora quem que é o seu cliente',
        type: 'link',
        icontype: 'flaticon-calculator'
    },{
        path: 'semana',
        title: 'Calculadora da semana',
        type: 'link',
        icontype: 'flaticon-check'
    },{
        path: 'tempo-de-producao',
        title: 'Tempo de Produção',
        type: 'link',
        icontype: 'flaticon-check'
    }
];

@Component({
    selector: 'app-gestao-sidebar',
    templateUrl: 'gestao-sidebar.component.html',
    styleUrls: ['./gestao-sidebar.component.css']
})

export class GestaoSidebarComponent implements OnInit {
    public menuItems: any[];

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

}
