import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner-editor.component.css']
})

export class BannerComponent {
  @Input() item: any;

  constructor(
    private sanitizer: DomSanitizer
  ) {
    
  }

  link() {
      console.log('lnk');
  }

}
