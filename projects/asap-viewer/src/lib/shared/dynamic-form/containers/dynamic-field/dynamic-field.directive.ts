import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormButtonComponent} from '../../components/form-button/form-button.component';
import {FormInputComponent} from '../../components/form-input/form-input.component';
import {FormNumberComponent} from '../../components/form-number/form-number.component';
import {FormTextAreaComponent} from '../../components/form-textarea/form-textarea.component';
import {FormSelectComponent} from '../../components/form-select/form-select.component';
import {FormDateComponent} from '../../components/form-date/form-date.component';
import {FormPasswordComponent} from '../../components/form-password/form-password.component';
import {FormLinkComponent} from '../../components/form-link/form-link.component';
import {FormAddressComponent} from '../../components/form-address/form-address.component';

const components = {
  button: FormButtonComponent,
  text: FormInputComponent,
  number: FormNumberComponent,
  textarea: FormTextAreaComponent,
  select: FormSelectComponent,
  date: FormDateComponent,
  password: FormPasswordComponent,
  link: FormLinkComponent,
  address: FormAddressComponent
};

@Directive({
  selector: '[appDynamicField]'
})
export class DynamicFieldDirective implements OnInit {

  @Input()
  public config;

  @Input()
  public group: FormGroup;

  public component;

  constructor(private resolver: ComponentFactoryResolver,
              private container: ViewContainerRef) {
  }

  ngOnInit(): void {
    const component = components[this.config.type];
    if (component !== undefined) {
      const factory = this.resolver.resolveComponentFactory<any>(component);
      this.component = this.container.createComponent(factory);
      this.component.instance.config = this.config;
      this.component.instance.group = this.group;
    }
  }

}
