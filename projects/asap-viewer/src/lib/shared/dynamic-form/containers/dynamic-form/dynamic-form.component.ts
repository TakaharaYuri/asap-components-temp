import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-dynamic-form',
  exportAs: 'appDynamicForm',
  styleUrls: ['./dynamic-form.component.scss'],
  templateUrl: './dynamic-form.component.html'
})
export class DynamicFormComponent implements OnInit {

  @Input()
  config: any[] = [];
  @Input()
  buttons: any[] = [];
  @Output()
  submitted: EventEmitter<any> = new EventEmitter<any>();

  address = [
    { label: 'Estado', name: 'uf', type: 'address', placeholder: 'Selecione seu Estado' },
    { label: 'Cidade', name: 'cidade', type: 'address', placeholder: 'Selecione sua Cidade' }
  ]

  form: FormGroup;
  
  get controls() {
    return this.config.filter(({type}) => type !== 'button');
  }

  get changes() {
    return this.form.valueChanges;
  }

  get valid() {
    return this.form.valid;
  }

  get value() {
    return this.form.value;
  }


  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.createGroup();
  }

  createGroup() {
    const group = this.fb.group({});
    this.address.forEach(control => group.addControl(control.name, new FormControl('')))
    this.config.forEach(control => group.addControl(control.name,  control.required == 1 ? new FormControl( '', Validators.required) : new FormControl('')));
    // this.buttons.forEach(control => group.addControl(control.name, this.fb.control(control)));
    return group;
  }

  setValueFull(jsonObj) {
    this.form.patchValue(jsonObj);    
  }

  setValue(name: string, value: any) {
    this.form.controls[name].setValue(value, {emitEvent: true});
  }

  patchValue(obj: object) {
    if (typeof obj === 'object') {
      this.form.patchValue(obj);
      console.log(obj)
    }
  }
}
