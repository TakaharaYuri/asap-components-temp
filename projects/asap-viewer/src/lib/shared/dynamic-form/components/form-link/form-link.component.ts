import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Field} from '../../models/field.interface';

@Component({
  selector: 'app-dynamic-form-button',
  styleUrls: ['form-link.component.scss'],
  templateUrl: './form-link.component.html'
})
export class FormLinkComponent implements Field {
  public config;
  public group: FormGroup;
}
