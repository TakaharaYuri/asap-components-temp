import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Field} from '../../models/field.interface';

@Component({
  selector: 'app-dynamic-form-button',
  styleUrls: ['form-button.component.scss'],
  templateUrl: './form-button.component.html'
})
export class FormButtonComponent implements Field {
  public config;
  public group: FormGroup;
}
