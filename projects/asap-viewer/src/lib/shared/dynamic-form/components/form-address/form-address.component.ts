import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Field} from '../../models/field.interface';

@Component({
  selector: 'app-dynamic-form-address',
  styleUrls: ['./form-address.component.scss'],
  templateUrl: './form-address.component.html',
  providers: [ EstadoCidadeService ]
})
export class FormAddressComponent implements Field {

  config;
  group: FormGroup;
  state;
  city = [];
  formTrue = false;
  loading = true;

  uf;
  constructor(
    private _estadoCidadeService: EstadoCidadeService
  ) { }

  ngOnInit() {
    // this.group.get('cidade').disable();
    this.getState();
  }

  getState() {
    this._estadoCidadeService.getState().subscribe( res=> {
      setTimeout(() => {
        this.state = res;
        if(this.group.get('uf').value != '') {          
          this.formTrue = false;
          let estado = res.filter(data => this.group.get('uf').value == data.nome)
          if(estado.length > 0) {
            this.getCity(estado[0].id);
            this.uf = estado[0].id
          }else {
            this.uf = 0
          }   
          this.loading = false;
        } else {
          this.loading = false;
        }
      }, 1000)
      
    })
  }

  getCity(id) {
    this.city = [];
    this._estadoCidadeService.getCity(id).subscribe( res=> {      
      this.city.push(res);
      this.formTrue = true;
      // this.group.get('cidade').enable();
    })
  }

  chanceState(event) {
    console.log(event)
    this.formTrue = false;    
    const id = event.value;
    console.log(this.state)
    const estado = this.state.filter(res => id == res.id);
    console.log(estado)
    this.group.get('uf').setValue(estado[0].nome)
    console.log(id)
    this.getCity(id);
  }
}
