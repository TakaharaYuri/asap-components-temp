import {Component, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dynamic-form-number',
  styleUrls: ['form-number.component.scss'],
  templateUrl: './form-number.component.html'
})
export class FormNumberComponent {
  config;
  group: FormGroup;
}
