import {Component, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dynamic-form-input',
  styleUrls: ['form-password.component.scss'],
  templateUrl: './form-password.component.html'
})
export class FormPasswordComponent {
  config;
  group: FormGroup;
}
