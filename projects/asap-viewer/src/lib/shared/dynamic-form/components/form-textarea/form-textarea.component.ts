import {Component, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dynamic-form-textarea',
  template: `
  <div class="col-md-12">
    <div class="form-group" [formGroup]="group">
      <label class="control-label">{{ config.label }}</label>
      <input type="number" class="form-control" value="{{ config.value }}" [attr.placeholder]="config.placeholder" [formControlName]="config.name"/>
    </div>
  </div>`
})
export class FormTextAreaComponent {
  config;
  group: FormGroup;
}
