import {Component, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dynamic-form-date',
  styleUrls: ['form-date.component.scss'],
  templateUrl: './form-date.component.html'
})
export class FormDateComponent {
  config;
  group: FormGroup;
}
