import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-multi-report-bar',
  templateUrl: 'dynamic-chart.component.html'
})

export class DynamicBarComponent implements OnInit, OnChanges {
  @Input()
  public chartData: any;
  @Input()
  public chartType: string;

  @Input()
  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  @Input()
  public chartLabels: string[] = ['Contagem Geral'];
  @Input()
  public chartLegend: boolean;

  constructor() {
    this.chartLegend = true;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('alteracoes', changes);
  }

  public chartClicked($event: any): void {
    // console.log($event);
  }

  public chartHovered($event: any): void {
    // console.log($event);
  }
}
