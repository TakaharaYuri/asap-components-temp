import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-multi-report-box',
  templateUrl: 'dynamic-multi-report.component.html'
})

export class DynamicMultiReportComponent implements OnInit {
  @Input()
  public title: string;
  @Input()
  public chartData: any;
  @Input()
  public chartType: string;
  @Input()
  public chartLabels: string[];
  @Input()
  public chartLegend: boolean;
  @Input()
  public chartOptions: boolean;
  public type: any;
  public chartDataPrepared: any;

  constructor() {
    this.chartLegend = true;
    this.chartType = 'bar';
    this.type = 'bar';
  }

  ngOnInit(): void {
    this.prepareData();
  }

  switchChartType(type: string): void {
    this.chartType = type;
    this.type = type;
    this.prepareData();
  }

  prepareData(): void {
    this.chartDataPrepared = undefined;
    this.chartLabels = undefined;
    console.log(this.chartData)
    if(this.chartData != ''){
      console.log('entroi aqui')
      const keys = Object.keys(this.chartData);
      if (this.chartType === 'bar') {
        const data = [];
        for (const key of keys) {
          const result = {data: [this.chartData[key]], label: key};
          data.push(result);
        }
        this.chartDataPrepared = data;
        console.log(this.chartDataPrepared)
      }
      if (this.chartType === 'doughnut') {
        this.chartType = 'doughnut';
        this.chartLabels = keys;
        const data = [];
        for (const key of keys) {
          data.push(this.chartData[key]);
        }
        this.chartDataPrepared = data;
      }
      if (this.chartType === 'pie') {
        this.chartLabels = keys;
        this.chartType = 'pie';
        const data = [];
        for (const key of keys) {
          data.push(this.chartData[key]);
        }
        this.chartDataPrepared = data;
      }
      // console.log('tipo', this.chartType);
      console.log(this.chartData)
      console.log(this.chartDataPrepared)
    }else{
      console.log('aki')
      let data = [{data: 0, label: ""}]
      this.chartDataPrepared = data;
    }
  }

  public chartClicked($event: any): void {
    // console.log($event);
  }

  public chartHovered($event: any): void {
    // console.log($event);
  }
}
