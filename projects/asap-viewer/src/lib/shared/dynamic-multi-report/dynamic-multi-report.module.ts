import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ChartsModule} from 'ng2-charts';
import {DynamicMultiReportComponent} from './dynamic-multi-report.component';
import {DynamicBarComponent} from './dynamic-chart/dynamic-chart.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    ChartsModule,
    FormsModule
  ],
  declarations: [
    DynamicMultiReportComponent,
    DynamicBarComponent
  ],
  exports: [
    DynamicMultiReportComponent,
    DynamicBarComponent
  ]
})

export class DynamicMultiReportModule {
}
