import {Injectable} from '@angular/core';
import {FileManagerModel} from '../models/file-manager.model';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { Global } from 'asap-crud';

@Injectable()
export class FileManagerService {

  constructor(private fileManagerModel: FileManagerModel, private httpClient: HttpClient, public global: Global) {
  }

  public uploadFile(data: any): Observable<any> {
    return this.fileManagerModel.uploadFile(this.serialize(data));
  }

  public removeFile(id: number): Observable<any> {
    if (id) {
      return this.fileManagerModel.deleteFile(id);
    } else {
      return null;
    }
  }

  public findFileCollection(): Observable<any> {
    return this.fileManagerModel.getAll();
  }

  public serialize(data: any): FileManagerModel {
    const fileManager = new FileManagerModel(this.httpClient, this.global);
    fileManager.name = data.name;
    fileManager.file = data;
    fileManager.description = data.type;
    return fileManager;
  }
}
