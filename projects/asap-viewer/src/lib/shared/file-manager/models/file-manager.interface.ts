import { Observable } from 'rxjs';

export interface FileManagerInterface {
  file: any,
  name: string,
  description: string,
  apiBaseUrl: string;

  populate(data: FileManagerInterface): FileManagerInterface;

  uploadFile(data: FileManagerInterface): Observable<any>;

  getAll(): Observable<any>;

  deleteFile(id: number): Observable<any>;

  changeFile(): Observable<any>;
}
