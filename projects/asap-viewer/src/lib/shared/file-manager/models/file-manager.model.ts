import {FileManagerInterface} from './file-manager.interface';
import { Observable } from 'rxjs';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Global } from 'asap-crud';

@Injectable()
export class FileManagerModel implements FileManagerInterface {

  file: any;
  name: string;
  description: string;
  apiBaseUrl: string;

  constructor(private http: HttpClient, public global: Global) {
    this.apiBaseUrl = this.global.apiURL();
  }

  populate(data: FileManagerInterface): FileManagerInterface {
    return undefined;
  }

  uploadFile(data: FileManagerModel): Observable<HttpEvent<{}>> {

    const formData: FormData = new FormData();
    formData.append('file', data.file);
    formData.append('name', data.name);
    formData.append('description', data.description);

    const headerOptions = {
      'Authorization': sessionStorage.getItem('token'),
      'Accept': 'application/json'
    };

    const req = new HttpRequest('POST', this.apiBaseUrl + '/api/v2/admin/file', formData, {
      reportProgress: true,
      responseType: 'json',
      headers: new HttpHeaders(headerOptions)
    });

    return this.http.request(req);


    // return this.http.post(this.apiBaseUrl + '/api/v2/admin/file', data, {headers: new HttpHeaders(headerOptions)});
  }

  getAll(): Observable<any> {
    return this.http.get(this.apiBaseUrl + '/api/v2/admin/file/all', {headers: this.getHeader()});
  }

  deleteFile(id: number): Observable<any> {
    return this.http.delete(this.apiBaseUrl + '/api/v2/admin/file/' + id, {headers: this.getHeader()});
  }

  changeFile(): Observable<any> {
    return undefined;
  }

  getHeader(): any {
    const token = sessionStorage.getItem('token');
    if (token) {
      const headerOptions = {
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      };
      return new HttpHeaders(headerOptions);
    }
  }
}
