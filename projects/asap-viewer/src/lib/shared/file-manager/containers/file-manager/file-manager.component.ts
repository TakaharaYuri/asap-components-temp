import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FileManagerModel} from '../../models/file-manager.model';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import { FileManagerService } from '../../services/file-manager.service';

declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-file-manager',
  exportAs: 'appFileManager',
  templateUrl: './file-manager.component.html',
  providers: [FileManagerService, FileManagerModel],
  styleUrls: ['file-manager.component.scss']
})

export class FileManagerComponent implements OnInit {

  @Output()
  event: EventEmitter<any> = new EventEmitter<any>();

  public selectedFile: any;
  public progress: { percentage: number } = {percentage: 0};
  public loading: boolean;
  public fileList: any[];

  constructor(private _fileManagerService: FileManagerService) {
    this.loading = false;
  }

  ngOnInit(): void {
    this.findAll();
  }

  showModal(): void {
    $('#appFileManager').modal('show');
  }

  closeModal(): void {
    $('#appFileManager').modal('hide');
  }

  public onChange($event): void {
    if ($event && this.whiteListUploads($event.target.files[0].type)) {
      this.selectedFile = $event.target.files[0];
    } else {
      swal({
        type: 'error',
        title: 'Erro ao carregar o arquivo.',
        text: 'Verifique se o arquivo tem o formato permitido.',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      });
      $('#file').val('');
    }
  }

  private whiteListUploads(fileType: any): boolean {
    switch (fileType) {
      case 'image/png':
        return true;
      case 'image/jpeg':
        return true;
      case 'image/jpg':
        return true;
      case 'image/gif':
        return true;
      case 'image/png':
        return true;
      case 'application/msword':
        return true;
      case 'application/pdf':
        return true;
      case 'application/zip':
        return true;
      case 'video/x-flv':
        return true;
      case 'video/mp4':
        return true;
      case 'application/x-mpegURL':
        return true;
      case 'video/MP2T':
        return true;
      case 'video/3gpp':
        return true;
      case 'video/quicktime':
        return true;
      case 'video/x-msvideo':
        return true;
      case 'video/x-ms-wmv':
        return true;
      default:
        return false;
    }
  }

  public submit() {
    this.progress.percentage = 0;
    this.loading = true;
    this._fileManagerService.uploadFile(this.selectedFile)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.event.emit(event.body.data);
          this.loading = false;
          this.closeModal();
        }
      });
    this.selectedFile = undefined;
  }

  public findAll(): void {
    this._fileManagerService.findFileCollection()
      .subscribe(
        res => {
          this.fileList = res.data;
        }
      )
  }

  public selectFile(item: any): void {
    this.event.emit(item);
    this.closeModal();
  }

  public deleteFromList(item: any): void {
    this._fileManagerService.removeFile(item.id)
      .subscribe(res => {
        swal({
          type: 'success',
          title: 'Removido com sucesso.',
          text: 'O arquivo selecido foi removido do sistema.',
          buttonsStyling: true,
          confirmButtonClass: 'btn btn-success'
        });
        this.findAll();
      }, err => {
        swal({
          type: 'error',
          title: 'Erro ao remover arquivo.',
          text: 'Tente novamente mais tarde.',
          buttonsStyling: true,
          confirmButtonClass: 'btn btn-success'
        });
      });
  }

}
