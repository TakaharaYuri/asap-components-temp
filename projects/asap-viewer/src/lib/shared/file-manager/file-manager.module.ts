import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FileManagerComponent} from './containers/file-manager/file-manager.component';
import {FileManagerDirective} from './containers/file-manager-directive/file-manager.directive';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
  ],
  declarations: [
    FileManagerComponent,
    FileManagerDirective
  ],
  exports: [
    FileManagerComponent
  ],
})
export class FileManagerModule {
}
