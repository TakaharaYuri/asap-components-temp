import {Component, EventEmitter, Output} from '@angular/core';
import {BoxInterface} from '../../models/box.interface';

@Component({
  selector: 'app-dynamic-config-list',
  styleUrls: ['config-item-list.component.scss'],
  templateUrl: './config-item-list.component.html'
})
export class ConfigItemListComponent implements BoxInterface {
  public config;
  public data: any;

  @Output()
  changed: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  detailItem: EventEmitter<any> = new EventEmitter<any>();

  changeStateEventFire(): void {
    this.changed.emit(this.config);
  }

  goToNextStep(): void {
    this.detailItem.emit(this.config.sub);
  }
}
