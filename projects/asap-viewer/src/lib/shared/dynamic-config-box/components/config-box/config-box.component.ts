import {Component, EventEmitter, Output, OnInit} from '@angular/core';
import {BoxInterface} from '../../models/box.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dynamic-config-box',
  styleUrls: ['config-box.component.scss'],
  templateUrl: './config-box.component.html'
})
export class ConfigBoxComponent implements BoxInterface , OnInit {
  public config;
  public data: any;
  public idCliente;

  constructor(
    private activivatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
      this.getParams();
    }

  @Output()
  changed: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  detailItem: EventEmitter<any> = new EventEmitter<any>();

  changeStateEventFire(): void {
    this.changed.emit(this.config);
  }

  goToNextStep(): void {
    this.detailItem.emit(this.config);
  }

  public getParams(): void {
    this.activivatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.idCliente = params['id'];
      }
    });
  }

}
