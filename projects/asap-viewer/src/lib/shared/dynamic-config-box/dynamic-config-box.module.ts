import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DynamicConfigBoxDirective} from './containers/dynamic-config-box/dynamic-config-box.directive';
import {DynamicConfigComponent} from './containers/dynamic-config/dynamic-config.component';
import {ConfigBoxComponent} from './components/config-box/config-box.component';
import {ConfigItemListComponent} from './components/config-item-list/config-item-list.component';

import { MdModule } from '../../md/md.module';
import { MaterialModule } from 'app/material.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    MdModule,
    MaterialModule
  ],
  declarations: [
    DynamicConfigBoxDirective,
    DynamicConfigComponent,
    ConfigBoxComponent,
    ConfigItemListComponent,
  ],
  exports: [
    DynamicConfigComponent
  ],
  entryComponents: [
    ConfigBoxComponent,
    ConfigItemListComponent,
  ]
})
export class DynamicConfigBoxModule {
}
