import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-dynamic-config',
  exportAs: 'appDynamicConfig',
  styleUrls: ['./dynamic-config.component.scss'],
  templateUrl: './dynamic-config.component.html'
})
export class DynamicConfigComponent implements OnInit {

  @Input()
  config: any[] = [];

  @Input()
  data: any;

  @Output()
  changed: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  detailItem: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
  }

  dataChanged($event): void {
    this.changed.emit($event);
  }

  goToNextStep($value): void {
    this.detailItem.emit($value);
  }

}
