import {ComponentFactoryResolver, Directive, EventEmitter, Input, OnInit, Output, ViewContainerRef} from '@angular/core';
import {ConfigBoxComponent} from '../../components/config-box/config-box.component';

const components = {
  box: ConfigBoxComponent,
};

@Directive({
  selector: '[appDynamicConfigBox]'
})
export class DynamicConfigBoxDirective implements OnInit {

  @Input()
  public config;

  @Input()
  public data;

  @Output()
  changed: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  detailItem: EventEmitter<any> = new EventEmitter<any>();

  public component;

  constructor(private resolver: ComponentFactoryResolver,
              private container: ViewContainerRef) {
  }

  ngOnInit(): void {
    const component = components[this.config.type];
    if (component !== undefined) {
      const factory = this.resolver.resolveComponentFactory<any>(component);
      this.component = this.container.createComponent(factory);
      this.component.instance.config = this.config;
      this.component.instance.data = this.data;
      this.component.instance.changed.subscribe(
        res => {
          this.changed.emit(res);
        }
      );
      this.component.instance.detailItem.subscribe(detail => {
        this.detailItem.emit(detail);
      });
    }
  }

}
