import {BoxConfigInterface} from './box-config.interface';

export interface BoxInterface {
  config: BoxConfigInterface,
  data: any[]
}
