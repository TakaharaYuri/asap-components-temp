export interface ItemListConfigInterface {
  title: string,
  tag: string,
  icon: string,
  description: string,
  type: string
}
