import {ItemListConfigInterface} from './item-list-config.interface';

export interface BoxConfigInterface {
  title: string,
  tag: string,
  icon: string,
  description: string,
  type: string,
  sub: ItemListConfigInterface[],
  enabled?: boolean
}
