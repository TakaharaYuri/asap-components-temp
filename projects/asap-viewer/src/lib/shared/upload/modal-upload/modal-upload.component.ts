import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { storage } from 'firebase';
import { FirebaseApp } from '@angular/fire';

declare const swal: any;

@Component({
  selector: 'app-modal-upload-plugin',
  templateUrl: './modal-upload.component.html',
  styleUrls: ['./modal-upload.component.css']
})

export class ModalUploadPluginComponent {

    typeImage = false;
    uploadMode = true;
    fileName:any;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    cropperReady = false;
    percentage = 0;
    storeRef: storage.Reference;
    archive: any;
    resizeToWidth: number;
    aspectRatio: string;
    horizontal: boolean;

    constructor(
        public dialogRef: MatDialogRef<ModalUploadPluginComponent>,
        @Inject(MAT_DIALOG_DATA) public data,
        @Inject(FirebaseApp) fb: any
    ) {
        this.storeRef = fb.storage().ref();
        data.type === 'image' ? this.typeImage = true : this.typeImage = false;
        this.resizeToWidth = data.resizeToWidth;
        this.aspectRatio = data.aspectRatio;
        this.horizontal = data.horizontal;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    reset() {
      this.croppedImage = '';
      this.fileName = '';
      this.imageChangedEvent = '';
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCroppedBase64(image: string) {
        this.croppedImage = image;
    }
    imageLoaded() {
      this.cropperReady = true;
    }
    imageLoadFailed () {
      console.log('Load failed');
    }

    UploadImage(event) {
        if(event.srcElement.files[0]){
            this.imageChangedEvent = event;
            this.fileName = event.srcElement.files[0].name
        }
    }

    dataURItoBlob(dataURI) {
        let type = 'image/png'
        var byteString = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        var bb = new Blob([ab], { type: type });
        return bb;
    }

    sendImage() {
        let imagee = this.dataURItoBlob(this.croppedImage)
        let caminho = this.storeRef.child('images/'+Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10)+this.fileName);
        let tarefa = caminho.put(imagee);
        tarefa.on('state_changed', (snapshot)=>{
            this.percentage =  Math.round((tarefa.snapshot.bytesTransferred / tarefa.snapshot.totalBytes) * 100)
            // console.log('porcentagem', Math.round((tarefa.snapshot.bytesTransferred / tarefa.snapshot.totalBytes) * 100));
            // console.log('state', tarefa.snapshot.state);
            }, error => {
                // Tratar possíveis erros
            }, () => {
             if(tarefa.snapshot.state==='success'){
                caminho.getDownloadURL().then(url => {
                // console.log(url);
                this.dialogRef.close({ path: url, name: this.fileName, size: tarefa.snapshot.totalBytes });
                });
             }
        });
    }

    UploadArchive(event) {
        if (event && this.whiteListUploads(event.target.files[0].type)) {
            this.fileName = event.srcElement.files[0].name;
            this.archive = event.srcElement.files[0];
        } else {
            swal({
              type: 'error',
              title: 'Erro ao carregar o arquivo.',
              text: 'Verifique se o arquivo tem o formato permitido.',
              buttonsStyling: true,
              confirmButtonClass: 'btn btn-success'
            });
        }
    }

    sendArchive() {
        let caminho = this.storeRef.child('archives/' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10) + '_' + this.fileName);
        let tarefa = caminho.put(this.archive);
        tarefa.on('state_changed', (snapshot)=>{
            this.percentage =  Math.round((tarefa.snapshot.bytesTransferred / tarefa.snapshot.totalBytes) * 100)
            // console.log('porcentagem', Math.round((tarefa.snapshot.bytesTransferred / tarefa.snapshot.totalBytes) * 100));
            // console.log('state', tarefa.snapshot.state);
            }, error => {
                // Tratar possíveis erros
            }, () => {
             if(tarefa.snapshot.state==='success'){
                caminho.getDownloadURL().then(url => {
                // console.log(url);
                this.dialogRef.close({ path: url, name: this.fileName });
                });
             }
        });
    }

    public loadImageFailed() {}

    private whiteListUploads(fileType: any): boolean {
        switch (fileType) {
          case 'image/png':
            return true;
          case 'image/jpeg':
            return true;
          case 'image/jpg':
            return true;
          case 'image/gif':
            return true;
          case 'image/png':
            return true;
          case 'application/msword':
            return true;
          case 'application/pdf':
            return true;
          case 'application/zip':
            return true;
          case 'video/x-flv':
            return true;
          case 'video/mp4':
            return true;
          case 'application/x-mpegURL':
            return true;
          case 'video/MP2T':
            return true;
          case 'video/3gpp':
            return true;
          case 'video/quicktime':
            return true;
          case 'video/x-msvideo':
            return true;
          case 'video/x-ms-wmv':
            return true;
          default:
            return true;
        }
    }
}
