/* ==== Imports Módulos === */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ImageCropperModule } from 'ngx-image-cropper';
import {
    MatButtonModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

/* ==== Imports Components === */
import { UploadPluginComponent } from './upload.component'
import { ModalUploadPluginComponent } from './modal-upload/modal-upload.component';
import { FirebaseApp } from '@angular/fire';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSnackBarModule,
    ImageCropperModule
  ],
  declarations: [
    UploadPluginComponent,
    ModalUploadPluginComponent
  ],
  exports: [
    UploadPluginComponent,
    ModalUploadPluginComponent
  ],
  providers: [
    FirebaseApp
  ],
  entryComponents: [ 
    ModalUploadPluginComponent 
  ]
})

export class UploadModule { }
