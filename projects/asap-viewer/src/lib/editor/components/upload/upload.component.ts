import { Component, Input, Output, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { TrailContentService } from '../../../@core/services/trail-content.service';
import { UploadComponent } from '../../../@core/models/item-component';

@Component({
  selector: 'app-editor-component-upload',
  templateUrl: 'upload.component.html',
  styleUrls: ['./upload.component.css'],
  providers: [TrailContentService]
})

export class EditorUploadComponent implements OnInit {
  @Input() item: UploadComponent;
  @Input() id: number;

  @Output() public event = new EventEmitter<any>();

  contentId: number;

  constructor(
    public _trailContentService: TrailContentService
  ) { }

  ngOnInit() {
    this.item.file = [];

    if(this.id) {
      this._trailContentService.all(this.id).subscribe(res => {
        const check = res.data.find(item => {
          return item.value.type === 'upload';
        });
        if(check) {
          this.contentId = check.id;
          this.item.file = check.value.file;
        }
      });
    }
  }

  onDone() {
    this.event.emit();
  }

  getArchive($event) {
    this.item.file.push($event);
    this.save();
  }

  save() {
    const data = {
      'type': 'upload',
      'file': this.item.file
    };

    if(this.contentId) {
      this._trailContentService.update(this.contentId, data).subscribe(res => {
        console.log(res);       
      });
    }else{
      this._trailContentService.save(this.id, data).subscribe(res => {
        this.contentId = res.data.id;
      });
    }
  }

  remove(f: any) {
    const index = this.item.file.indexOf(f);
    this.item.file.splice(index, 1);

    this.save();
  }
  
}
