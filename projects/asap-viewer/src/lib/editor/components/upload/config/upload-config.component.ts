import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { UploadComponent } from '../../../../@core/models/item-component';

declare var $: any;

@Component({
  selector: 'app-editor-component-upload-config',
  templateUrl: 'upload-config.component.html',
  styleUrls: ['../../../editor.component.css']
})
export class EditorUploadConfigComponent {
  @Input() item: UploadComponent;
  @Output() public sendEvent = new EventEmitter<any>();

  constructor(
  ){
    
  }

  setAlign(element: any, align: string) {
    element.align = align;
    this.emitSendEvent();
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

}
