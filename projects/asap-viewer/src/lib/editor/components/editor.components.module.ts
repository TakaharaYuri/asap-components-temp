import { CommonModule } from '@angular/common';
import { FileManagerModule } from '../../shared/file-manager/file-manager.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TagInputModule } from 'ngx-chips';
import { ColorPickerModule } from 'ngx-color-picker';

import { DynamicInputComponent } from './dynamic-input/dynamic-input.component';
import { VideoComponent } from './video/video.component';
import { ImageComponent } from './image/image.component';
import { EditorImageConfigComponent } from './image/config/image-config.component';

import { EditorChallengeComponent } from './challenge/challenge.component';
import { EditorChallengeConfigComponent } from './challenge/config/challenge-config.component';

import { EditorConclusionComponent } from './conclusion/conclusion.component';
import { EditorConclusionConfigComponent } from './conclusion/config/conclusion-config.component';

import { EditorStarPointComponent } from './star-point/star-point.component';
import { EditorStarPointConfigComponent } from './star-point/config/star-point-config.component';

import { EditorDynamicTableComponent } from './dynamic-table/dynamic-table.component';
import { EditorDynamicTableConfigComponent } from './dynamic-table/config/dynamic-table-config.component';

import { EditorIntroComponent } from './intro/intro.component';
import { EditorIntroConfigComponent } from './intro/config/intro-config.component';

import { EditorLongTextComponent } from './long-text/long-text.component';
import { EditorLongTextConfigComponent } from './long-text/config/long-text-config.component';

import { EditorMultiChoiceComponent } from './multichoice/multichoice.component';

import { EditorMultipleQuestionsComponent } from './multiple-questions/multiple-questions.component';
import { EditorMultipleQuestionsConfigComponent } from './multiple-questions/config/multiple-questions-config.component';

import { EditorSalesChannelComponent } from './sales-channel/sales-channel.component';
import { EditorSalesChannelConfigComponent } from './sales-channel/config/sales-channel-config.component';

import { EditorSheetCorrectIncorrectComponent } from './sheet-correct-incorrect/sheet-correct-incorrect.component';
import { EditorSheetCorrectIncorrectConfigComponent } from './sheet-correct-incorrect/config/sheet-correct-incorrect-config.component';

import { EditorYesNoComponent } from './yes-no/yes-no.component';
import { EditorYesNoConfigComponent } from './yes-no/config/yes-no-config.component';

import { EditorUploadComponent } from './upload/upload.component';
import { EditorUploadConfigComponent } from './upload/config/upload-config.component';

import { EditorDownloadComponent } from './download/download.component';
import { EditorDownloadConfigComponent } from './download/config/download-config.component';

import { EditorRatingComponent } from './rating/rating.component';
import { EditorRatingConfigComponent } from './rating/config/rating-config.component';

import { ForumComponent } from './forum/forum.component';
import { EditorForumConfigComponent } from './forum/config/forum-config.component';

import { MomentModule } from 'angular2-moment';
import { NgxNouisliderModule } from 'ngx-nouislider';
import { MatSelectModule, MatStepperModule, MatRadioModule, MatTooltipModule, MatSliderModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { UploadModule } from '../../shared/upload/upload.module';
import { PipesModule } from '../../@core/pipes/pipe.module';
import { EditorAvaliationComponent } from './avaliation/avaliation.component';

const components = [
    DynamicInputComponent,
    EditorChallengeComponent,
    EditorChallengeConfigComponent,
    EditorConclusionComponent,
    EditorConclusionConfigComponent,
    EditorDynamicTableComponent,
    EditorDynamicTableConfigComponent,
    EditorIntroComponent,
    EditorIntroConfigComponent,
    EditorLongTextComponent,
    EditorLongTextConfigComponent,
    EditorMultiChoiceComponent,
    EditorMultipleQuestionsComponent,
    EditorMultipleQuestionsConfigComponent,
    EditorSalesChannelComponent,
    EditorSalesChannelConfigComponent,
    EditorSheetCorrectIncorrectComponent,
    EditorSheetCorrectIncorrectConfigComponent,
    EditorYesNoComponent,
    EditorYesNoConfigComponent,
    VideoComponent,
    ImageComponent,
    EditorImageConfigComponent,
    EditorUploadComponent,
    EditorUploadConfigComponent,
    EditorDownloadComponent,
    EditorDownloadConfigComponent,
    EditorStarPointComponent,
    EditorStarPointConfigComponent,
    EditorRatingComponent,
    EditorRatingConfigComponent,
    ForumComponent,
    EditorForumConfigComponent,
    EditorAvaliationComponent
];

@NgModule({
  imports: [
    CommonModule,
    FileManagerModule,
    FormsModule,
    NgxNouisliderModule,
    ReactiveFormsModule,
    TagInputModule,
    ColorPickerModule,
    MomentModule,
    MatSelectModule,
    TranslateModule,
    UploadModule,
    MatStepperModule,
    MatRadioModule,
    MatTooltipModule,
    MatSliderModule,
    PipesModule
  ],
  declarations: [
    ... components
  ],
  exports: [
    ... components
  ]
})

export class EditorComponentsModule { }
