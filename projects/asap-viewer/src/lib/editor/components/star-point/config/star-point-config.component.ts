import { Component, Input, Output, EventEmitter } from '@angular/core';
import { StarPointComponent } from '../../../../@core/models/item-component';

declare var $: any;

@Component({
  selector: 'app-editor-component-star-point-config',
  templateUrl: 'star-point-config.component.html',
  styleUrls: ['../../../editor.component.css']
})

export class EditorStarPointConfigComponent {
  @Input() item: StarPointComponent;
  @Output() public sendEvent = new EventEmitter<any>();

  setAlign(element: any, align: string) {
    element.align = align;
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }
}
