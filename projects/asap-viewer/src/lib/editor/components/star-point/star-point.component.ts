import { Component, Input, Output, EventEmitter } from '@angular/core';
import { StarPointComponent } from '../../../@core/models/item-component';

declare var $: any;

@Component({
  selector: 'app-editor-component-star-point',
  templateUrl: 'star-point.component.html',
  styleUrls: ['./star-point.component.css']
})

export class EditorStarPointComponent {
  @Input() item: StarPointComponent;
  @Output() public event = new EventEmitter<any>();

  toNext() {
    this.event.emit();
  }

}
