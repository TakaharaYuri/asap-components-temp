import { Component, Input } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { TrailContentService } from '../../../@core/services/trail-content.service';
import { TableComponent } from '../../../@core/models/item-component';

@Component({
  selector: 'app-editor-component-dynamic-table',
  templateUrl: 'dynamic-table.component.html',
  styleUrls: ['../../editor.component.css', './dynamic-table.component.css'],
  providers: [TrailContentService]
})

export class EditorDynamicTableComponent {
  @Input() item: TableComponent;
  @Input() id: number;

  responseValue: any;

  contentId: number;
  
  public view: Array<any>;

  form: FormGroup;

  showSaved: boolean;

  constructor(
    public _trailContentService: TrailContentService,
    private fb: FormBuilder
  ) {
    this.view = [];
  }

  generateFormTable() {
    
    let lineArray = [];

    if(!this.item.userActive) {
      for(let i = 0; i < this.item.countLines; i++){
        let arrayControls = [];

        for(let col of this.item.collumns) {
          arrayControls.push(new FormControl(''));
        }
    
        lineArray.push(new FormArray(arrayControls));
      }
    }else{
      let arrayControls = [];

      for(let col of this.item.collumns) {
        arrayControls.push(new FormControl(''));
      }

      lineArray.push(new FormArray(arrayControls));
    }

    this.form = new FormGroup({
      items: new FormArray(lineArray)
    });

  }

  ngOnInit() {
    this.generateFormTable();

    if(this.id) {
      this._trailContentService.all(this.id).subscribe(res => {
        const check = res.data.find(item => {
          return item.value.type === 'dynamic-table';
        });
        if(check) {
          this.contentId = check.id;


          let lineArray = [];

    
          for(let n of check.value.notes){
            let arrayControls = [];

            for(let c of n) {
              arrayControls.push(new FormControl(c));
            }
        
            lineArray.push(new FormArray(arrayControls));
          }

          this.form = new FormGroup({
            items: new FormArray(lineArray)
          });

        }
      });
    }
  }

  calc() {
    let totals = [];
  }

  removeView(index: number) {
    console.log(index);
    let itemsArray = this.form.get('items') as FormArray;
    itemsArray.removeAt(index);
  }

  createView() {
    let arrayControls = [];

      for(let col of this.item.collumns) {
        arrayControls.push(new FormControl(''));
      }
  
      let itemsArray = this.form.get('items') as FormArray;
      itemsArray.push(new FormArray(arrayControls));
  }

  onSubmit() {

    const data = {
      'type': 'dynamic-table',
      'notes': this.form.value.items
    };

    this.showSaved = true;
    
    setInterval(() => {
      this.showSaved = false;
    }, 1000); 

    if(this.contentId) {
      this._trailContentService.update(this.contentId, data).subscribe(res => {
        console.log(res);
      });
    }else{
      this._trailContentService.save(this.id, data).subscribe(res => {
        this.contentId = res.data.id;
      });
    }
  }

}
