import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TrailContentService } from '../../../@core/services/trail-content.service';
import { MultiChoiceComponent, MultiChoiceOptions } from '../../../@core/models/item-component';
import { Global } from 'asap-crud';

declare var $: any;

@Component({
  selector: 'app-editor-component-multichoice',
  templateUrl: 'multichoice.component.html',
  styleUrls: ['../../editor.component.css', './multichoice.component.css'],
  providers: [TrailContentService]
})

export class EditorMultiChoiceComponent implements OnInit {
  @Input() id: number;
  @Input() item: MultiChoiceComponent;
  @Output() public event = new EventEmitter<any>();

  listMarkerRoman = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
  listMarkerLetter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
  listMarkerNumber = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

  blocked = false;

  contentId: number;

  constructor(
    private global: Global,
    private _trailContentService: TrailContentService
  ) {

  }

  ngOnInit() {
    if (this.id) {
      this._trailContentService.all(this.id).subscribe(res => {
        const check = res.data.find(item => {
          return item.value.type === 'multichoice';
        });
        if (check) {
          console.log('ngOnInit: check', check);
          const selected = check.value.value.value;
          for (const opt of this.item.options) {
            opt.selected = false;
          }
          const selectedOption = this.item.options.find(item => {
            return item.value === selected;
          });
          selectedOption.selected = true;
          $('#radio-' + selected).prop('checked', true);
          this.blocked = true;
        }
      });
    }
  }

  onSelectionChange(option: MultiChoiceOptions): void {
    for (const opt of this.item.options) {
      opt.selected = false;
    }
    option.selected = true;
  }

  onSubmit() {

    const correctOption = this.item.options.find(item => {
      return item.correct === true;
    });

    const selectedOption = this.item.options.find(item => {
      return item.selected === true;
    });

    if (!selectedOption) {
      this.global.alert(
        'Ops...',
        'Selecione uma opção',
        'error'
      );
      return;
    }

    if (this.item.verification === 'single') {
      this.blocked = true;
    }

    this.save();

    if (correctOption.selected) {
      this.global.alert(
        'Parabéns!',
        'Você selecionou a opção correta!',
        'success'
      )
      this.blocked = true;
    } else {
      if (this.item.feedback === 'GENERAL') {
        this.global.alert(
          'Opção incorreta!',
          this.item.feedback_text,
          'error'
        );
      } else {
        this.global.alert(
          'Opção incorreta!',
          selectedOption.messageInError,
          'error'
        );
      }
    }

  }

  save() {
    const selectedOption = this.item.options.find(item => {
      return item.selected === true;
    });

    const data = {
      'type': 'multichoice',
      'value': selectedOption
    };

    if (this.contentId) {
      this._trailContentService.update(this.contentId, data).subscribe(res => {
        console.log(res);
      });
    } else {
      this._trailContentService.save(this.id, data).subscribe(res => {
        this.contentId = res.data.id;
        console.log(res);
      });
    }
  }

  goNext() {
    this.event.emit();
  }
}
