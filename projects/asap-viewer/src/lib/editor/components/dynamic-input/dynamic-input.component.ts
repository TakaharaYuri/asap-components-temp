import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-editor-component-dynamic-input',
  templateUrl: 'dynamic-input.component.html',
  styleUrls: ['../../editor.component.css', 'dynamic-input.component.css']
})

export class DynamicInputComponent {
  @Input() input: any;

  constructor(
  ){ }

}
