/* ==== Imports Componentes === */
import { Component, OnInit } from '@angular/core';

/* ==== Decorators === */
@Component({
  selector: 'app-editor-component-yes-no',
  templateUrl: 'yes-no.component.html',
  styleUrls: ['../../editor.component.css']
})

export class EditorYesNoComponent {

  constructor() {
  }

  ngOnInit() {
  }

}
