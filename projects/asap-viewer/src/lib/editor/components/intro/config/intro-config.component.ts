import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { IntroComponent } from '../../../../@core/models/item-component';
import { FileManagerComponent } from '../../../../shared/file-manager/containers/file-manager/file-manager.component';

declare var $: any;

@Component({
  selector: 'app-editor-component-intro-config',
  templateUrl: 'intro-config.component.html',
  styleUrls: ['../../../editor.component.css']
})
export class EditorIntroConfigComponent {
  @Input() item: IntroComponent;
  @Output() public sendEvent = new EventEmitter<any>();
  @ViewChild(FileManagerComponent,{static:false}) appFileManager: FileManagerComponent;

  title = new FormControl();
  mediaText = new FormControl();
  videoUrl = new FormControl();
  mediaTitle = new FormControl();
  mediaDescription = new FormControl();
  button = new FormControl();

  constructor(
    private _sanitizer: DomSanitizer
  ){
    this.title.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((title: string) => {
        this.emitSendEvent();
      });
    this.mediaText.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((mediaText: string) => {
        this.emitSendEvent();
      });
    this.videoUrl.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((videoUrl: string) => {
        this.emitSendEvent();
      });
    this.mediaTitle.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((videoUrl: string) => {
        this.emitSendEvent();
      });
    this.mediaDescription.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((videoUrl: string) => {
        this.emitSendEvent();
      });
    this.button.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((videoUrl: string) => {
        this.emitSendEvent();
      });
  }

  setAlign(element: any, align: string) {
    element.align = align;
    this.emitSendEvent();
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

  showFileManager(): void {
    this.appFileManager.showModal();
  }

  uploadSubmited($event) {
    if($event.data) {
      this.item.media.image = $event.data.url;
    }else{
      this.item.media.image = $event.url;
    }
    this.emitSendEvent();
  }

  videoChanged() {
    if(this.item.media.type == 'youtube'){
      this.item.link = this.sanitizer(this.item.media.youtube, 'youtube');
    }
    if(this.item.media.type == 'vimeo'){
      this.item.link = this.sanitizer(this.item.media.vimeo, 'vimeo');
    }

    $('iframe').attr('src', $('iframe').attr('src'));
  }

  private sanitizer(url: string, type: string): SafeResourceUrl {

    let uri;

    if (!url) {
      return null;
    }

    if (type === 'youtube') {
      const code = this.getYoutubeID(url);
      uri = 'https://www.youtube.com/embed/' + code + '?rel=0&amp;showinfo=0';
    }

    if (type === 'vimeo') {
      const code = this.getVimeoID(url);
      uri = 'https://player.vimeo.com/video/' + code;
    }

    if (type === 'file') {
      uri = url
    }

    return this._sanitizer.bypassSecurityTrustResourceUrl(uri);
  }

  getYoutubeID(url: string): string {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match && match[7].length==11) ? match[7] : '';
  }

  getVimeoID(url: string): string {
    var regExp = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/;
    var match = url.match(regExp);
    return (match && match[3]) ? match[3] : '';
  }

  getPhoto($event) {
    this.item.media.image = $event.path;
  }

}
