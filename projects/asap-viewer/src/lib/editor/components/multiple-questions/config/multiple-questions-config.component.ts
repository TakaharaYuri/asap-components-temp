import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MultipleQuestionComponent, MultipleQuestion, MultipleQuestionOption } from '../../../../@core/models/item-component';

@Component({
  selector: 'app-editor-component-multiple-questions-config',
  templateUrl: 'multiple-questions-config.component.html',
  styleUrls: ['../../../editor.component.css']
})

export class EditorMultipleQuestionsConfigComponent {

  @Input() item: MultipleQuestionComponent;
  @Output() public sendEvent = new EventEmitter<any>();

  insertQuestion() {
      this.item.questions.push(new MultipleQuestion);
  }

  insertOption(questionIndex: number) {
      this.item.questions[questionIndex].options.push(new MultipleQuestionOption);
  }

  removeQuestion(index: number): void {
      this.item.questions.splice(index, 1);
  }

  removeOption(index: number, indexOption: number): void {
      this.item.questions[index].options.splice(indexOption, 1);
  }

  insertImage(option) {
    // client.pick({
    //     fromSources:["local_file_system"],
    //     maxFiles:1,
    //     lang:"pt"
    //   }).then(response => {
    //     option.image = response.filesUploaded["0"].url
    //   });
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

  getPhoto(option, $event) {
    option.image = $event.path;
  }


}
