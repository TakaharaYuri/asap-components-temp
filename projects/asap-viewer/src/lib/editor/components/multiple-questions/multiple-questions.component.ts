import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Global } from 'asap-crud';
import { TrailContentService } from '../../../@core/services/trail-content.service';

@Component({
  selector: 'app-editor-component-multiple-questions',
  templateUrl: 'multiple-questions.component.html',
  styleUrls: ['../../editor.component.css'],
  providers: [TrailContentService]
})

export class EditorMultipleQuestionsComponent implements OnInit {

  @Input() item: any;
  @Input() id: number;
  @Output() public event = new EventEmitter<any>();

  constructor(
    public global: Global,
    public _trailContentService: TrailContentService
    
    ) {
    
  }

  ngOnInit() {
    console.log('ITEM', this.item);
      for(let i of this.item.questions) {
        console.log('Log', i);
        // i.sended = false;
      }
  }

  storeAnswer(question, option) {
      question.correct = option.correct;
  }

  verify(question) {
    if(question.correct) {
      this.global.alert('Acertou', 'A opção seleciona está correta!', 'success');
      question.sended = true;
      this.saveAnswer(question);
    }else{
      this.global.alert('Ops...', 'Você não selecionou a opção correta', 'error');
    }
  }

  saveAnswer(question) {
    this._trailContentService.save(this.id, {type: 'multiple-questions', data: question}).subscribe(res => {
      console.log(res);
    });
  }

  goNext() {
    this.event.emit();
  }

}
