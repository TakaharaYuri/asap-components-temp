import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ConclusionComponent } from '../../../../@core/models/item-component';

declare var $: any;

@Component({
  selector: 'app-editor-component-conclusion-config',
  templateUrl: 'conclusion-config.component.html',
  styleUrls: ['../../../editor.component.css']
})
export class EditorConclusionConfigComponent {
  @Input() item: ConclusionComponent;
  @Output() public sendEvent = new EventEmitter<any>();

  setAlign(element: any, align: string) {
    element.align = align;
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

}
