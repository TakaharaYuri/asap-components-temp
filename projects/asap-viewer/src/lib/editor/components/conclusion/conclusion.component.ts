import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import {
  trigger,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger,
  animateChild
} from '@angular/animations';

import { ActivatedRoute } from '@angular/router';
import { TrilhaPerformanceService } from '../../../@core/services/trilha/trilha-performance.service';
import { TrilhaService } from '../../../@core/services/trilha/trilha.service';
import { TrilhaRatingService } from '../../../@core/services/trilha/trilha-rating.service';
import { ConclusionComponent } from '../../../@core/models/item-component';

@Component({
  selector: 'app-editor-component-conclusion',
  templateUrl: 'conclusion.component.html',
  styleUrls: [
    './conclusion.component.css'
  ],
  providers: [TrilhaPerformanceService, TrilhaService, TrilhaRatingService],
  animations: [
    trigger('icon', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', display: 'none', opacity: 0 }),
        animate(
          '500ms cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({ transform: 'scale(1.3)', display: 'block', opacity: 1 })
        ),
        animate(
          '300ms cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({ transform: 'scale(1)' })
        )
      ])
    ]),
    trigger('icons', [
      transition(':enter', [query('@icon', stagger(500, animateChild()))])
    ])
  ]
})
export class EditorConclusionComponent implements OnInit {
  @Input()
  id: number;
  @Input()
  item: ConclusionComponent;
  @Output()
  public event = new EventEmitter<any>();
  @Output()
  public rating = new EventEmitter<any>();

  routeId: number;

  // 0 == mini-etapa; 1 == etapa-develop; 2 == etapa-articles;
  miniType: number;
  public performance: any;
  public sequence: any;
  public loading: boolean;

  badge: any;

  loggedUser: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private _trilhaService: TrilhaService,
    private _trilhaPerformanceService: TrilhaPerformanceService,
    private _trilhaRatingService: TrilhaRatingService
  ) {
    this.loading = true;
    this.getLoggedUser();
    this.activatedRoute.params.subscribe(param => {
      this.routeId = +param['id'];
      this.getStageData();
    });
  }

  ngOnInit() {
    const path = this.activatedRoute.snapshot.routeConfig.path.split('/');
    if (path[0] === 'mini-etapa') {
      this.miniType = 0;
    } else if (path[0] === 'etapa-develop') {
      this.miniType = 1;
    } else if (path[0] === 'etapa-artigo') {
      this.miniType = 2;
    }
    this.getPerformance();
  }

  getPerformance() {
    if (this.id) {
      this.sequence = [];
      this._trilhaPerformanceService.getResource(this.id)
        .subscribe(res => {
          this.performance = res.data.performance.toFixed(0);

          res.data.sequence.map(item => {
            if (typeof item.value.value.correct !== 'undefined') {
              this.sequence.push(item.value.value.correct);
            } else {
              this.sequence.push(false);
            }
          });

          this.loading = false;
        });
    }
  }

  getLoggedUser() {
    const sessionLoggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedUser = sessionLoggedUser.data;
  }

  getStageData(): void {
    this._trilhaService.getResource(this.routeId).subscribe(res => {
      if (res.data.length > 0) {
        this.badge = res.data[0].arquivo;
      }
    });
  }

  onDone() {
    this.checkEvaluate();
  }

  checkEvaluate() {
    this._trilhaRatingService.getResource(this.routeId)
      .subscribe(res => {
      if (res.data.rating) {
        this.onNext();
      } else {
        this.rating.emit();
      }
    })
  }

  onNext() {
    this.event.emit();
  }

}
