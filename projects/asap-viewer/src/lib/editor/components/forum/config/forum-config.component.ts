import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ForumService } from '../../../../@core/services/forum/forum.service';

@Component({
  selector: 'app-editor-component-forum-config',
  templateUrl: 'forum-config.component.html',
  styleUrls: ['../../../editor.component.css'],
  providers: [ForumService]
})
export class EditorForumConfigComponent implements OnInit {
  @Input() item: any;
  @Output() public sendEvent = new EventEmitter<any>();

  forumArray: any;

  constructor(
    private _forumClientService: ForumService
  ) {
  }

  ngOnInit() {
    this._forumClientService.getResources().subscribe(res => {
      this.forumArray = res.data;
    });
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

}
