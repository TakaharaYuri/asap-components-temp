import { Component, Input, OnInit, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ForumService } from '../../../@core/services/forum/forum.service';
import { ForumResponseService } from '../../../@core/services/forum/response.service';
import { ForumVoteService } from '../../../@core/services/forum/forum-vote.service';
import { Global } from 'asap-crud';

declare var $: any;

@Component({
  selector: 'app-editor-component-forum',
  templateUrl: 'forum.component.html',
  styleUrls: ['../../editor.component.css', './forum.component.scss'],
  providers: [ForumService, ForumResponseService, ForumVoteService]
})
export class ForumComponent implements OnInit, OnChanges {
  @Input() item: any;
  @Input() id: any;
  @Output() public event = new EventEmitter<any>();

  loading = true;

  form: FormGroup;
  formUpdate: FormGroup;
  response: any;
  forum: any;
  loggedUser: any;

  answered: boolean;

  score = 0;
  displayRatingScore = 0;

  constructor(
    private global: Global,
    private formBuilder: FormBuilder,
    private _ForumResponseService: ForumResponseService,
    private _forumService: ForumService,
    private _forumVoteService: ForumVoteService
  ) {
    moment.locale('pt-br');

    this.form = this.formBuilder.group({
      id: [''],
      resposta: ['']
    });
    this.formUpdate = this.formBuilder.group({
      id: [''],
      resposta: ['']
    });
  }

  ngOnInit() {
    this.getForum();
    this.getUserLogged();
  }

  getUserLogged(): any {
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedUser = loggedUser.data;
  }

  getForum() {
    this._forumService.getResource(this.id)
      .subscribe(res => {
        this.forum = res.data[0];
        this.loading = false;
        this.getResponse();
      })
  }

  getResponse() {
    this._ForumResponseService.getResource('user/' + this.id)
      .subscribe(res => {
        this.response = res.data.filter(item => {
          return item.user_id === this.loggedUser.id;
        });
        if (this.response.length > 0) {
          this.answered = true;
        }
        this.loading = false;
      })
  }

  save() {
    console.log('save');
    const form = this.form.value;
    this._forumService.createResource(form, {router: this.id})
    .subscribe(res => {
      this.answered = true;
      this.getResponse();
      this.ResetPost();
    })
  }


  ResetPost() {
    this.form.reset();
    $('#answer').collapse('hide');
  }

  onRemove(forum) {
    this.global.swal({
        title: 'Você quer remover esta pergunta?',
        text: 'Você não poderá reverter essa ação depois de confirmado',
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: true,
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não'
    }).then(result => {
      this._ForumResponseService.deleteResource(forum.id)
        .subscribe(res => {
          this.getResponse();
        })
    }, cancel => {}
    );
  }

  onClose(forum) {
    this.formUpdate.reset();
    return forum.disable = false;
  }

  onEdit(forum) {
    this.formUpdate.setValue({id: forum.id, resposta: forum.resposta})
    return forum.disable = true;
  }

  updateForm() {
    const form = this.formUpdate.value;
    this._forumService.updateResource(form)
      .subscribe(res => {
        this.getResponse();
        this.ResetPost();
      })
  }

  vote(item, number) {
    const form = { pontos: number }
    this._forumVoteService.createResource(form, {router: item.id})
    .subscribe(res => {
      this.getResponse();
    })
  }

  getHidden(item) {
    return item.hidden = !item.hidden;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.loading = true;
    // this.getForum();
  }

  goNext() {
    this.event.emit();
  }


}
