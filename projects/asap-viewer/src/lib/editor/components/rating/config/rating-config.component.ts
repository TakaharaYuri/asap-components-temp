import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-editor-component-rating-config',
  templateUrl: 'rating-config.component.html',
  styleUrls: ['../../../editor.component.css', '../rating.component.css']
})

export class EditorRatingConfigComponent {

  @Input() item: any;
  @Output() public sendEvent = new EventEmitter<any>();

  constructor() { }
  
  setAlign(element: any, align: string) {
    element.align = align;
  }
  
  emitSendEvent() {
    this.sendEvent.emit();
  }

}
