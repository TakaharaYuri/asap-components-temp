import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editor-component-rating',
  templateUrl: 'rating.component.html',
  styleUrls: ['../../editor.component.css', './rating.component.css']
})

export class EditorRatingComponent {
  @Input() item: any;
  @Output() event = new EventEmitter<any>();

  constructor() { }

  formatLabel(value: number | null) {
    value = (!value) ? 0 : value;
    return (value >= 1000) ? Math.round(value / 1000) + 'k' : value;
  }

  goNext() {
    this.event.emit();
  }
}
