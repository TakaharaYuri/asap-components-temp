import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { VimeoService } from '../../../../@core/services/vimeo.service';
import { ChallengeComponent, ChallengeMedia } from '../../../../@core/models/item-component';
import { FileManagerComponent } from '../../../../shared/file-manager/containers/file-manager/file-manager.component';
import { SanitizerHelper } from '../../../../@core/helpers/sanitizer.helper';
declare var $: any;

@Component({
  selector: 'app-editor-component-challenge-config',
  templateUrl: 'challenge-config.component.html',
  styleUrls: ['../../../editor.component.css'],
  providers: [ VimeoService ]
})
export class EditorChallengeConfigComponent {
  @Input() item: ChallengeComponent;
  @Output() public sendEvent = new EventEmitter<any>();
  @ViewChild(FileManagerComponent,{static:false}) appFileManager: FileManagerComponent;

  mediaToManipulate: ChallengeMedia;

  title = new FormControl();
  notesTitle = new FormControl();
  notesText = new FormControl();
  button = new FormControl();

  constructor(
    private _sanitizer: DomSanitizer,
    private _vimeoService: VimeoService
  ){
    this.title.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((text: string) => {
        this.emitSendEvent();
      });
    this.notesTitle.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((text: string) => {
        this.emitSendEvent();
      });
    this.notesText.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((text: string) => {
        this.emitSendEvent();
      });
    this.button.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged())
      .subscribe((text: string) => {
        this.emitSendEvent();
      });
  }

  setAlign(element: any, align: string) {
    element.align = align;
    this.emitSendEvent();
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

  showFileManager(media: ChallengeMedia): void {
    this.mediaToManipulate = media;
    this.appFileManager.showModal();
  }

  uploadSubmited($event) {
    if($event.data) {
      this.mediaToManipulate.image = $event.data.url;
    }else{
      this.mediaToManipulate.image = $event.url;     
    }
    this.mediaToManipulate = null;
    this.emitSendEvent();
  }

  getPhoto(media: ChallengeMedia, $event) {
    media.image = $event.path;
    this.emitSendEvent();
  }

  videoChanged(data: any) {
    const Sanitizer = new SanitizerHelper(this._sanitizer);
    if(data.type == 'youtube') {
      const videoID = Sanitizer.getYoutubeID(data.url);
      data.thumb = 'https://img.youtube.com/vi/' + Sanitizer.getYoutubeID(data.url) + '/hqdefault.jpg';
      data.link = Sanitizer.sanitizer(data.url, 'youtube');
    }
    if(data.type == 'vimeo') {
      data.thumb = 'https://i.vimeocdn.com/video/default_295x166.webp';
      this._vimeoService.getThumb(data.url).then(res => {
        data.thumb = res;
      });
      data.link = Sanitizer.sanitizer(data.url, 'vimeo');
    }

    this.emitSendEvent();
    
    $('iframe').attr('src', $('iframe').attr('src'));
  }

  insertMedia() {
    this.item.medias.push(new ChallengeMedia);
    this.emitSendEvent();
  }

  removeMedia(media: ChallengeMedia) {
    let index = this.item.medias.indexOf(media);
    this.item.medias.splice(index, 1);
  }

}
