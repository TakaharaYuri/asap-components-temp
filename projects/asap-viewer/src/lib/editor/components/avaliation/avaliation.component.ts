import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { TrailContentService } from '../../../@core/services/trail-content.service';

@Component({
  selector: 'app-editor-component-avaliation',
  templateUrl: 'avaliation.component.html',
  styleUrls: ['../../editor.component.css', './avaliation.component.css']
})

export class EditorAvaliationComponent implements OnInit {
  @Input() item: any;
  @Input() id: any;

  @Output() event = new EventEmitter<any>();

  public contentId: any;

  public form: any;

  public blocked = false;
  public listMarkerLetter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];

  constructor(
    public _trailContentService: TrailContentService
  ) {
    this.form = new FormArray([]);
  }

  ngOnInit() {
    this.generateForm();

    if (this.id) {
      this.getAnswer();
    }
  }

  generateForm() {
    this.item.questions.map((i: any) => {
      if (i.type === 'objetiva' || i.type === 'subjetiva') {
        this.form.push(new FormControl(''));
      }
      if (i.type === 'tabela') {
        console.log('EditorAvaliationComponent: generateForm: i', i);
        const arr = new FormArray([]);
        const lineLength = parseInt(i.lines, 10);
        for(let k = 0; k < lineLength; k++) {
          const array = new FormArray([]);
          i.collumns.map(col => {
            array.push(new FormControl(''));
          });
          arr.push(array);          
        }
        this.form.push(arr);
      }
    });
  }

  getAnswer() {
    this._trailContentService.all(this.id).subscribe(res => {
      console.log('EditorAvaliationComponent: getAnswer: res', res);
      const check = res.data.find((item: any) => {
        return item.value.type === 'avaliation';
      });
      console.log('EditorAvaliationComponent: getAnswer: check', check);
      if (check) {
        this.contentId = check.id;
        this.form.patchValue(check.value.value);
        this.blocked = true;
      }
    });
  }

  onSubmit() {

    const data = {
      type: 'avaliation',
      value: this.form.value
    };

    console.log('onSubmit: data', data);

    if (this.contentId) {
      this._trailContentService.update(this.contentId, data).subscribe(res => {
        console.log('onSubmit: _trailContentService.update: res', res);
      });
    } else {
      this._trailContentService.save(this.id, data).subscribe(res => {
        this.contentId = res.data.id;
        this.blocked = true;
        console.log('onSubmit: _trailContentService.save: res', res);
      });
    }
  }

  goNext() {
    this.event.emit();
  }

}
