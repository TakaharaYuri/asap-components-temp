import { Component, Input, Output, EventEmitter, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TrailContentService } from '../../../@core/services/trail-content.service';
import { VideoItemComponent } from '../../../@core/models/item-component';

declare var Vimeo: any;

@Component({
  selector: 'app-editor-component-video',
  templateUrl: 'video.component.html',
  styleUrls: ['../../editor.component.css', '../../../visualizador/curso-visualizador/curso-visualizador.component.scss'],
  providers: [TrailContentService]
})

export class VideoComponent implements OnInit, OnDestroy {

  @Input() item: VideoItemComponent;
  @Input() id: number;
  @Output() public event = new EventEmitter<any>();
  @Output() public videoTime = new EventEmitter<any>();
  @ViewChild('video',{static:false}) video: any;

  play = false;
  contentId: number;

  showSaved: boolean;
  notes: string;

  currentTime: number;
  seconds: number;

  // Youtube
  public player: any;
  public videoMonitor;
  public playerStatus;
  public idVideo;
  public timeLapse = 0;
  public timeLapseTimer;
  public disableNextButton = true;

  constructor(
    private sanitizer: DomSanitizer,
    public _trailContentService: TrailContentService
  ) {
    this.loadYT();
  }

  loadYT() {
    let YT:any;
    if (!window['YT']) {
      window['YT'] = {
          loading: 0,
          loaded: 0
      };
    }
    if (!window['YTConfig']) {
        var YTConfig = {
            'host': 'http://www.youtube.com'
        };
    }
    if (!window['YT'].loading) {
      window['YT'].loading = 1;
        var l = [];
        window['YT'].ready = function(f) {
            if (window['YT'].loaded) {
                f();
            } else {
                l.push(f);
            }
        };
        window["onYTReady"] = function() {
          window['YT'].loaded = 1;
            for (var i = 0; i < l.length; i++) {
                try {
                    l[i]();
                } catch (e) {}
            }
        };
        window['YT'].setConfig = function(c) {
            for (var k in c) {
                if (c.hasOwnProperty(k)) {
                    YTConfig[k] = c[k];
                }
            }
        };
        var a = document.createElement('script');
        a.type = 'text/javascript';
        a.id = 'www-widgetapi-script';
        a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflPBjLfx/www-widgetapi.js';
        a.async = true;
        var c = document.currentScript;
        if (c) {
            var n = c.nonce || c.getAttribute('nonce');
            if (n) {
                a.setAttribute('nonce', n);
            }
        }
        var b = document.getElementsByTagName('script')[0];
        b.parentNode.insertBefore(a, b);
    }
    return window['YT'];
  }

  ngOnInit() {
    console.log('ITEM ->', this.item);

    if (this.id) {
      this._trailContentService.all(this.id).subscribe(res => {
        console.log('RESPONSE', res);
        const check = res.data.find((item: any) => {
          return item.value.type === 'video';
        });
        if (check) {
          this.contentId = check.id;
          this.notes = check.value.notes;
        }
      });
    }

    if (this.item.source != 'stream') {
      this.videoMonitor = setInterval(() => {
        this.idVideo = this.getYoutubeID(this.item.link);
        let divPlayer = document.getElementById('video_player');


        if (divPlayer && !this.player) {

          clearInterval(this.videoMonitor);

          if (this.item.source === 'youtube') {
            this.disableNextButton = true;
            console.log('Type -> Youtube');
            let YT:any = window["YT"];
            this.player = new YT.Player('video_player', {
              videoId: this.idVideo,
              playerVars: {
                  'autoplay': 1,
                  'controls': (this.item.showVideoControl)? 1:0,
                  'autohide': 1,
                  'showinfo' : 0, // <- This part here
                  'wmode': 'opaque',
                  'rel': 0,
                  'loop': 1
              },
              events: {
                'onStateChange': (event) => {
                  if (event.data == 1) {
                    this.timeLapseTimer = setInterval(() => {
                      this.timeLapse = event.target.getCurrentTime();
                      let time = {
                        current: this.timeLapse,
                        duration: event.target.getDuration()
                      }
                      let videoProgress = (time.current/time.duration)*100;
                      this.disableNextButton = (videoProgress < 90);
                      this.videoTime.emit(time);
                      if (videoProgress >= 100) clearInterval(this.timeLapseTimer);
                    }, 1000)
                  }
                  if (event.data == 2) {
                    clearInterval(this.timeLapseTimer);
                  }
                }
              }
            });
          }
          else if(this.item.source === 'vimeo') {
            this.disableNextButton = true;
            console.log('Type -> Vimeo');
            this.idVideo = this.getVimeoID(this.item.source_path);

            this.player = new Vimeo.Player(divPlayer, {
              id: this.idVideo
            });

            this.player.on('timeupdate', ((event) => {
              let time = {
                current: event.seconds,
                duration: event.duration
              }
              let videoProgress = (time.current/time.duration)*100;
              console.log(time,videoProgress);
              this.disableNextButton = (videoProgress < 90);
              this.videoTime.emit(time);
              if (videoProgress >= 100) clearInterval(this.timeLapseTimer);
            }))
          }
        }
      }, 2000)
    }
  }

  ngOnDestroy() {
    if (this.timeLapseTimer) {
      clearInterval(this.timeLapseTimer);
    }
  }

  onSubmit() {
    const data = {
      'type': 'video',
      'notes': this.notes
    };
    this.showSaved = true;

    setInterval(() => {
      this.showSaved = false;
    }, 1000);

    if (this.contentId) {
      this._trailContentService.update(this.contentId, data).subscribe(res => {
        console.log(res);
      });
    }else{
      this._trailContentService.save(this.id, data).subscribe(res => {
        this.contentId = res.data.id;
        console.log(res);
      });
    }
  }

  goNext() {
    this.event.emit();
  }

  setCurrentTime(data) {
    console.log(data);
    let seconds = this.formatTime(data.target.currentTime)
    this.currentTime = data.target.currentTime;
    let time = {
      current: data.target.currentTime,
      duration: data.target.duration
    }
    let videoProgress = (time.current/time.duration)*100;
    this.disableNextButton = (videoProgress < 90);
    this.videoTime.emit(time);
    this.seconds = seconds;
  }

  formatTime(seconds) {
    let second: any = Math.floor(seconds % 60);
        second = (second >= 10) ? second : "0" + second;
    return seconds.toFixed(0);
  }

  getThumb(item) {
    let url = item.changingThisBreaksApplicationSecurity.split('/');
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://api.sr02.twcreativs.stream/videos/thumb/' + url[4]);
  }

  goPlay() {
    this.play = true;
    setTimeout(() => {
      this.video.nativeElement.play();
    }, 1000);
  }

  getYoutubeID(url) {
    if (url) {
      let urlText = url.changingThisBreaksApplicationSecurity;
      let urlResult = "";
      if (urlText.indexOf('embed') >= 0) {
        urlResult =  urlText.split('embed/')[1].split('?')[0];
      }
      else {
        urlResult = url.changingThisBreaksApplicationSecurity.split('=')[1];
      }
      return urlResult;
    }
  }

  getVimeoID(url) {
    let urlText = url;
    let urlResult = "";

    urlResult = urlText.split('/');
    console.log(urlResult[3]);
    return urlResult[3];
  }
}
