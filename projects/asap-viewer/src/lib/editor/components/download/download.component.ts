import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editor-component-download',
  templateUrl: 'download.component.html',
  styleUrls: ['../../editor.component.css', './download.component.css']
})

export class EditorDownloadComponent implements OnInit {

  @Input() item: any;
  @Output() event = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }

  goNext() {
    this.event.emit();
  }

}
