import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-editor-component-download-config',
  templateUrl: 'download-config.component.html',
  styleUrls: ['../../../editor.component.css', '../download.component.css']
})

export class EditorDownloadConfigComponent {

  @Input() item: any;
  @Output() public sendEvent = new EventEmitter<any>();

  setAlign(element: any, align: string) {
    element.align = align;
  }

  showFileManager(): void {
    // client.pick({
    //   fromSources: ["local_file_system"],
    //   uploadInBackground: false,
    //   maxFiles: 1,
    //   lang: "pt"
    // }).then(response => {
    //   this.item.file = response.filesUploaded["0"].url;
    // });
  }

  getArchive($event) {
    this.item.file = $event.path;
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

}
