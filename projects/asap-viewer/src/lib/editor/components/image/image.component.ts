import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ImageItemComponent } from '../../../@core/models/item-component';

@Component({
  selector: 'app-editor-component-image',
  templateUrl: 'image.component.html',
  styleUrls: ['../../editor.component.css']
})
export class ImageComponent {
    @Input() item: ImageItemComponent;
    @Output() event = new EventEmitter<any>();

    goNext() {
      this.event.emit();
    }
}
