import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';

import { FileManagerComponent } from '../../../../shared/file-manager/containers/file-manager/file-manager.component';
import { ImageItemComponent } from '../../../../@core/models/item-component';


declare var $: any;

@Component({
  selector: 'app-editor-component-image-config',
  templateUrl: 'image-config.component.html',
  styleUrls: ['../../../editor.component.css']
})
export class EditorImageConfigComponent {
  @Input() item: ImageItemComponent;
  @Output() public sendEvent = new EventEmitter<any>();
  @ViewChild(FileManagerComponent,{static:false}) appFileManager: FileManagerComponent;

  constructor(
    private _sanitizer: DomSanitizer
  ){

  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

  showFileManager(): void {
    this.appFileManager.showModal();
  }

  uploadSubmited($event) {
    this.item.url = ($event.data) ? $event.data.url : $event.url;    
  }

  getPhoto($event) {
    this.item.url = $event.path;
  }

}
