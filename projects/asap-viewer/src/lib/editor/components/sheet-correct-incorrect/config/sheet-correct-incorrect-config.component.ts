import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CorrectIncorrectComponentLines } from '../../../../@core/models/item-component';

@Component({
  selector: 'app-editor-sheet-correct-incorrect-config',
  templateUrl: 'sheet-correct-incorrect-config.component.html',
  styleUrls: ['../../../editor.component.css']
})

export class EditorSheetCorrectIncorrectConfigComponent {
  @Input() item;
  @Output() public sendEvent = new EventEmitter<any>();

  setAlign(element: any, align: string) {
    element.align = align;
  }

  insertLine() {
    if(!this.item.lines) {
      this.item.lines = [];
    }
    this.item.lines.push(new CorrectIncorrectComponentLines);
  }

  removeLine(index: number) {
    this.item.lines.splice(index, 1);
  }

  emitSendEvent() {
    this.sendEvent.emit();
  }

}
