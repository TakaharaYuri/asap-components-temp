import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TrailContentService } from '../../../@core/services/trail-content.service';
import { Global } from 'asap-crud';

@Component({
  selector: 'app-editor-component-sheet-correct-incorrect',
  templateUrl: 'sheet-correct-incorrect.component.html',
  styleUrls: ['../../editor.component.css',  './sheet-correct-incorrect.component.css'],
  providers: [ TrailContentService ]
})

export class EditorSheetCorrectIncorrectComponent implements OnInit {
  @Input() item;
  @Input() id: number;
  @Output() public event = new EventEmitter<any>();
  public view;
  

  contentId: number;

  public sended: boolean;

  constructor(
    public global: Global,
    public _trailContentService: TrailContentService
  ) {

  }

  ngOnInit() {
    if(this.id) {
      this._trailContentService.all(this.id).subscribe(res => {
        const check = res.data.find(item => {
          return item.value.type === 'correct-incorrect';
        })
        if(check) {
          this.sended = true;
          let i = 0;
          for(let line of this.item.lines) {
            line.selected = check.value.data[i];
            i++;
          }
        }
      });
    }

    for(let i of this.item.lines) {
      i.selected = false;
    }
  }

  onSubmit() {
    const filter = this.item.lines.filter(item => {
      return item.selected === false;
    });

    if(filter.length > 0) {
      this.global.alert("Ops...", "Responda a todas as questões", "error");
    }else{

      let data = [];

      for(let i of this.item.lines) {
        data.push(i.selected);
      }

      this.sended = true;

      this._trailContentService.save(this.id, {type: 'correct-incorrect', data: data}).subscribe(res => {
        this.contentId = res.data.id;
        console.log(res);
      });
    }
  }

  goNext() {
    if (this.sended) {
      this.event.emit();
    }
  }

}
