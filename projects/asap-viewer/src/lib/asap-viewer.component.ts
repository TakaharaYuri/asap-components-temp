import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'asap-viewer',
  template: `
    <router-outlet></router-outlet>
  `,
  // styles: [ './asap-viewer.css' ],
  encapsulation: ViewEncapsulation.None
})
export class AsapViewerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
