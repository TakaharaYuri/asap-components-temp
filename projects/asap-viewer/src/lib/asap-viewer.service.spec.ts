import { TestBed } from '@angular/core/testing';

import { AsapViewerService } from './asap-viewer.service';

describe('AsapViewerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapViewerService = TestBed.get(AsapViewerService);
    expect(service).toBeTruthy();
  });
});
