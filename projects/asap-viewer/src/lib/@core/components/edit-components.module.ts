import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EditComponentsComponent } from './edit-components.components';
import { UploadModule } from '../../shared/upload/upload.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UploadModule
  ],
  declarations: [
    EditComponentsComponent
  ],
  exports: [
    EditComponentsComponent
  ],
  entryComponents: [ ]
})
export class EditComponentsModule { }
