import { Component, Input } from '@angular/core';

declare var $:any;

@Component({
    selector: 'app-edit-components',
    templateUrl: './edit-components.component.html',
    styleUrls: ['./edit-components.component.css'],
    providers: [ ]
})
export class EditComponentsComponent  {

    @Input() component;

    constructor(    
    ) {

    }

    getArchive(event) {
        this.component.orientation = event.path;
    }

    editDocument() {
        alert('A');
    }


}