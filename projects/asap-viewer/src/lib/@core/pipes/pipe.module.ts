import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SafeUrlPipe } from './safe-url.pipe';
import { StreamPipe } from './stream.pipe';
import { AsapClientPipe } from './asap-client.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    SafeUrlPipe,
    StreamPipe,
    AsapClientPipe
  ],
  exports: [
    SafeUrlPipe,
    StreamPipe,
    AsapClientPipe
  ]
})

export class PipesModule { }
