import { Observable } from 'rxjs';

export interface ModelsInterface {
  populate(data: any): any;

  findAll(): Observable<any>;

  findOneBy(data: any): Observable<any>;

  findBy(id: number): Observable<any>;

  filterBy(data: any[]): Observable<any>;

  persist(data: any): Observable<any>;

  delete(id: number): Observable<any>;

}
