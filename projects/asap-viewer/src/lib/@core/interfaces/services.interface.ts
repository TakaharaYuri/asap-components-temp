import { Observable } from 'rxjs';

export interface ServicesInterface {

  findAll(): Observable<any>;

  findOneBy(data: any): Observable<any>;

  findBy(id: number): Promise<any>;

  filterBy(data: any[]): Observable<any>;

  persist(data: any): Observable<any>;

  delete(id: number): Observable<any>;

}
