import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';

@Injectable()
export class loginSocial {

  constructor(public http: HttpClient, public global: Global) {

  }

  save(person: any): Observable<any> {
    console.log("service", person);
    return this.http.post(this.global.apiURL() + '/api/v2/client/cad', person);
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
