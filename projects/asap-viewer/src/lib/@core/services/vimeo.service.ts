import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class VimeoService {

  constructor(public http: HttpClient) {

  }

  getThumb(vimeoUrl: string): Promise<string> {
    const url = 'https://vimeo.com/api/oembed.json?url=' + vimeoUrl;
    return new Promise<string>((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        resolve(res['thumbnail_url_with_play_button']);
      });
    });
  }


}
