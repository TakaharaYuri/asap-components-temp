import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NoticeItem } from '../models/notice-item.model';
import { Global } from 'asap-crud';

@Injectable()
export class NoticeItemService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(id: number): Promise<Array<NoticeItem>> {
    let arrayModel: NoticeItem[] = [];

    return new Promise<Array<NoticeItem>>((resolve, reject) => {
      this.fetchAll(id).subscribe(res => {
        for (const noticeItem of res.data) {
          arrayModel.push(new NoticeItem(
            noticeItem.id,
            noticeItem.title,
            noticeItem.description,
            noticeItem.image
          ));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<NoticeItem> {
    return new Promise<NoticeItem>((resolve, reject) => {
      this.find(id).subscribe(res => {
        const notice = new NoticeItem(
          res.data.id,
          res.data.title,
          res.data.description,
          res.data.image
        );
        notice.notice_id = res.data.notice.id;
        resolve(notice);
      }, err => {
        reject(err);
      })
    })
  }


  fetchAll(id): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/folder/' + id + '/items', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/item/' + id, this.getHeaders());
  }

  persist(notice: NoticeItem): Observable<any> {
    if (notice.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/notice/item/' + notice.id, notice, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/notice/item', notice, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/notice/item/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
