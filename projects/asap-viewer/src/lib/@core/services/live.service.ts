import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/Observable/of';

@Injectable()
export class LiveService {
  public streamVideoPath: string;

  constructor(public http: HttpClient) {
    const lu = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.streamVideoPath = lu.data.link;
  }

  uploadFile(data: any): Observable<HttpEvent<{}>> {

    const formData: FormData = new FormData();
    formData.append('file', data);

    const headerOptions = {
      'Authorization': sessionStorage.getItem('token'),
      'Accept': 'application/json'
    };

    const req = new HttpRequest('POST', this.streamVideoPath + 'videos/upload', formData, {
      reportProgress: true,
      responseType: 'json',
      headers: new HttpHeaders(headerOptions)
    });

    return this.http.request(req);
  }

}
