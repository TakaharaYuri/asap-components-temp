import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NoticeSubCategory } from '../models/notice-subcategory.model';
import { Global } from 'asap-crud';

@Injectable()
export class NoticeSubCategoryService {

  constructor(public http: HttpClient, public global: Global) {

  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/notice/answer/' + id, this.getHeaders());
  }

  persist(noticeSubCategory: NoticeSubCategory): Observable<any> {
    if (noticeSubCategory.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/notice/sub-category/' + noticeSubCategory.id, noticeSubCategory, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/notice/sub-category', noticeSubCategory, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/notice/sub-category/' + id, this.getHeaders());
  }

  answer(id:number, data: any) {
    return this.http.post(this.global.apiURL() + '/api/v2/client/notice/answer/' + id, {answer: data}, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
