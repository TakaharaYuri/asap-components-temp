import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AgendaModel } from '../models/agenda.model';
import { Global } from 'asap-crud';

@Injectable()
export class AgendaService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<AgendaModel>> {
    let arrayModel: AgendaModel[] = [];

    return new Promise<Array<AgendaModel>>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const agenda of res.data) {
          arrayModel.push(this.hydrate(agenda));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id: number): Promise<AgendaModel> {
    return new Promise<AgendaModel>((resolve, reject) => {
      this.find(id).subscribe(res => {
        resolve(this.hydrate(res.data[0]));
      }, err => {
        reject(err);
      })
    });
  }

  hydrate(data: any): AgendaModel {
    return new AgendaModel().populate({
        id: data.id,
        title: data.titulo,
        description: data.descr,
        shared_groups: data.grupo_compartilha,
        shared_users: data.user_compartilha,
        color: data.cor,
        notification: (data.notifica === 'sim'),
      });
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/cria', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/cria/' + id, this.getHeaders());
  }

  groups(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/grupo', this.getHeaders());
  }

  users(): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/agenda/user', {"search": ""}, this.getHeaders());
  }

  persist(agenda: AgendaModel): Observable<any> {
    const data = {
      id: agenda.id,
      titulo: agenda.title,
      descr: agenda.description,
      grupo_compartilha: agenda.shared_groups,
      user_compartilha: agenda.shared_users,
      cor: agenda.color,
      notifica: (agenda.notification) ? 'sim' : 'nao'
    }

    if(agenda.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/agenda/cria/' + agenda.id, data, this.getHeaders());  
    }

    return this.http.post(this.global.apiURL() + '/api/v2/agenda/cria', data, this.getHeaders());  
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/agenda/cria/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
