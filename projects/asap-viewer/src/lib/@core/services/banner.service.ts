import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { bannerModel } from '../models/banner.model';
import { Global } from 'asap-crud';

@Injectable()
export class bannerService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(id: number): Promise<Array<bannerModel>> {
    let arrayModel: bannerModel[] = [];

    return new Promise<Array<bannerModel>>((resolve, reject) => {
      this.fetchAll(id).subscribe(res => {
        for (const monitoraItem of res.data) {
          arrayModel.push(this.populate(monitoraItem));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  populate(data: any): bannerModel {
    return new bannerModel(data.id, data.name, data.value, data.assessment_id, data.trilha_id);
  }

  fetchAll(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/folder/' + id, this.getHeaders());
  }

  save(banner: any, id: number): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/banner/folder/' + id, banner, this.getHeaders());
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/client/banner/folder/' + id, this.getHeaders());
  }
  
  folderAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/folder', this.getHeaders());
  }

  folderDelete(params: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/client/banner/folder/delete/' +params, this.getHeaders());
  }

  folderPost(data: any): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/banner/folder/post', data, this.getHeaders());
  }

  getItem(params: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/folder/' +params, this.getHeaders());
  }

  update(banner: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/client/banner/folder/item/' + banner.id, banner, this.getHeaders());
  }

  getTrails(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/trilha', this.getHeaders());
  }

  getAssessments(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/assessment', this.getHeaders());
  }

  trailView(id: number): Observable<any> {
    const data = {
      trilha_id: id
    }
    return this.http.post(this.global.apiURL() + '/api/v2/client/trilha/view', data, this.getHeaders());
  }

  verifyBanner(data: any): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/banner/verify?link=' + data, this.getHeaders());
  }

  verifyBannerTrilha(data: any): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/trilha/banner?etapaId=' + data, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
