import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { BaseService } from './precifica/base.service';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';
import { Http } from '@angular/http';

@Injectable()
export class AssessmentService extends BaseService {

  constructor(public httpClient: HttpClient, public http: Http, public global:Global) {
    super(http,'','');
  }

  all(): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/client/meu-desempenho';
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  fetch(id: number): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/client/meu-desempenho/' + id;
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  find(id: number): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/client/pasta-assessment/' + id;
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  getScore(data: any) {
    const url = this.global.apiURL() + '/api/v2/client/pasta-assessment/pergunta/post';
    return this.httpClient.post(url, data, {headers: this.getHeader()});
  }

  getReportIndicador(): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/report/indicador';
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  getReportTema(): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/report/tema';
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  getReportEixo(): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/report/eixo';
    return this.httpClient.get(url, {headers: this.getHeader()});
  }

  postConfirmAssessment(banner_id: number): Observable<any> {
    return this.httpClient.post(this.global.apiURL() + '/api/v2/client/banner/view/post',
      {banner_id: banner_id},
      {headers: this.getHeader()});
  }

  assessmentBannerViewed(assessment_id: number): Observable<any> {
    return this.httpClient.post(
      this.global.apiURL() + '/api/v2/client/meu-desempenho/view',
      {assessment_id: assessment_id},
      {headers: this.getHeader()}
    );
  }

  getHeader(): HttpHeaders {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return new HttpHeaders(headerOptions);
  }


}
