import {ServicesInterface} from '../interfaces/services.interface';
import { Observable } from 'rxjs';
import {Injectable} from '@angular/core';
import {User} from '../models/user';

@Injectable()
export class UserService implements ServicesInterface {

  private arrayModel: User[] = [];

  constructor(private _userModel: User) {
  }

  collectionUserTrail(): Promise<any> {
    const _this = this;
    this.arrayModel = [];
    return new Promise<any>((resolve, reject) => {
      _this.findAllByTrail().subscribe(res => {
        for (const user of res.data) {
          _this.arrayModel.push(_this._userModel.populate({
            id: user.user_info.user_id,
            name: user.user_info.user_name,
            email: user.user_info.user_email,
            picture: user.user_info.picture,
            role: user.user_info.user_role,
            date: user.created_at.date,
            value: user.value
          }));
        }
        resolve(_this.arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  findForms(): Observable<any> {
    return this._userModel.getForms();
  }

  findAll(): Observable<any> {
    return this._userModel.findAll();
  }

  findAllByTrail(): Observable<any> {
    return this._userModel.findAllByTrail();
  }

  findOneBy(data: any): Observable<any> {
    return this._userModel.findOneBy(data);
  }

  findBy(id: number): Promise<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return this._userModel.filterBy(data);
  }

  persist(user: User): Observable<any> {
    return this._userModel.persist(user);
  }

  delete(id: number): Observable<any> {
    return this._userModel.delete(id);
  }

  associateGroupUser(data: any, group_id: number): Observable<any> {
    return this._userModel.associateGroupUser(data, group_id);
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property of toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }

}
