import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AmbienteVirtualService {

  constructor(
      public http: HttpClient,
      public global: Global
  ) {
  }

  getEtapasByTrail(idCurso: number): Observable<any> {
    return this.getByUrl(this.global.apiURL() + '/api/v2/client/trilha/etapa/item/' + idCurso);
  }

  getBannerByTrail(idCurso: number): Observable<any> {
    return this.getByUrl(this.global.apiURL() + '/api/v2/client/banner/trilha/' + idCurso);
  }

  getByUrl(url: string): Observable<any> {
    return this.http.get(url, {headers: this.getHeader()});
  }

  registerNewProgress(subStepTrail: number): Observable<any> {
    return this.http.post(
      this.global.apiURL() + '/api/v2/client/progresso/sub/post?sub_id=' + subStepTrail,
      {
        sub_id: subStepTrail
      },
      {headers: this.getHeader()}
    );
  }

  getSubEtapa(subStepTrail: number): Observable<any> {
    console.log("getSubEtapa",this.global.apiURL() + '/api/v2/client/trilha/sub-etapa/etapa/' + subStepTrail);
    return this.http.get(
      this.global.apiURL() + '/api/v2/client/trilha/sub-etapa/etapa/' + subStepTrail,
      {headers: this.getHeader()}
    );
  }

  getContent(id: number): Observable<any> {
    const url = this.global.apiURL() + '/api/v2/client/trilha/conteudo/' + id;
    const header = {headers: this.getHeader()};
    return this.http.get(url, header);
  }

  getHeader(): any {
    const token = sessionStorage.getItem('token');
    if (token) {
      const headerOptions = {
        'Authorization': token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      };
      return new HttpHeaders(headerOptions);
    }
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property of toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
