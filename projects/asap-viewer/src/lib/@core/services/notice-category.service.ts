import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NoticeCategory } from '../models/notice-category.model';
import { NoticeSubCategory } from '../models/notice-subcategory.model';
import { Global } from 'asap-crud';

@Injectable()
export class NoticeCategoryService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(id: number): Promise<Array<NoticeCategory>> {
    let arrayModel: NoticeCategory[] = [];

    return new Promise<Array<NoticeCategory>>((resolve, reject) => {
      this.fetchAll(id).subscribe(res => {
        console.log(res);
        for (const noticeCategory of res.data) {
          arrayModel.push(this.createEntity(noticeCategory));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<NoticeCategory> {
    return new Promise<NoticeCategory>((resolve, reject) => {
      this.find(id).subscribe(res => {
        const notice = this.createEntity(res.data)
        resolve(notice);
      }, err => {
        reject(err);
      })
    })
  }

  createEntity(data: any): NoticeCategory {
    let noticeCategory = new NoticeCategory(
      data.id,
      data.title,
      data.item.id
    );

    noticeCategory.sub_categories = [];

    for(let subs of data.SubCategoria.data) {
      noticeCategory.sub_categories.push(new NoticeSubCategory(subs.id, subs.title, data.id, subs.content));
    }

    return noticeCategory;
  }

  fetchAll(id): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/category/' + id, this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/category/' + id + '/edit', this.getHeaders());
  }

  persist(notice: NoticeCategory): Observable<any> {
    if (notice.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/notice/category/' + notice.id, notice, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/notice/category', notice, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/notice/category/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
