import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from '../base.service';

@Injectable()
export class ListaService extends BaseService {

  constructor(protected http: Http) { 
    super(http, 'api/v2/precifica', 'lista') 
  }

}
