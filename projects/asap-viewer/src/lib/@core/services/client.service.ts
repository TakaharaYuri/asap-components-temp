import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';

@Injectable()
export class ClientService {

  constructor(public http: HttpClient, public global: Global) {

  }

  getColor(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/color', this.getHeaders());
  }

  putColor(config: any): Observable<any> {
    const body = {
        config: config
    };
    return this.http.put(this.global.apiURL() + '/api/v2/admin/color', body, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
