import { Injectable } from '@angular/core';
import { ServicesInterface } from '../interfaces/services.interface';
import { Observable } from 'rxjs';

import { QuestionA, QuestionB, OptionA, OptionB, IndicatorA, IndicatorB, AssessmentForm } from '../models/assessment-form';

@Injectable()
export class AssessmentFormService implements ServicesInterface {

  constructor(protected _assessmentFormModel: AssessmentForm) {
  }

  collection(id: number): Promise<AssessmentForm> {
    return new Promise<AssessmentForm>((resolve, reject) => {
      this.findByFolder(id).subscribe(res => {

        const data = res.data[0];
        
        const assessmentForm = this._assessmentFormModel.populate(data)

        resolve(assessmentForm);
      }, err => {
        reject(err);
      })
    })
  }

  findByFolder(id: number) {
    return this._assessmentFormModel.findByFolder(id);
  }

  findAll(): Observable<any> {
    return undefined;
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Promise<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(_assessmentForm: AssessmentForm): Observable<any> {
    return undefined;
  }

  save(id: number, data: any): Observable<any> {
    return this._assessmentFormModel.save(id, data);
  }

  delete(id: number): Observable<any> {
    return undefined;
  }

  setStatus(status: number, id: number): Observable<any> {
    return undefined;
  }

}
