import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AgendaInstitutionModel } from '../models/agenda-institution.model';
import { Global } from 'asap-crud';

@Injectable()
export class AgendaInstitutionService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<AgendaInstitutionModel>> {
    let arrayModel: AgendaInstitutionModel[] = [];

    return new Promise<Array<AgendaInstitutionModel>>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const agenda of res) {
          arrayModel.push(this.hydrate(agenda));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  hydrate(data: any): AgendaInstitutionModel {
    return new AgendaInstitutionModel().populate({
        id: data.id,
        name: data.name
    });
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/institution', this.getHeaders());
  }

  medias(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/institution/ind/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
