import { Injectable } from '@angular/core';
import { ServicesInterface } from '../interfaces/services.interface';
import { Observable } from 'rxjs';
import { AssessmentItem } from '../models/assessment-item';

@Injectable()
export class AssessmentItemService implements ServicesInterface {

  protected arrayModel: Array<AssessmentItem>;

  constructor(protected _assessmentItemModel: AssessmentItem) {
  }

  collection(id: number): Promise<AssessmentItem[]> {
    this.arrayModel = [];
    
    return new Promise<AssessmentItem[]>((resolve, reject) => {
      this.findByFolder(id).subscribe(res => {
        for (const assessmentItem of res.data) {
          this.arrayModel.push(this._assessmentItemModel.populate({
            id: assessmentItem.id,
            title: assessmentItem.title,
            description: assessmentItem.description,
            folder_id: assessmentItem.folder_id,
            status: assessmentItem.status,
            file: assessmentItem.file
          }));
        }
        resolve(this.arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  findByFolder(id: number) {
    return this._assessmentItemModel.findByFolder(id);
  }

  findAll(): Observable<any> {
    return this._assessmentItemModel.findAll();
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Promise<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(assessmentItem: AssessmentItem): Observable<any> {
    return this._assessmentItemModel.persist(assessmentItem);
  }

  delete(id: number): Observable<any> {
    return this._assessmentItemModel.delete(id);
  }

  setStatus(status: number, id: number): Observable<any> {
    return undefined;
  }

}
