import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';
import { CampaignModel } from '../models/campaign.model';

@Injectable()
export class CampaignService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<CampaignModel>> {
    let arrayModel: CampaignModel[];
    return new Promise<Array<CampaignModel>>((resolve, reject) => {
      arrayModel = [];
      this.fetchAll().subscribe(res => {
        for (const r of res.data) {
          arrayModel.push(this.hydrate(r));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  resource(type: string): Promise<CampaignModel> {
    return new Promise<CampaignModel>((resolve, reject) => {
      this.getResource(type).subscribe(res => {
        resolve(this.hydrate(res.data[0]));
      }, err => {
        reject(err);
      })
    })
  }

  entity(id: number): Promise<CampaignModel> {
    return new Promise<CampaignModel>((resolve, reject) => {
      this.find(id).subscribe(res => {
        resolve(this.hydrate(res.data));
      }, err => {
        reject(err);
      })
    })
  }

  hydrate(data: any): CampaignModel {
    return new CampaignModel().populate({
      id: data.id,
      name: data.name,
      description: data.description,
      start: data.start,
      theme: data.theme,
      content: data.content,
    });
  }

  getResource(type: string): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/campanha/fixa/' + type, this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/campanha/' + id, this.getHeaders());
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/campanha', this.getHeaders());
  }

  persist(campaign: CampaignModel): Observable<any> {
    if (campaign.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/campanha/' + campaign.id, campaign, this.getHeaders());
    }

    return this.http.post(this.global.apiURL() + '/api/v2/admin/campanha', campaign, this.getHeaders());
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/campanha/' + id, this.getHeaders());
  }


  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
