import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';
import { BalanceSheet } from '../models/balance-sheet';

@Injectable()
export class BalanceSheetService {

  private exerciseId: number = 2;

  constructor(public http: HttpClient, public global: Global) {
  }

  collection(): Promise<Array<BalanceSheet>> {
    const arrayModel: Array<BalanceSheet> = [];

    return new Promise<Array<BalanceSheet>>((resolve, reject) => {
      this.findAll().subscribe(res => {
        if(res.data[0] && res.data[0].value) {
          for (const bs of res.data[0].value) {
            arrayModel.push(new BalanceSheet().populate(bs));
          }
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  superCollection(): Promise<Array<BalanceSheet>[]> {
    const arrayModel: Array<BalanceSheet>[] = [];

    return new Promise<Array<BalanceSheet>[]>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const data of res.data) {
          let array = [];
          for (const bs of data.value) {
            array.push(new BalanceSheet().populate(bs));
          }
          arrayModel.push(array);
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  findAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/exercises/' + this.exerciseId + '/last', this.getHeaders());
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/exercises/' + this.exerciseId + '/all', this.getHeaders());
  }

  persist(data: any): Observable<any> {
    const post = {
      "exercises_id": this.exerciseId,
      "value": data
    };
    return this.http.post(this.global.apiURL() + '/api/v2/client/exercises', post, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
