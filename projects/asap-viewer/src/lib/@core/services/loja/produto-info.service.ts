import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base.service';

@Injectable()
export class ProdInfoService extends BaseService {

  constructor(protected http: Http) { 
    super(http, 'api/v2/loja', 'produto/info') 
  }

}
