import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class EmitterService {

    static commentEmit = new EventEmitter();
    static responseEmit = new EventEmitter();
    static responseQtdEmit = new EventEmitter();
}
