import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Global } from 'asap-crud';

@Injectable()
export class TrailContentService {

  constructor(public http: HttpClient, public global: Global) {

  }

  all(id: any): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/trilha/conteudo/multiple/' + id, this.getHeaders());
  }

  save(id: number, data: any): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/trilha/conteudo/multiple/' + id, {value: data}, this.getHeaders());
  }
  
  update(id: number, data: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/client/trilha/conteudo/multiple/' + id, {value: data}, this.getHeaders());
  }
  
  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/client/exercises/' + id, this.getHeaders());
  }

  videoTime(id,seconds) {
    return this.http.get(this.global.apiURL() + '/api/v2/client/trilha/conteudo/tempo/' + id + '/' + seconds,this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
