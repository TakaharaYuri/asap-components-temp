import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventModel } from '../models/event.model';
import { Global } from 'asap-crud';

@Injectable()
export class EventService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<EventModel>> {
    let arrayModel: EventModel[] = [];

    return new Promise<Array<EventModel>>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const eventModel of res) {
          arrayModel.push(this.hydrate(eventModel));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  agenda(id: number): Promise<Array<EventModel>> {
    let arrayModel: EventModel[] = [];

    return new Promise<Array<EventModel>>((resolve, reject) => {
      this.fetchByAgenda(id).subscribe(res => {
        for (const eventModel of res) {
          arrayModel.push(this.hydrate(eventModel));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  user(): Promise<Array<EventModel>> {
    let arrayModel: EventModel[] = [];

    return new Promise<Array<EventModel>>((resolve, reject) => {
      this.fetchByUser().subscribe(res => {
        for (const eventModel of res) {
          arrayModel.push(this.hydrate(eventModel));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id: number): Promise<EventModel> {
    return new Promise<EventModel>((resolve, reject) => {
      this.find(id).subscribe(res => {
        resolve(this.hydrate(res[0]));
      }, err => {
        reject(err);
      })
    });
  }

  hydrate(data: any): EventModel {
    return new EventModel().populate({
        id: data.id,
        agenda_id: data.agenda_id,
        title: data.titulo,
        description: data.descr,
        shared_groups: data.grupo_compartilha,
        shared_users: data.user_compartilha,
        color: data.cor,
        notification: (data.notifica === 'sim'),
        company: data.instituicao,
        photo: data.foto,
        location: data.local,
        type: data.tipo,
        phone: data.telefone,
        date: data.data,
        hour_start: data.hora_inicio,
        hour_end: data.hora_fim,
        status: (data.status === 'aceito') ? 'ACCEPTED' : 'WAIT',
      });
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/evento', this.getHeaders());
  }

  fetchByAgenda(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/evento/' + id, this.getHeaders());
  }

  fetchByUser(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/user', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/evento/individual/' + id, this.getHeaders());
  }

  persist(eventModel: EventModel): Observable<any> {

    const data = {
      id: eventModel.id,
      agenda_id: eventModel.agenda_id,
      titulo: eventModel.title,
      descr: eventModel.description,
      grupo_compartilha: eventModel.shared_groups,
      user_compartilha: eventModel.shared_users,
      cor: eventModel.color,
      notifica: (eventModel.notification) ? 'sim' : 'nao',
      instituicao: eventModel.company,
      foto: eventModel.photo,
      local: eventModel.location,
      tipo: eventModel.type,
      telefone: eventModel.phone,
      data: eventModel.date,
      hora_inicio: eventModel.hour_start,
      hora_fim: eventModel.hour_end
    }

    if(eventModel.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/agenda/evento/' + eventModel.id, data, this.getHeaders());  
    }

    return this.http.post(this.global.apiURL() + '/api/v2/agenda/evento', data, this.getHeaders());  
  }

  accept(id: number): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/agenda/user/aceite/' + id, {}, this.getHeaders());
  }

  media(id: number, files: string[]): Observable<any> {
    const body = {"arquivos": files};
    return this.http.post(this.global.apiURL() + '/api/v2/agenda/user/midia/' + id, body, this.getHeaders());
  }

  getMedia(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/agenda/midia', this.getHeaders());
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/agenda/evento/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
