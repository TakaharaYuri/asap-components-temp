import {Injectable} from '@angular/core';
import {ServicesInterface} from '../interfaces/services.interface';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {LibraryHelper} from '../helpers/library.helper';
import { Global } from 'asap-crud';

@Injectable()
export class ComunicacaoService implements ServicesInterface {

  constructor(private http: HttpClient, public global: Global) {
  }

  sendMailIndividual(data: any): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/admin/campanha/sendInd', data, LibraryHelper.getHeaders());
  }

  findAll(): Observable<any> {
    return undefined;
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Promise<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(data: any): Observable<any> {
    return undefined;
  }

  delete(id: number): Observable<any> {
    return undefined;
  }

}
