import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';

@Injectable()
export class filterService {

  constructor(public http: HttpClient, public global: Global) {

  }

  getPermission(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/shared/config?id=' + id, this.getHeaders());
  }

  fetchAll(params: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/filter/client?id=' + params, this.getHeaders());
  }

  findParams(params1: string, params2: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/filter/client/params?value=' + params1 +'&id=' +params2, this.getHeaders());
  }


  fetchEstados(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/filter/estado', this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
