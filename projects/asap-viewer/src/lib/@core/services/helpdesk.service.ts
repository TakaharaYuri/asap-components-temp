import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';

@Injectable()
export class HelpdeskService {

  constructor(public http: HttpClient, public global: Global) {

  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/help/abre/user', this.getHeaders());
  }

  closed(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/help/user/list', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/help/' + id, this.getHeaders());
  }

  persist(data): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/help', data, this.getHeaders());
  }

  feedback(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/help/resp/' + id, this.getHeaders());
  }

  postFeedback(id: number, message: string): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/help/resp/' + id, {mensage: message}, this.getHeaders());
  }

  status(id: number, status: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/client/help/concluir/' + id, {status: status}, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
