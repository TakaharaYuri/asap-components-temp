import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Global } from 'asap-crud';

@Injectable()
export class sharedPermisionService {

  constructor(public http: HttpClient, public global: Global) {

  }

  save(person: any, params: number): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/admin/shared/save?id=' +params, person , this.getHeaders());
  }

  verify(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/shared/verify', this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
