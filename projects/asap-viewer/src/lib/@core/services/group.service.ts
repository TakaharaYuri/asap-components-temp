import {ServicesInterface} from '../interfaces/services.interface';
import { Observable } from 'rxjs';
import {Group} from '../models/group';
import {Injectable} from '@angular/core';

@Injectable()
export class GroupService implements ServicesInterface {

  private arrayModel: Group[] = [];

  constructor(private _groupModel: Group) {
  }

  collection(): Promise<any> {
    const _this = this;
    this.arrayModel = [];
    return new Promise<any>((resolve, reject) => {
      _this.findAll().subscribe(res => {
        for (const group of res.data) {
          _this.arrayModel.push(_this._groupModel.populate(group));
        }
        resolve(_this.arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  findAll(): Observable<any> {
    return this._groupModel.findAll();
  }

  findOneBy(data: any): Observable<any> {
    return this._groupModel.findOneBy(data);
  }

  findBy(id: number): Promise<any> {
    const _this = this;
    this.arrayModel = [];
    return new Promise((resolve, reject) => {
      _this._groupModel.findBy(id).subscribe(res => {
        for (const group of res.data) {
          _this.arrayModel.push(_this._groupModel.populate(group));
        }
        resolve(_this.arrayModel);
      }, err => {
        reject(err);
      })
    });
  }

  filterBy(data: any[]): Observable<any> {
    return this._groupModel.filterBy(data);
  }

  persist(group: Group): Observable<any> {
    return this._groupModel.persist(group);
  }

  delete(id: number): Observable<any> {
    return this._groupModel.delete(id);
  }

}
