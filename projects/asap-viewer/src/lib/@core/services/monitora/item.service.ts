import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MonitoraItem } from '../../models/monitora-item.model';
import { Global } from 'asap-crud';

@Injectable()
export class MonitoraItemService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(id: number): Promise<Array<MonitoraItem>> {
    let arrayModel: MonitoraItem[] = [];

    return new Promise<Array<MonitoraItem>>((resolve, reject) => {
      this.fetchAll(id).subscribe(res => {
        for (const monitoraItem of res.data) {
          arrayModel.push(this.populate(monitoraItem));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<MonitoraItem> {
    return new Promise<MonitoraItem>((resolve, reject) => {
      this.find(id).subscribe(res => {
        resolve(this.populate(res.data));
      }, err => {
        reject(err);
      })
    })
  }

  populate(data: any): MonitoraItem {
    const monitora = new MonitoraItem(
      data.id,
      data.title,
      data.description,
      data.image
    );
    monitora.monitorings_id = data.monitoring.id;
    return monitora;
  }

  fetchAll(id): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/folder/' + id + '/items', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/item/' + id, this.getHeaders());
  }

  persist(monitora: MonitoraItem): Observable<any> {
    if (monitora.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/monitoring/item/' + monitora.id, monitora, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/monitoring/item', monitora, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/monitoring/item/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
