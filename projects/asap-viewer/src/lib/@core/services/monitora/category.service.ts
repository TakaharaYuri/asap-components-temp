import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MonitoraCategory } from '../../models/monitora-category.model';
import { MonitoraSubCategory } from '../../models/monitora-subcategory.model';
import { Global } from 'asap-crud';

@Injectable()
export class MonitoraCategoryService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(id: number): Promise<Array<MonitoraCategory>> {
    let arrayModel: MonitoraCategory[] = [];

    return new Promise<Array<MonitoraCategory>>((resolve, reject) => {
      this.fetchAll(id).subscribe(res => {
        console.log(res);
        for (const monitoraCategory of res.data) {
          arrayModel.push(this.createEntity(monitoraCategory));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<MonitoraCategory> {
    return new Promise<MonitoraCategory>((resolve, reject) => {
      this.find(id).subscribe(res => {
        const monitora = this.createEntity(res.data)
        resolve(monitora);
      }, err => {
        reject(err);
      })
    })
  }

  createEntity(data: any): MonitoraCategory {
    let monitoraCategory = new MonitoraCategory(
      data.id,
      data.title,
      data.item.id
    );

    monitoraCategory.sub_categories = [];

    for(let subs of data.SubCategoria.data) {
      monitoraCategory.sub_categories.push(new MonitoraSubCategory(subs.id, subs.title, data.id, subs.content));
    }

    return monitoraCategory;
  }

  fetchAll(id): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/category/' + id, this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/category/' + id + '/edit', this.getHeaders());
  }

  persist(monitora: MonitoraCategory): Observable<any> {
    if (monitora.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/monitoring/category/' + monitora.id, monitora, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/monitoring/category', monitora, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/monitoring/category/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
