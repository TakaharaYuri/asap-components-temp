import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Monitora } from '../../models/monitora.model';
import { Global } from 'asap-crud';

@Injectable()
export class MonitoraService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<Monitora>> {
    let arrayModel: Monitora[] = [];

    return new Promise<Array<Monitora>>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const monitora of res.data) {
          arrayModel.push(new Monitora(
            monitora.id,
            monitora.title,
            monitora.created_at.date
          ));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<Monitora> {
    return new Promise<Monitora>((resolve, reject) => {
      this.find(id).subscribe(res => {
        const monitora = new Monitora(
          res.data.id,
          res.data.title,
          res.data.created_at.date
        );
        resolve(monitora);
      }, err => {
        reject(err);
      })
    })
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/folder', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/monitoring/folder/' + id, this.getHeaders());
  }

  persist(monitora: Monitora): Observable<any> {
    if (monitora.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/monitoring/folder/' + monitora.id, monitora, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/monitoring/folder', monitora, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/monitoring/folder/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
