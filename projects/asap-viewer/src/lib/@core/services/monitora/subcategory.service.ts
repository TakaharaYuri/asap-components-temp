import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MonitoraSubCategory } from '../../models/monitora-subcategory.model';
import { Global } from 'asap-crud';

@Injectable()
export class MonitoraSubCategoryService {

  constructor(public http: HttpClient, public global: Global) {

  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/monitoring/answer/' + id, this.getHeaders());
  }

  persist(monitoraSubCategory: MonitoraSubCategory): Observable<any> {
    if (monitoraSubCategory.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/monitoring/sub-category/' + monitoraSubCategory.id, monitoraSubCategory, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/monitoring/sub-category', monitoraSubCategory, this.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/monitoring/sub-category/' + id, this.getHeaders());
  }

  answer(id:number, data: any) {
    return this.http.post(this.global.apiURL() + '/api/v2/client/monitoring/answer/' + id, {answer: data}, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
