import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Notice, NoticeFieldValidation } from '../models/notice.model';
import { Global } from 'asap-crud';

@Injectable()
export class NoticeService {

  constructor(public http: HttpClient, public global: Global) {

  }

  collection(): Promise<Array<Notice>> {
    let arrayModel: Notice[] = [];

    return new Promise<Array<Notice>>((resolve, reject) => {
      this.fetchAll().subscribe(res => {
        for (const notice of res.data) {
          arrayModel.push(new Notice(
            notice.id,
            notice.title,
            notice.created_at.date
          ));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  entity(id): Promise<Notice> {
    return new Promise<Notice>((resolve, reject) => {
      this.find(id).subscribe(res => {
        const notice = new Notice(
          res.data.id,
          res.data.title,
          res.data.created_at.date
        );
        resolve(notice);
      }, err => {
        reject(err);
      })
    })
  }

  fetchAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/folder', this.getHeaders());
  }

  find(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/folder/' + id, this.getHeaders());
  }

  persist(notice: Notice): Observable<any> {
    if (notice.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/notice/folder/' + notice.id, notice, this.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/notice/folder', notice, this.getHeaders());
    }
  }

  save(data, id) {
    return this.http.post(this.global.apiURL() + '/api/v2/projetos/result/' + id, data, this.getHeaders());
  }

  update(data, id) {
    return this.http.put(this.global.apiURL() + '/api/v2/projetos/result/' + id, data, this.getHeaders());
  }

  projects(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/listabre', this.getHeaders());
  }

  getResultsAsAdmin(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/projetos/result', this.getHeaders());
  }

  project(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/listabre/' + id, this.getHeaders());
  }

  goals(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/objetivo/' + id, this.getHeaders());
  }

  communication(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/comunica/' + id, this.getHeaders());
  }

  getAllResult(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/result', this.getHeaders());
  }

  getResult(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/projetos/result/' + id, this.getHeaders());
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/notice/folder/' + id, this.getHeaders());
  }

  fetchValidationFieldsRequest(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/notice/validations/' + id, this.getHeaders());
  }

  fetchValidationFields(id: number): Promise<Array<NoticeFieldValidation>> {
    let arrayModel: Array<NoticeFieldValidation> = [];

    return new Promise<Array<NoticeFieldValidation>>((resolve, reject) => {
      this.fetchValidationFieldsRequest(id).subscribe(res => {
        for (const field of res.data) {
          arrayModel.push(new NoticeFieldValidation().populate(field));
        }
        resolve(arrayModel);
      }, err => {
        reject(err);
      })
    })
  }

  saveValidationField(id: number, field: NoticeFieldValidation): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/admin/notice/validations/' + id, field, this.getHeaders());
  }

  deleteValidationField(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/notice/validations/' + id, this.getHeaders());
  }

  saveValidation(id: number, data: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/admin/projetos/result/' + id, {result: data}, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
