import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Global } from 'asap-crud';

@Injectable()
export class ExercisesService {

  constructor(public http: HttpClient, public global: Global) {

  }

  last(id: any): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/exercises/' + id + '/last', this.getHeaders());
  }

  all(id: any): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/client/exercises/' + id + '/all', this.getHeaders());
  }

  save(data: any): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/client/exercises', data, this.getHeaders());
  }
  
  update(id: number, data: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/client/exercises/' + id, data, this.getHeaders());
  }
  
  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/client/exercises/' + id, this.getHeaders());
  }

  getHeaders(): {headers: HttpHeaders} {
    const token = sessionStorage.getItem('token');
    const headerOptions = {
      'Authorization': token,
      'Accept': 'application/json'
    };
    return {headers: new HttpHeaders(headerOptions)};
  }

}
