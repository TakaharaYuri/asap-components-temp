import {Injectable} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import "rxjs/add/operator/map";

@Injectable()
export class EstadoCidadeService {
  
  constructor(public http: Http) {}

  private host: string = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
  protected token = sessionStorage.getItem('token')
  protected headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json'
  })

  protected options = new RequestOptions({
    headers: this.headers
  })

  getState() {
    return this.http.get(this.host, this.options).map((res: Response) => res.json())
  }

  getCity(id) {
    return this.http.get(this.host+'/' + id + '/municipios', this.options).map((res: Response) => res.json())
  }
}
