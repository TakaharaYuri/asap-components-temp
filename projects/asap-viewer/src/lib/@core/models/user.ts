import {ModelsInterface} from '../interfaces/models.interface';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {LibraryHelper} from '../helpers/library.helper';
import {Injectable} from '@angular/core';
import { Global } from 'asap-crud';

@Injectable()
export class User implements ModelsInterface {

  public id: number;
  public name: string;
  public email: string;
  public picture: string;
  public role: string;
  public date: string;
  public value: string;
  public checked?: boolean;

  constructor(private http: HttpClient, public global: Global) {
  }

  populate(data: any): User {
    const user = new User(this.http,this.global);
    user.id = data.id;
    user.name = data.name;
    user.email = data.email;
    user.picture = data.picture;
    user.role = data.role;
    user.value = data.value;
    user.date = (data.created_at) ? data.created_at.date : data.date;
    return user;
  }

  public getForms(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/user/form', LibraryHelper.getHeaders());
  }

  findAll(): Observable<any> {
    return undefined;
  }

  findAllByTrail(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/trail/config/users', LibraryHelper.getHeaders());
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Observable<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(group: User): Observable<any> {
    return undefined;
  }

  delete(id: number): Observable<any> {
    return undefined;
  }

  associateGroupUser(data: User, group_id: number): Observable<any> {
    return this.http.post(this.global.apiURL() + '/api/v2/admin/trail/config/users',
      {
        user_id: data.id,
        grupo_id: group_id
      }, LibraryHelper.getHeaders());
  }
}
