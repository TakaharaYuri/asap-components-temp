import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModelsInterface } from '../interfaces/models.interface';
import { LibraryHelper } from '../helpers/library.helper';
import { Global } from 'asap-crud';

@Injectable()
export class AssessmentItem implements ModelsInterface {
  public id: number;
  public title: string;
  public descricao: string;
  public folder_id: number;
  public file: string;
  public status: number;
  public icone: string;

  constructor(private http: HttpClient, public global: Global) {
  }

  populate(data: any): AssessmentItem {
    const assessmentItem = new AssessmentItem(this.http,this.global);

    assessmentItem.id = data.id;
    assessmentItem.title = data.title;
    assessmentItem.descricao = data.descricao;
    assessmentItem.folder_id = data.folder_id;
    assessmentItem.file = data.file;
    assessmentItem.status = data.status;
    assessmentItem.icone = data.icone;



    return assessmentItem;
  }

  findAll(): Observable<any> {
    return undefined;
  }

  findByFolder(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/assessment/folder/' + id + '/items', LibraryHelper.getHeaders());
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Observable<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(assessmentItem: AssessmentItem): Observable<any> {
    const body = {
      titulo: assessmentItem.title,
      descricao: assessmentItem.descricao,
      arquivo: assessmentItem.file,
      pasta_id: assessmentItem.folder_id,
      icone: assessmentItem.icone
    };

    if (assessmentItem.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/assessment/item/' + assessmentItem.id + '/update', body, LibraryHelper.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/assessment/item/post', body, LibraryHelper.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/assessment/item/' + id + '/delete', LibraryHelper.getHeaders());
  }

}
