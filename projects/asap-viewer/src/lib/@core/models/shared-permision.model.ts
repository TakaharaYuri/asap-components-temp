export class sharedPermisionModel {

  public client_id: number;
  public resource: string;
  public resource_filter: string;
  public permission: string;

  constructor(
    client_id: number,
    resource: string,
    resource_filter: string,
    permission: string
  ) {
    this.client_id = client_id;
    this.resource = resource;
    this.resource_filter = resource_filter;
    this.permission = permission;
  }


}
