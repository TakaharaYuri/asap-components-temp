export class NoticeItem {
  public id: number;
  public title: string;
  public description: string;
  public image: string;
  public notice_id: number;

  constructor(
    id: number,
    title: string,
    description: string,
    image: string,
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.image = image;
  }
}
