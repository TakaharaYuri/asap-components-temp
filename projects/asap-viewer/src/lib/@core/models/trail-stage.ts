import { TrailSubStage } from './trail-substage';

export class TrailStage {
  public id: number;
  public title: string;
  public image: string;
  public substages: Array<TrailSubStage>;

  constructor(
      id: number,
      title: string,
      image: string
  ) {
    this.id = id;
    this.title = title;
    this.image = image;
  }

}
