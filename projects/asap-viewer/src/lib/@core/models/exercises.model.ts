export class ExercisesModel {
  public id: number;
  public value: string;

  populate(data: any) {
    this.id = data.id;
    this.value = data.value;
  }
}