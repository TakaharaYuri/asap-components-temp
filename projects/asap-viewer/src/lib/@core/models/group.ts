import {ModelsInterface} from '../interfaces/models.interface';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {LibraryHelper} from '../helpers/library.helper';
import {Injectable} from '@angular/core';
import { Global } from 'asap-crud';

@Injectable()
export class Group implements ModelsInterface {

  public id: number;
  public nome: string;
  public tipo: any;
  public date: string;

  constructor(private http: HttpClient, public global: Global) {
  }

  populate(data: any): Group {
    const group = new Group(this.http,this.global);
    group.id = data.id;
    group.nome = data.nome;
    group.tipo = data.tipo;
    group.date = (data.created_at) ? data.created_at.date : data.date;
    return group;
  }

  findAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/grupo', LibraryHelper.getHeaders());
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/grupo/' + id, LibraryHelper.getHeaders());
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(group: Group): Observable<any> {
    if (group.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/grupo/' + group.id + '/update', group, LibraryHelper.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/grupo/post', group, LibraryHelper.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/grupo/' + id, LibraryHelper.getHeaders());
  }

}
