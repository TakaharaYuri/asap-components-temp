export class AgendaModel {
  public id: number;
  public title: string;
  public description: string;
  public color: string;
  public notification: boolean;
  public shared_groups: Array<number>;
  public shared_users: Array<number>;

  populate(data: any): AgendaModel {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.color = data.color;
    this.notification = data.notification;
    this.shared_groups = data.shared_groups;
    this.shared_users = data.shared_users;
    return this;
  }

}