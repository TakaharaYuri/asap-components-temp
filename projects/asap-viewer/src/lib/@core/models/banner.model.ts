export class bannerModel {

  public id: number;
  public name: string;
  public assessment_id: number;
  public trilha_id: number;
  public value: BannerValue;

  constructor(
    id: number,
    name: string,
    value: any,
    assessment_id: number,
    trilha_id: number
  ) {
    this.id = id;
    this.name = name;
    this.value = value;
    this.assessment_id = assessment_id;
    this.trilha_id = trilha_id;
  }


}

export class BannerValue {

  public title: {text: string, align: string};
  public subtitle: {text: string, align: string};
  public description: {text: string, align: string};
  public button: {text: string, size: string, align: string};
  public text_color: string;
  public size: string;
  public background: {type: string, colorPrimary: string, colorSecondary: string, image: string};
  public target: string;

  constructor() {
    this.title = {text: 'Título aqui', align: 'center'};
    this.subtitle = {text: 'Sub-título aqui', align: 'center'};
    this.description = {text: 'Texto aqui', align: 'center'};
    this.button = {text: 'Botão', size: 'medium', align: 'center'};
    this.text_color = '#FFFFFF';
    this.size = 'medium';
    this.background = {type: 'color', colorPrimary: '#fc36fd', colorSecondary: '#5d3fda', image: ''};
    this.target = '';
  }

}