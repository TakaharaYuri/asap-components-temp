import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModelsInterface } from '../interfaces/models.interface';
import { LibraryHelper } from '../helpers/library.helper';
import { Global } from 'asap-crud';

@Injectable()
export class Assessment implements ModelsInterface {
  public id: number;
  public name: string;
  public date: string;

  constructor(private http: HttpClient, public global: Global) {
  }

  populate(data: any): Assessment {
    const trail = new Assessment(this.http,this.global);
    trail.id = data.id;
    trail.name = data.name;
    trail.date = data.date;
    return trail;
  }

  findAll(): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/assessment/folder', LibraryHelper.getHeaders());
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/assessment/folder/' + id, LibraryHelper.getHeaders());
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(assessment: Assessment): Observable<any> {
    const body = {
      nome_pasta: assessment.name
    };

    if (assessment.id) {
      return this.http.put(this.global.apiURL() + '/api/v2/admin/assessment/folder/' + assessment.id + '/update', body, LibraryHelper.getHeaders());
    } else {
      return this.http.post(this.global.apiURL() + '/api/v2/admin/assessment/folder/post', body, LibraryHelper.getHeaders());
    }
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.global.apiURL() + '/api/v2/admin/assessment/folder/' + id + '/delete', LibraryHelper.getHeaders());
  }

}
