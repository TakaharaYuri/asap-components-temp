import { SafeResourceUrl } from '@angular/platform-browser';

import { StringHelper } from '../helpers/string.helper';

export class ItemComponent {
  public type: string;
  public button: string;
  public active: boolean;
}

export class TitleItemComponent extends ItemComponent {
  public text: string;
  public size: string;
  public align: string;
  public color: string;
  public italic: boolean;
  public bold: boolean;
  public underline: boolean;

  public type = 'title';

  constructor(
    text: string,
    size: string,
    align: string,
    color: string,
    italic: boolean,
    bold: boolean,
    underline: boolean
  ) {
    super();

    this.text = text;
    this.size = size;
    this.align = align;
    this.color = color;
    this.italic = italic;
    this.bold = bold;
    this.underline = underline;

  }

}

export class VideoItemComponent extends ItemComponent {
  public source: string;
  public source_path: string;
  public title: string;
  public link: SafeResourceUrl;
  public mainButton: { color: string, background: string, text: string };
  public showVideoControl;
  public allowNext;
  public type = 'video';

  constructor(
    source: string,
    source_path: string,
    link: SafeResourceUrl,
    title: string,
    button: string
  ) {
    super();

    this.source = source;
    this.source_path = source_path;
    this.link = link;
    this.title = title;
    this.button = button;
    this.mainButton = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
  }


}

// Componente: Paragraph (Parágrafo)
export class TextItemComponent extends ItemComponent {
  public text: string;
  public textColor: string;
  public textSize: number;
  public textAnimation: string;
  public textAlign: string;
  public background: string;
  public buttonBgColor: string;
  public buttonTxtColor: string;
  public borderWidth: string;
  public borderStyle: string;
  public borderRadius: string;


  public type = 'text';

  constructor(
    text: string,
    button: string
  ) {
    super();

    this.text = text;
    this.button = button;
    this.textColor = '#333333';
    this.textSize = 14;
    this.textAnimation = '';
    this.textAlign = 'left';
    this.background = '';
    this.buttonBgColor = '#595959';
    this.buttonTxtColor = '#FFFFFF';
    this.borderWidth = '3px';
    this.borderStyle = 'solid';
    this.borderRadius  = '0px';
  }
}

export class ImageItemComponent extends ItemComponent {
  public url: string;

  public type = 'image';
  public title: string;
  public titleSize: number;
  public mainButton: { color: string, background: string, text: string };


  constructor(
    url: string,
    button: string
  ) {
    super();

    this.title = 'Título Aqui';
    this.button = button;
    this.url = url;
    this.titleSize = 16;
    this.mainButton = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
  }

}

export class PdfItemComponent extends ItemComponent {
  public url: string;
  public title: string;
  public link: SafeResourceUrl;

  public type = 'pdf';

  constructor(
    url: string,
    link: SafeResourceUrl,
    title: string,
    button: string
  ) {
    super();

    this.url = url;
    this.link = link;
    this.title = title;
    this.button = button;
  }

}

export class ExerciseComponent extends ItemComponent {
  public exercise: number;
  public type = 'exercise';

  constructor(
    exercise: number
  ) {
    super();

    this.exercise = exercise;
  }

}

export class MultiChoiceComponent extends ItemComponent {
  public type = 'multi-choice';
  public title: string;
  public media: string;
  public url: string;
  public feedback: string;
  public feedback_text: string;
  public options: Array<MultiChoiceOptions>;
  public link: SafeResourceUrl;
  public answersAlign: string;
  public marketType: string;
  public verification: string;
  public color: string;

  public constructor() {
    super();
    this.feedback = 'GENERAL';
    this.media = 'NONE';
    this.options = [];
  }
}

export class MultiChoiceOptions {
  public label: string;
  public correct: boolean;
  public selected: boolean;
  public value: string;
  public messageInError: string;

  public constructor() {
    this.label = '';
    this.correct = false;
    this.selected = false;
    this.value = StringHelper.generate(12);
    this.messageInError = '';
  }
}

export class PredefinedComponent extends ItemComponent {
  public type = 'predefined';

  public constructor() {
    super();
  }
}

export class ShortTextComponent extends ItemComponent {
  public type = 'short-text';
  public label: string;

  constructor() {
    super();
    this.label = 'Label';
  }
}

export class ActionAidFormComponent extends ItemComponent {
  public type;
  public form;
  public label: string;

  constructor() {
    super();
    this.type = 'action-aid-form';
    this.label = 'Label';
  }
}

export class IntroComponent extends ItemComponent {
  public title: { text: string, align: string, size: number, color: string, animation: string };
  public media: { type: string, text: string, align: string, size: number, color:string, animation: string, image: string, youtube: string, vimeo: string, title: string, description: string };
  public link: SafeResourceUrl;
  public button: string;
  public buttonOptions: { background: string };
  public settings: { background: string, color: string, size: number };

  public title_animation;
  public text_animation;
  public layout: string;

  public borderWidth: string;
  public borderStyle: string;
  public borderRadius: string;

  constructor() {
    super();
    this.type = 'intro';

    this.title = { text: '', align: 'center', size: 32, color: '#595959', animation: '' };
    this.media = { type: 'none', text: '', align: 'center', size: 12, color: '#595959', animation: '' , image: '', youtube: '', vimeo: '', title: '', description: '' };
    this.button = '';
    this.buttonOptions = { background: '' };
    this.settings = { background: '#f9f9f9', color: '', size: 14, };
    this.layout = '1';

    this.borderWidth = '3px';
    this.borderStyle = 'solid';
    this.borderRadius = '0px';
  }
}

export class ConclusionComponent extends ItemComponent {
  public bg: string;
  public title: { text: string, align: string, size?: string };
  public text: { text: string, align: string, size?: string };
  public button: string;
  public background: string;
  public textColor: string;
  public featureColor: string;
  public icon_animation;
  public title_animation;
  public text_animation;
  public button_animation;
  public feedback: string;

  constructor() {
    super();
    this.type = 'conclusion';

    this.title = { text: '', align: 'left', size: '32' };
    this.text = { text: '', align: 'left', size: '16' };
    this.textColor = '';
    this.background = '';
    this.featureColor = '';
    this.button = 'concluir';
    this.feedback = 'n';
  }
}

export class ChallengeComponent extends ItemComponent {
  public title: { text: string, align: string };
  public notes_title: { text: string, align: string };
  public notes_text: { text: string, align: string };
  public medias: Array<ChallengeMedia>;
  public button_bg;

  constructor() {
    super();
    this.type = 'challenge';
    this.title = { text: '', align: 'left' };
    this.notes_title = { text: '', align: 'left' };
    this.notes_text = { text: '', align: 'left' };
    this.medias = [];
    this.button_bg = ''
  }
}

export class ChallengeMedia {
  public type: string;
  public text: { text: string, align: string };
  public link: SafeResourceUrl;
  public url: string;
  public thumb: string;
  public image: string;
  public video: string;
  public title: string;
  public description: string;
  public featured: boolean;

  constructor() {
    this.featured = false;
    this.text = { text: '', align: 'left' };
  }
}

export class FormComponent extends ItemComponent {
  public title: string;
  public lines: Array<LineForm>;

  constructor() {
    super();
    this.type = 'form';
    this.lines = [];
  }
}

export class LineForm {
  public title: string;
  public showCols: boolean;
  public cols;
  public multiplier: string;
  public extraCols: Array<Array<ColOfLineForm>>;

  constructor() {
    this.title = '';
    this.showCols = true;
    this.cols = [];
    this.extraCols = [];
    this.multiplier = '1';
  }
}

export class ColOfLineForm {
  public placeholder: string;
  public format: string;
  public size: string;
  public type: string;
  public required: string;
  public minLength: number;
  public maxLength: number;
  public selectType: string;
  public multiple: string;

  constructor() {
    this.type = 'text';
    this.format = 'text';
    this.size = 'col-xs-12';
    this.required = '0';
  }
}

export class GroupComponent extends ItemComponent {
  public title: string;
  public group: GroupForm;

  constructor() {
    super();
    this.type = 'group';
    this.title = '';
    this.group = new GroupForm;
  }
}

export class GroupForm {
  public title: string;
  public type: string;
  public selectOptions: Array<any>;
  public subGroups: Array<GroupForm>;

  constructor() {
    this.title = '';
    this.type = 'text-short'
    this.selectOptions = [];
    this.subGroups = [];
  }
}

export class TableComponent extends ItemComponent {
  public title: string;
  public type: string;
  public userActive: boolean;
  public countLines: number;
  public collumns: Array<CollumnTable>;

  constructor() {
    super();
    this.type = 'table';
    this.title = 'Tabela';
    this.userActive = false;
    this.countLines = 1;
    this.collumns = [];
  }
}

export class CollumnTable {
  public type: string;
  public title: string;
  public align: string;
  public placeholder: string;
  public selectOptions: Array<any>;

  constructor() {
    this.title = '';
    this.placeholder = '';
    this.align = 'left';
    this.selectOptions = [];
  }
}

export class MultipleQuestionComponent extends ItemComponent {
  public questions: Array<MultipleQuestion>;

  constructor() {
    super();
    this.type = 'multiple-questions';
    this.questions = [];
  }
}

export class MultipleQuestion {
  public title: string;
  public options: Array<MultipleQuestionOption>;

  constructor() {
    this.title = '';
    this.options = [];
  }
}

export class MultipleQuestionOption {
  public image: string;
  public text: string;
  public correct: boolean;

  constructor() {
    this.image = '';
    this.text = '';
    this.correct = false;
  }
}

export class CorrectIncorrectComponent extends ItemComponent {
  public title: { text: string; align: string };
  public lines: Array<CorrectIncorrectComponentLines>;
  public feedback: string;
  public feedbackMessage: string;

  constructor() {
    super();
    this.type = 'correct-incorrect';
    this.title = { text: 'Título aqui', align: 'left' };
    this.feedback = 'GENERAL';
    this.feedbackMessage = 'Feedback aqui'
    this.lines = [];
  }
}

export class CorrectIncorrectComponentLines {
  public title: string;
  public feedback: string;
  public correct: boolean;

  constructor() {
    this.title = 'Título aqui';
    this.feedback = 'left';
    this.correct = false;
  }
}

export class UploadComponent extends ItemComponent {
  public title: { text: string; align: string, color: string };
  public titleAnimation: string;
  public titleSize: number;

  public text: { text: string; align: string, color: string };
  public textAnimation: string;
  public textSize: number;

  public background: string;

  public mainButton: { text: string, color: string, background: string };

  public file: any;
  public button_bg;

  //public titleColor;
  //public size;

  constructor() {
    super();
    this.type = 'upload';

    this.title = { text: 'Título aqui', align: 'center', color: '#333333' };
    this.titleAnimation = '';
    this.titleSize = 14;


    this.text = { text: 'Título aqui', align: 'center', color: '#333333' };
    this.textAnimation = '';
    this.textSize = 14;

    this.background = '#FFFFFF';

    this.mainButton = { text: 'Próximo', color: '#FFFFFF', background: '#333333' };
    this.file = [];

  }
}

export class StarPointComponent extends ItemComponent {
  public title: { text: string, align: string, size: string, animation: string, color: string };
  public text: { text: string, align: string, size: string, animation: string, color: string };
  public mainButton: { color: string, background: string, text: string };
  public background: string;
  public points: number;

  constructor() {
    super();
    this.type = 'star-point';

    this.title = { text: 'Título aqui', align: 'left', size: '32', animation: '', color: "#333333" };
    this.text = { text: 'Texto aqui', align: 'left', size: '16', animation: '', color: "#333333" };
    this.mainButton = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
    this.background = '#FFFFFF';
    this.points = 0;
  }
}

export class DownloadComponent extends ItemComponent {
  public layout: string;
  public title: { text: string, align: string, size?: string, animation: string, color: string };
  public text: { text: string, align: string, size?: string, animation: string, color: string };
  public background: string;
  public file: string;
  public icon: string;
  public iconColor: string;
  public iconSize: number;
  public downloadButton: { color: string, background: string, text: string };
  public mainButton: { color: string, background: string, text: string };

  constructor() {
    super();
    this.type = 'download';

    this.layout = '1';
    this.title = { text: 'Título aqui', align: 'left', size: '32', animation: '', color: '#333333' };
    this.text = { text: 'Texto aqui', align: 'left', size: '18', animation: '', color: '#333333' };
    this.icon = 'insert';
    this.iconColor = '#333333';
    this.iconSize = 13;
    this.downloadButton = { color: '#FFFFFF', background: '#333333', text: 'Baixar' };
    this.mainButton = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
    this.background = '#FFFFFF';
  }
}

export class RatingComponent extends ItemComponent {
  public title: { text: string, align: string, size?: string, animation: string };
  public mainButton: { color: string, background: string, text: string };
  public positive: string;
  public negative: string;
  public titleColor: string;
  public background: string;

  constructor() {
    super();
    this.type = 'rating';

    this.title = { text: 'Título aqui', align: 'left', size: '32', animation: '' };
    this.mainButton = { color: '#FFFFFF', background: '#333333', text: 'Próximo' };
    this.positive = 'Gostei';
    this.negative = 'Não gostei';
    this.titleColor = '#333333';
    this.background = '#FFFFFF';
  }
}
