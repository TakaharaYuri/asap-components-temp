export class MonitoraItem {
  public id: number;
  public title: string;
  public description: string;
  public image: string;
  public monitorings_id: number;

  constructor(
    id: number,
    title: string,
    description: string,
    image: string,
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.image = image;
  }
}
