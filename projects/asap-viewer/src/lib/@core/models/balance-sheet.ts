export class BalanceSheet {
  public id: string;
  public type: string;
  public description: string;
  public amount: number;
  public date: string;

  populate(data: any): BalanceSheet {
    this.id = data.id;
    this.type = data.type;
    this.description = data.description;
    this.amount = data.amount;
    this.date = data.date;
    return this;
  }

}