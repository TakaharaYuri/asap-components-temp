import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModelsInterface } from '../interfaces/models.interface';
import { LibraryHelper } from '../helpers/library.helper';
import { Global } from 'asap-crud';

@Injectable()
export class AssessmentForm implements ModelsInterface {
  public id: number;

  public indicatorA: Array<IndicatorA>;
  public indicatorB: Array<IndicatorB>;

  constructor(private http: HttpClient, public global: Global) {
  }

  populate(data: any): AssessmentForm {
    const assessmentForm = new AssessmentForm(this.http,this.global);

    assessmentForm.id = data.id;

    if(data.pergunta_tp_a && data.pergunta_tp_a.length > 0) {
      assessmentForm.indicatorA = data.pergunta_tp_a;
    }else{
      assessmentForm.indicatorA = [];
    }

    if(data.pergunta_tp_b && data.pergunta_tp_b.length > 0) {
      assessmentForm.indicatorB = data.pergunta_tp_b;
    }else{
      assessmentForm.indicatorB = [];
    }

    return assessmentForm;
  }

  findAll(): Observable<any> {
    return undefined;
  }

  findByFolder(id: number): Observable<any> {
    return this.http.get(this.global.apiURL() + '/api/v2/admin/assessment/item/pergunta/' + id, LibraryHelper.getHeaders());
  }

  save(id:number, data: any): Observable<any> {
    return this.http.put(this.global.apiURL() + '/api/v2/admin/assessment/item/pergunta/' + id, data, LibraryHelper.getHeaders());
  }

  findOneBy(data: any): Observable<any> {
    return undefined;
  }

  findBy(id: number): Observable<any> {
    return undefined;
  }

  filterBy(data: any[]): Observable<any> {
    return undefined;
  }

  persist(assessmentForm: AssessmentForm): Observable<any> {
    return undefined;
  }

  delete(id: number): Observable<any> {
    return undefined;
  }

}

export class IndicatorA {
  public nome: string;
  public ponderacao: number;
  public questoes: Array<QuestionA>;


  public constructor(nome: string, ponderacao: number) {
    this.nome = nome;
    this.ponderacao = ponderacao;
  }
}

export class IndicatorB {
  public nome: string;
  public ponderacao: number;
  public questoes: Array<QuestionB>;

  public constructor(nome: string, ponderacao: number) {
    this.nome = nome;
    this.ponderacao = ponderacao;
  }
}

export class Question {
  public nome: string;
  public ponderacao: string;
}

export class QuestionA extends Question {
  public opcoes: Array<OptionA>;

  constructor(nome: string, ponderacao: string, opcoes: Array<OptionA>) {
    super();
    this.nome = nome;
    this.ponderacao = ponderacao;
    this.opcoes = opcoes;

    return this;
  }
}

export class QuestionB extends Question {
  public textoUm: string;
  public textoDois: string;
  public opcoes: Array<OptionB>;

  constructor(nome: string, ponderacao: string, textoUm: string, textoDois: string, opcoes: Array<OptionB>) {
    super();
    this.nome = nome;
    this.ponderacao = ponderacao;
    this.opcoes = opcoes;
    this.textoUm = textoUm;
    this.textoDois = textoDois;

    return this;
  }
}

export class Option {
  public nome: string;
  public trilha_id: string;
}

export class OptionA extends Option {
  public valor: string;
  constructor(nome: string, valor: string, trilha_id: string) {
    super();
    this.nome = nome;
    this.valor = valor;
    this.trilha_id = trilha_id;

    return this;
  }
}

export class OptionB extends Option {
  public valorUm: string;
  public valorDois: string;
  public valueSelected: string;

  constructor(nome: string, valorUm: string, valorDois: string, trilha_id: string) {
    super();
    this.nome = nome;
    this.valorUm = valorUm;
    this.valorDois = valorDois;
    this.trilha_id = trilha_id;

    return this;
  }
}
