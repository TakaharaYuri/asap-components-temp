import { NoticeSubCategory } from './notice-subcategory.model';

export class NoticeCategory {
    public id: number;
    public title: string;
    public item_id: number;
    public sub_categories: Array<NoticeSubCategory>;

    constructor(id: number, title: string, item_id: number) {
        this.id = id;
        this.title = title;
        this.item_id = item_id;
    }
}
