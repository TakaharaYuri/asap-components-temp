import { MonitoraSubCategory } from './monitora-subcategory.model';

export class MonitoraCategory {
    public id: number;
    public title: string;
    public item_id: number;
    public sub_categories: Array<MonitoraSubCategory>;

    constructor(id: number, title: string, item_id: number) {
        this.id = id;
        this.title = title;
        this.item_id = item_id;
    }
}
