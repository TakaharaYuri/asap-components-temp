export class Notice {
  public id: number;
  public title: string;
  public date: string;

  constructor(id: number, title: string, date: string) {
    this.id = id;
    this.title = title;
    this.date = date;
  }
}

export class NoticeFieldValidation {
    public id: number;
    public type: string;

    populate(data: any): NoticeFieldValidation {
        this.id = data.id;
        this.type = data.type
        return this;
    }
}