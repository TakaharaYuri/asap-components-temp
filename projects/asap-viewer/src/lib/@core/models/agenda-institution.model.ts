export class AgendaInstitutionModel {
    public id: number;
    public name: string;

    public populate(data): AgendaInstitutionModel {
        this.id = data.id;
        this.name = data.name;
        return this;
    }
}