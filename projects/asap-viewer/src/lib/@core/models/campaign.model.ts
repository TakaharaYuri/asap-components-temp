export class CampaignModel {
  public id: number;
  public name: number;
  public description: string;
  public theme: string;
  public content: any;
  public start: string;

  populate(data: any): CampaignModel {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.theme = data.theme;
    this.content = data.content;
    this.start = data.start;
    return this;
  }

}