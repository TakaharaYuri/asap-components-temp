export class loginSocialModel {

  public name: string;
  public email: string;
  public facebook_id: string;
  public password: string;

  constructor(
    name: string,
    email: string,
    facebook_id: string,
    password: string
  ) {
    this.name = name;
    this.email = email;
    this.facebook_id = facebook_id;
    this.password = password;
  }


}
