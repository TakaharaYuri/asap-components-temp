import { ItemComponent } from './item-component';

export class MonitoraSubCategory {
    public id: number;
    public title: string;
    public category_id: number;
    public content: any;

    constructor(id: number, title: string, category_id: number, content: any) {
        this.id = id;
        this.title = title;
        this.category_id = category_id;
        this.content = content;
    }
}
