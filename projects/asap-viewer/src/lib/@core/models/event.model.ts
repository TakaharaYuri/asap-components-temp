export class EventModel {
  public id: number;
  public agenda_id: number;
  public company: string;
  public photo: string;
  public location: string;
  public type: string;
  public phone: string;
  public title: string;
  public description: string;
  public date: string;
  public hour_start: string;
  public hour_end: string;
  public color: string;
  public notification: string;
  public shared_groups: Array<number>;
  public shared_users: Array<number>;
  public status: string;

  populate(data: any): EventModel {
    this.id = data.id;
    this.agenda_id = data.agenda_id;
    this.company = data.company;
    this.photo = data.photo;
    this.location = data.location;
    this.type = data.type;
    this.phone = data.phone;
    this.title = data.title;
    this.description = data.description;
    this.date = data.date;
    this.hour_start = data.hour_start;
    this.hour_end = data.hour_end;
    this.color = data.color;
    this.notification = data.notification;
    this.shared_groups = data.shared_groups;
    this.shared_users = data.shared_users;
    this.status = data.status;
    return this;
  }

}