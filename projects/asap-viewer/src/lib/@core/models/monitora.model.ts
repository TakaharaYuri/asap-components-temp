export class Monitora {
  public id: number;
  public title: string;
  public date: string;

  constructor(id: number, title: string, date: string) {
    this.id = id;
    this.title = title;
    this.date = date;
  }
}
