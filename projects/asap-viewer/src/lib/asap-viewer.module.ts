import { NgModule } from '@angular/core';
import { AsapViewerComponent } from './asap-viewer.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatOptionModule, MatSelectModule, MatIconModule, MatRadioModule, MatInputModule, MatAutocompleteModule, MatDatepickerModule, MatTooltipModule } from '@angular/material';
import { NgxNouisliderModule } from 'ngx-nouislider';
import { TagInputModule } from 'ngx-chips';
import { AssessmentVisualizadorModule } from './visualizador/assessment-visualizador/assessment-visualizador.module';
import { BannerModule } from './banner/banner.module';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from './@core/pipes/pipe.module';
import { SpinnerModule } from './shared/spinner/spinner.module';
import { EditorComponentsModule } from './editor/components/editor.components.module';
import { CursoVisualizadorComponent } from './visualizador/curso-visualizador/curso-visualizador.component';
import { CursoNavbarComponent } from './visualizador/curso-visualizador/curso-navbar/curso-navbar.component';
import { CursoSidebarComponent } from './visualizador/curso-visualizador/curso-sidebar/curso-sidebar.component';
import { ViewerComponent } from './visualizador/curso-visualizador/viewer/viewer.component';
import { MiniCursoVisualizadorComponent } from './visualizador/mini-curso-visualizador/mini-curso-visualizador.component';
import { MiniCursoSidebarComponent } from './visualizador/mini-curso-visualizador/curso-sidebar/mini-curso-sidebar.component';
import { MiniCursoSideMenuComponent } from './visualizador/mini-curso-visualizador/mini-sidemenu/mini-sidemenu.component';
import { MiniCursoNavbarComponent } from './visualizador/mini-curso-visualizador/curso-navbar/mini-curso-navbar.component';
import { MiniViewerComponent } from './visualizador/mini-curso-visualizador/viewer/mini-viewer.component';
import { MiniModalTrilhaBanner } from './visualizador/mini-curso-visualizador/banner-modal/mini-banner-modal-component';
import { MiniSliderComponent } from './visualizador/mini-curso-visualizador/slider/slider.component';
import { ModalStarRatingComponent } from './visualizador/modal-rating/modal-rating.component';
import { StarRatingComponent } from './visualizador/modal-rating/star-rating/star-component';
import { modalTrilhaBanner } from './visualizador/curso-visualizador/banner-modal/banner-modal-component';
import { CommentsComponent } from './visualizador/mini-curso-visualizador/comment/comment.component';
import { VisualizarForumComponent } from './visualizador/mini-curso-visualizador/forum/question.component';
import { PointsComponent } from './shared/points/points.component';
import { AsapViewerRoutes } from './asap-viewer.routing';
import { ExercisesComponent } from './visualizador/exercicios/exercicios.component';
import { PrecificacaoVisualizadorComponent } from './visualizador/precificacao-visualizador/precificacao-visualizador.component';
import { PrecificacaoSidebarComponent } from './visualizador/precificacao-visualizador/precificacao-sidebar/precificacao-sidebar.component';
import { PrecificacaoNavbarComponent } from './visualizador/precificacao-visualizador/precificacao-navbar/precificacao-navbar.component';
import { ViewerPrecificacaoComponent } from './visualizador/precificacao-visualizador/viewer/viewer.component';
import { ProdutosComponent } from './visualizador/precificacao-visualizador/viewer/produtos/produtos.component';
import { NovoProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/novo/novo-produto.component';
import { VisualizarProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/visualizar-produto.component';
import { VisualizarResumoProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/resumo/visualizar-resumo-produto.component';
import { VisualizarMateriaisProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/materiais/visualizar-materiais-produto.component';
import { VisualizarMaoObraProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/mao-obra/visualizar-mao-obra-produto.component';
import { ModalMaoObraProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/mao-obra/modal/modal.mao-de-obra.component';
import { VisualizarFornecedorProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/fornecedor/visualizar-fornecedor-produto.component';
import { VisualizarCalcularProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/calcular/visualizar-calcular-produto.component';
import { ViabilidadeComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/viabilidade.component';
import { CustosFixosComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/custos-fixos/custos-fixos.component';
import { ModalCustosComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/custos-fixos/modal/modal.custos-fixos.component';
import { PedidosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/pedidos.component';
import { ModalResumoComponent } from './visualizador/precificacao-visualizador/viewer/modal-resumo/modal-resumo.component';
import { MeusPedidosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/meus-pedidos/meus-pedidos.component';
import { ModalEdit } from './visualizador/precificacao-visualizador/viewer/pedidos/calculo/modal/modal.calculo-pedido.component';
import { DadosPedidoComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/dados-pedido/dados-pedido.component';
import { CalculoPedidoComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/calculo/calculo-pedido.component';
import { CustosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/custos/custos.component';
import { ComprasComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/compras/compras.component';
import { CustosVariaveisComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/custos-variaveis/custos-variaveis.component';
import { ModalFaturamentoEstimadoComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/faturamento-estimado/modal/modal.faturamento-estimado.component';
import { FaturamentoEstimadoComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/faturamento-estimado/faturamento-estimado.component';
import { MaterialPipe } from './visualizador/precificacao-visualizador/pipes/material.pipe';
import { ExerciciosModule } from './visualizador/gestao-visualizador/gestao-visualizador.module';
import { AsapLimitToModule } from 'asap-limit-to';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AsapViewerRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatOptionModule,
        MatSelectModule,
        MatIconModule,
        MatRadioModule,
        MatInputModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatTooltipModule,
        NgxNouisliderModule,
        TagInputModule,
        ExerciciosModule,
        AssessmentVisualizadorModule,
        EditorComponentsModule,
        BannerModule,
        MomentModule,
        TranslateModule,
        PipesModule,
        SpinnerModule,
        AsapLimitToModule
    ],
    declarations: [
        AsapViewerComponent,
        CursoVisualizadorComponent,
        CursoNavbarComponent,
        CursoSidebarComponent,
        modalTrilhaBanner,
        ModalStarRatingComponent,
        ViewerComponent,
        MiniCursoVisualizadorComponent,
        MiniCursoSidebarComponent,
        MiniCursoSideMenuComponent,
        MiniCursoNavbarComponent,
        MiniViewerComponent,
        MiniModalTrilhaBanner,
        MiniSliderComponent,
        StarRatingComponent,
        PrecificacaoVisualizadorComponent,
        PrecificacaoSidebarComponent,
        PrecificacaoNavbarComponent,
        ViewerPrecificacaoComponent,
        ProdutosComponent,
        NovoProdutoComponent,
        VisualizarProdutoComponent,
        VisualizarResumoProdutoComponent,
        VisualizarMateriaisProdutoComponent,
        VisualizarMaoObraProdutoComponent,
        ModalMaoObraProdutoComponent,
        VisualizarFornecedorProdutoComponent,
        VisualizarCalcularProdutoComponent,
        ViabilidadeComponent,
        CustosFixosComponent,
        ModalCustosComponent,
        PedidosComponent,
        ModalResumoComponent,
        MeusPedidosComponent,
        ModalEdit,
        DadosPedidoComponent,
        CalculoPedidoComponent,
        CustosComponent,
        ComprasComponent,
        CustosVariaveisComponent,
        ModalFaturamentoEstimadoComponent,
        FaturamentoEstimadoComponent,
        MaterialPipe,
        CommentsComponent,
        VisualizarForumComponent,
        PointsComponent,
        ExercisesComponent,
        ModalStarRatingComponent
    ],
    exports: [
        AsapViewerComponent,
        CursoVisualizadorComponent,
        CursoNavbarComponent,
        CursoSidebarComponent,
        modalTrilhaBanner,
        ModalStarRatingComponent,
        ViewerComponent,
        MiniCursoVisualizadorComponent,
        MiniCursoSidebarComponent,
        MiniCursoSideMenuComponent,
        MiniCursoNavbarComponent,
        MiniViewerComponent,
        MiniModalTrilhaBanner,
        MiniSliderComponent,
        StarRatingComponent,
        PrecificacaoVisualizadorComponent,
        PrecificacaoSidebarComponent,
        PrecificacaoNavbarComponent,
        ViewerPrecificacaoComponent,
        ProdutosComponent,
        NovoProdutoComponent,
        VisualizarProdutoComponent,
        VisualizarResumoProdutoComponent,
        VisualizarMateriaisProdutoComponent,
        VisualizarMaoObraProdutoComponent,
        ModalMaoObraProdutoComponent,
        VisualizarFornecedorProdutoComponent,
        VisualizarCalcularProdutoComponent,
        ViabilidadeComponent,
        CustosFixosComponent,
        ModalCustosComponent,
        PedidosComponent,
        ModalResumoComponent,
        MeusPedidosComponent,
        ModalEdit,
        DadosPedidoComponent,
        CalculoPedidoComponent,
        CustosComponent,
        ComprasComponent,
        CustosVariaveisComponent,
        ModalFaturamentoEstimadoComponent,
        FaturamentoEstimadoComponent,
        MaterialPipe,
        CommentsComponent,
        VisualizarForumComponent,
        PointsComponent,
        ExercisesComponent,
        ModalStarRatingComponent
    ],
    entryComponents: [
        NovoProdutoComponent,
        ModalMaoObraProdutoComponent,
        ModalFaturamentoEstimadoComponent,
        ModalResumoComponent,
        ModalEdit,
        ModalCustosComponent,
        modalTrilhaBanner,
        MiniModalTrilhaBanner,
        ModalStarRatingComponent
    ]
})
export class AsapViewerModule { }
