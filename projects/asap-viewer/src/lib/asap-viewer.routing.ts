import { Routes } from '@angular/router';
import { AsapViewerComponent } from './asap-viewer.component';
import { CursoVisualizadorComponent } from './visualizador/curso-visualizador/curso-visualizador.component';
import { MiniCursoVisualizadorComponent } from './visualizador/mini-curso-visualizador/mini-curso-visualizador.component';
import { ExercisesComponent } from './visualizador/exercicios/exercicios.component';
import { PrecificacaoVisualizadorComponent } from './visualizador/precificacao-visualizador/precificacao-visualizador.component';
import { ProdutosComponent } from './visualizador/precificacao-visualizador/viewer/produtos/produtos.component';
import { NovoProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/novo/novo-produto.component';
import { VisualizarProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/visualizar-produto.component';
import { VisualizarResumoProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/resumo/visualizar-resumo-produto.component';
import { VisualizarMateriaisProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/materiais/visualizar-materiais-produto.component';
import { VisualizarMaoObraProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/mao-obra/visualizar-mao-obra-produto.component';
import { VisualizarFornecedorProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/fornecedor/visualizar-fornecedor-produto.component';
import { VisualizarCalcularProdutoComponent } from './visualizador/precificacao-visualizador/viewer/produtos/visualizar/calcular/visualizar-calcular-produto.component';
import { PedidosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/pedidos.component';
import { MeusPedidosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/meus-pedidos/meus-pedidos.component';
import { DadosPedidoComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/dados-pedido/dados-pedido.component';
import { CalculoPedidoComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/calculo/calculo-pedido.component';
import { CustosComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/custos/custos.component';
import { ComprasComponent } from './visualizador/precificacao-visualizador/viewer/pedidos/compras/compras.component';
import { ViabilidadeComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/viabilidade.component';
import { CustosFixosComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/custos-fixos/custos-fixos.component';
import { CustosVariaveisComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/custos-variaveis/custos-variaveis.component';
import { FaturamentoEstimadoComponent } from './visualizador/precificacao-visualizador/viewer/viabilidade/faturamento-estimado/faturamento-estimado.component';

export const AsapViewerRoutes: Routes = [
  {
    path: 'visualizador',
    component: AsapViewerComponent,
    children: [
      { path: 'etapa/:id', component: CursoVisualizadorComponent },
      { path: 'mini-etapa/:id', component: MiniCursoVisualizadorComponent },
      { path: 'etapa-develop/:id', component: MiniCursoVisualizadorComponent },
      { path: 'etapa-artigo/:id', component: MiniCursoVisualizadorComponent },
      { path: 'exercicios', component: ExercisesComponent },
      { 
        path: 'precificacao',
        component: PrecificacaoVisualizadorComponent,
        children: [
          { path: 'produtos', component: ProdutosComponent },
          { path: 'novo', component: NovoProdutoComponent },
          { path: 'meus-pedidos', component: MeusPedidosComponent },
          {
            path: 'visualizar/:id', component: VisualizarProdutoComponent, 
            children:
            [
              { path: '',  component: VisualizarResumoProdutoComponent },
              { path: 'materiais', component: VisualizarMateriaisProdutoComponent },
              { path: 'mao-de-obra', component: VisualizarMaoObraProdutoComponent },
              { path: 'fornecedor', component: VisualizarFornecedorProdutoComponent },
              { path: 'calcular', component: VisualizarCalcularProdutoComponent }
            ]
          },
          {
            path: 'pedidos', component: PedidosComponent, 
            children: [
              { path: 'dados-pedido', component: DadosPedidoComponent },
              { path: 'produtos', component: CalculoPedidoComponent },
              { path: 'custos', component: CustosComponent },
              { path: 'compras', component: ComprasComponent }
            ]
          },
          {
            path: 'viabilidade', component: ViabilidadeComponent, 
            children: [
              { path: 'custos-fixos', component: CustosFixosComponent },
              { path: 'custos-variaveis', component: CustosVariaveisComponent },
              { path: 'faturamento-estimado', component: FaturamentoEstimadoComponent }
            ]
          }

        ]
      }
    ]
  }
];
