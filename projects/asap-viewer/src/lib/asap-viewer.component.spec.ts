import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapViewerComponent } from './asap-viewer.component';

describe('AsapViewerComponent', () => {
  let component: AsapViewerComponent;
  let fixture: ComponentFixture<AsapViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
