/*
 * Public API Surface of asap-error-pages
 */

export * from './lib/asap-error-pages.service';
export * from './lib/asap-error-pages.component';
export * from './lib/asap-error-pages.module';
