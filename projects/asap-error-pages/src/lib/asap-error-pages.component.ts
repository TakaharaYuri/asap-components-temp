import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'asap-error-pages',
  templateUrl:'asap-error-pages.component.html',
  styles: []
})
export class AsapErrorPagesComponent implements OnInit {

  @Input() type:any = '404';
  @Input() msg:any =  'Não foi possível encontrar a informação que você procura';
  @Input() img:any = 'assets/img/logo.png';
  @Input() btnMsg:any = 'Voltar';
  @Input() redirect:any = '/';
  constructor() { }

  ngOnInit() {
  }

}
