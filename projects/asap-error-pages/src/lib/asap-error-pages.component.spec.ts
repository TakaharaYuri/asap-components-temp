import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapErrorPagesComponent } from './asap-error-pages.component';

describe('AsapErrorPagesComponent', () => {
  let component: AsapErrorPagesComponent;
  let fixture: ComponentFixture<AsapErrorPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapErrorPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapErrorPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
