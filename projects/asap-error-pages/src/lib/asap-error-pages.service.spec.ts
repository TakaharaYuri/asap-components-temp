import { TestBed } from '@angular/core/testing';

import { AsapErrorPagesService } from './asap-error-pages.service';

describe('AsapErrorPagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapErrorPagesService = TestBed.get(AsapErrorPagesService);
    expect(service).toBeTruthy();
  });
});
