import { NgModule } from '@angular/core';
import { AsapErrorPagesComponent } from './asap-error-pages.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AsapErrorPagesComponent],
  imports: [
    RouterModule
  ],
  exports: [AsapErrorPagesComponent]
})
export class AsapErrorPagesModule { }
