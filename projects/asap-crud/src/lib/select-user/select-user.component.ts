import { Component, Inject, OnChanges, SimpleChanges, Input, Output, EventEmitter } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { UserService } from './user.service';

@Component({
    selector: 'app-select-user',
    templateUrl: './select-user.component.html',
    styleUrls: ['./select-user.component.css'],
    providers: [ UserService ]
})
  
export class SelectUserComponent implements OnChanges  {

    @Input() userList;
    @Output() selectUser = new EventEmitter<any>();
    public component = false;

    public showFields = false;
    public fields;
    public field;
    public values;
    public value;

    public filter = "";
    public users;
    public multiselect = false;
    public multiselected = [];
    
    constructor(
        public userService: UserService,
        public dialogRef: MatDialogRef<SelectUserComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.userService.loadFields().subscribe((response) => {
            this.fields = response;
        });
        if (data.searchList) {
            this.users = data.searchList
        }
        else {
            this.users = [];
        }
        this.multiselect = data.multiselect;
        if (!this.multiselect) {
            this.multiselect = false;
        }
    }

    public searchUser() {
        this.users = null;
        this.userService.getAllUserData(this.filter,this.field,this.value).subscribe((response) => {
            this.users = response.data;
        });
    }

    ngOnChanges(changes:SimpleChanges) {
        if (changes.userList) {
            this.users = this.userList;
            this.component = true;
        }
    }

    public doFilter(event) {
        this.searchUser();
        if (event.preventDefault) {
            event.preventDefault();
        }
    }

    public getValues(event) {
        this.userService.loadValues(this.field).subscribe((response) => {
            this.values = response;
        });
    }

    public toggleFields() {
        this.showFields = !this.showFields;
        if (!this.showFields) {
            this.field = null;
            this.value = null;
            this.values = null;
        }
    }
    
    public onNoClick(): void {
        if (!this.component) {
            this.dialogRef.close();
        }
        else {
            this.users = [];
        }
    }

    public unselect(item) {
        let index = this.multiselected.indexOf(item);
        if (index >= 0) {
            this.multiselected.splice(index,1);
        }
    }

    public select(item) {
        if (this.multiselected == item) {
            if (!this.component) {
                this.dialogRef.close(item);
            }
            else {
                this.selectUser.emit(item);
            }
        }
        else if (this.multiselect) {
            this.multiselected.push(item);
        }
        else {
            if (!this.component) {
                this.dialogRef.close(item);
            }
            else {
                this.selectUser.emit(item);
            };
        }
    }

    public selectAll() {
        if (this.multiselected.length > 0) {
            this.multiselected = [];
        }
        else {
            this.multiselected = this.users.slice();
        }
    }

}