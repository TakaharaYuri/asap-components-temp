import { Injectable } from '@angular/core';
import { BaseService } from '../crud/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UserService extends BaseService {

    constructor(
        public http: HttpClient
    ) {
        super(http);
        this.basePath = "api/v2/admin";
        this.entityName = "asap-user";
    }

    public loadFields() {
        this.entityName = "folder/permission"
        let result = this.getResources();
        this.entityName = "asap-user"
        return result;
    }

    public loadValues(field) {
        let data = {
            filter: [field]
        };
        this.entityName = "folder/permission"
        let result = this.createResource(data);
        this.entityName = "asap-user"
        return result;
    }

    public getAllUserData(filter=null,field=null,value=null): Observable<any> {
        let query = "all";
        if ((filter) && (filter != "")) {
          query += '&search=' + filter;
        }
        if ((field) && (value)) {
            query += "&field=" + field;
            query += "&value=" + value;
        }
        return this.getResources({query: query});
    }

}
