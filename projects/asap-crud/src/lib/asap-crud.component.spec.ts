import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapCrudComponent } from './asap-crud.component';

describe('AsapCrudComponent', () => {
  let component: AsapCrudComponent;
  let fixture: ComponentFixture<AsapCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
