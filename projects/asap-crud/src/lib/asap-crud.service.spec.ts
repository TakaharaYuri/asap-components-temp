import { TestBed } from '@angular/core/testing';

import { AsapCrudService } from './asap-crud.service';

describe('AsapCrudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapCrudService = TestBed.get(AsapCrudService);
    expect(service).toBeTruthy();
  });
});
