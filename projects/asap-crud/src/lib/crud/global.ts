import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SelectDialogComponent } from '../select-dialog/select-dialog.component';
import { SelectUserComponent } from '../select-user/select-user.component';
import { Observable } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';

import Swal from 'sweetalert2';
declare var swal:any;

@Injectable()
export class Global {

    constructor(
        public dialog: MatDialog,
        public notification: NotificationsService,
    ) {

    }

    public alert(title, message?, type?) {
        let aSwal:any = Swal;
        if (aSwal.fire) {
            return aSwal.fire(title,message,type);     
        }
        else {
            return swal(title,message,type);
        }
    }

    public swal(options) {
        console.log("SWAL: ",swal, Swal);
        if (swal) {
            return swal(options);
        }
        let aSwal:any = Swal;
        if (aSwal.fire) {
            return aSwal.fire(options);     
        }
        aSwal(options);   
    }

    public apiURL() {
        if (localStorage.getItem('apiURL')) {
            return localStorage.getItem('apiURL');
        }
        else if (sessionStorage.getItem('apiURL')) {
            return sessionStorage.getItem('apiURL');
        }
        return "https://apiv3.plataformaasap.com.br";
    }

    public clientID() {
        if (localStorage.getItem('client')) {
            return localStorage.getItem('client');
        }
        return sessionStorage.getItem('client');
    }

    public loggedUser() {
        let user:any = sessionStorage.getItem("loggedUser");
        if (user) {
            user = JSON.parse(user).data;
            return user;
        }
        return null;
    }

    public searchData(searchParams): Observable<any> {
        let dialog = this.dialog.open(SelectDialogComponent,{
            width: "550px",
            data: searchParams
        });
        return dialog.afterClosed();
    }

    public searchUser(searchParams): Observable<any> {
        let dialog = this.dialog.open(SelectUserComponent,{
            width: "550px",
            data: searchParams
        });
        return dialog.afterClosed();
    }

}
