import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject, Component } from '@angular/core';

@Component({
    selector: 'asap-edit',
    templateUrl: 'asap-edit.component.html',
    styleUrls: ['./asap.component.css']
})
export class AsapEditComponent {

    public entity;
    public fields;
    public owner;
  
    constructor(
      public dialogRef: MatDialogRef<AsapEditComponent>,
      @Inject(MAT_DIALOG_DATA) public data:any
    ) {
        this.owner = data.owner;
        this.fields = this.owner.tableDef();
        this.entity = this.owner.entity;
    }
  
    public title() {
        if (this.entity[this.owner.idField]) {
            return "Editando #" + this.entity[this.owner.idField];
        }
        else {
            return "Adicionando..."
        }
    }

    public close(): void {
      this.dialogRef.close();
    }
  
}