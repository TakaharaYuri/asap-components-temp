import { AsapBaseService } from './asap-base.service';
import { Global } from './global';
import { AsapEditComponent } from './asap-edit.component';

declare var $:any;

export class AsapBaseCRUD {
    
    public editing = false;
    public saving = false;
    public editDialog;
    public editBootstrap = false;

    public tableMode;
    public idField = "id";
    public descriptionField = "description";
    public descriptionLabel = "Descrição";
    public imageField = "Descrição";

    public useNoty = false;
    
    public saveTitle = 'Salvar!';
    public saveMessage = 'Registro salvo com Sucesso!';
    public deleteTitle = 'Remover!';
    public deleteMessage = 'Registro removido com Sucesso!';
    

    public entity = null;
    public list = null;

    public editClass:any = AsapEditComponent;
    
    constructor(
        public global: Global,
        public service: AsapBaseService
    ) {

    }

    protected displayedColumns() {
        let defs = this.tableDef();
        let result = [];
        for (const def of defs) {
            result.push(def.fieldName);
        }
        result.push("Acao");
        return result;
    }

    protected tableDef() {
        let result = [
            {
                fieldName: this.descriptionField,
                fieldLabel: this.descriptionLabel
            }
        ];
        return result;
    }

    protected actionButtons() {
        let buttons = [
            {
                styleClass: 'btn btn-sm btn-primary',
                color: 'primary',
                content:'<i class="fa fa-pencil-square-o"></i>',
                action: (item) => { this.edit(item) }
            },
            {
                styleClass: 'btn btn-sm btn-danger',
                color: 'accent',
                content: '<i class="fa fa-times"></i>',
                action: (item) => { this.delete(item) }
            },
        ];
        return buttons;
    }

    public afterCreate() {
        this.listAll();
    }

    public defaults() {
        return {}
    }

    public listCondition() {
        return {};
    }

    public save(entity=null,afterSave=null) {
        if (!this.canSave()) {
            this.global.notification.warn(
                "Campos requeridos...",
                "Existem campos necessários que não foram preenchidos!"
            );
        }
        else if (!this.saving) {
            this.saving = true;
            setTimeout(() => {
                if (!entity) entity = this.entity;
                this.service.save(entity).subscribe(
                    (response: any) => { 
                        let success = true;
                        if (response.success != undefined) {
                            success = response.success;
                        }
                        if (success) {
                            this.editing = false;
                            this.global.notification.success(
                                this.saveTitle,
                                this.saveMessage
                            );
                            if (this.editBootstrap) {
                                setTimeout(() => {
                                    $(this.editClass).modal("hide");
                                },100);
                            }
                            else if (this.editDialog) {
                                this.editDialog.close();
                                this.editDialog = null;
                            }  
                            setTimeout(() => {
                                this.listAll();
                                this.saving = false;
                                
                                let anyThis:any = this;
                                if (afterSave) {
                                    afterSave();
                                }
                                else if (anyThis.afterSave) {
                                    anyThis.afterSave();
                                }
                            }); 
                        }
                        else {
                            this.global.notification.error('Erro!', "Ocorreu um erro ao Salvar!");
                            this.saving = false;
                        }                     
                    },
                    (error) => {
                        this.global.notification.error('Erro!', error.message);
                        this.saving = false;
                    }
                );
            },100);
        }
    }

    public openEdit() {
        if (this.editBootstrap) {
            if (this.editClass != "") {
                setTimeout(() => {
                    $(this.editClass).modal("show");
                },100);
            }
        }
        else {
            this.editDialog = this.global.dialog.open(this.editClass, {
                width: '500px',
                disableClose: true,
                data: {owner: this}
            });
        }
    }

    public add() {
        this.editing = true;
        this.entity = this.defaults();
        this.openEdit();
    }

    public edit(item) {
        this.editing = true;
        this.service.getEntity(item[this.idField]).subscribe((response:any) => {
            if (response.data[0]) {
                this.entity = response.data[0];
            }
            else {
                this.entity = response.data;
            }
            this.openEdit();
        });
    }

    public delete(item) {
        this.global.swal({
            title: "Remover?",
            text: "Você tem certeza que deseja remover este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Sim",
            cancelButtonText: "Não"
        }).then(
            confirm => {
                if (confirm) {
                    this.service.delete(item[this.idField]).subscribe(
                        (response:any) => {
                            if (this.useNoty) {
                                this.global.notification.info(this.deleteTitle, this.deleteMessage);
                            }
                            else {
                                this.global.notification.success(
                                    this.deleteTitle,
                                    this.deleteMessage
                                );
                            }
                            this.listAll();
                        },
                        (error) => {
                            if (this.useNoty) {
                                this.global.notification.error('Erro!', error.message);
                            }
                            else {
                                this.global.notification.error(
                                    "Erro!",
                                    error.message,
                                ); 
                            }
                        }
                    );              
                }
            },
            cancel => {}
        );     
    }

    public listAll() {
        this.service.listAll(this.listCondition()).subscribe((response:any) => {
            this.list = response.data;
        });
    }

    public canSave() {
        return ($('.ng-invalid').length <= 0);
    }

}

