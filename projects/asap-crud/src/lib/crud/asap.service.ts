import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AsapBaseService } from './asap-base.service';

@Injectable()
export class AsapService extends AsapBaseService {

    constructor(public http: HttpClient) {
        super(http);
    };
}