import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';

export class AsapBaseService extends BaseService {

    constructor(
        public http: HttpClient
    ) {
        super(http);
    };

    public getEntity(id): Observable<any> {
        return this.getResource(id);
    }

    public save(entity): Observable<any> {
        if (entity.id) {
            return this.updateResource(entity);
        }
        else {
            return this.createResource(entity);
        }
    }

    public delete(id): Observable<any> {
        return this.deleteResource(id);
    }

    public listAll(params): Observable<any> {
        return this.getResources(params);
    }
}