import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule, MatOptionModule, MatDialogModule } from '@angular/material';
import { AsapEditComponent } from './crud/asap-edit.component';
import { NgModule } from '@angular/core';
import { Global } from './crud/global';
import { AsapService } from './crud/asap.service';
import { SelectDialogComponent } from './select-dialog/select-dialog.component';
import { SelectUserComponent } from './select-user/select-user.component';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatOptionModule,
        ReactiveFormsModule,
        SimpleNotificationsModule.forRoot()
    ],
    declarations: [
        AsapEditComponent,
        SelectDialogComponent,
        SelectUserComponent
    ],
    providers: [
        Global,
        AsapService,
        NotificationsService
    ],
    exports: [
        AsapEditComponent,
        SelectDialogComponent,
        SelectUserComponent
    ],
    entryComponents: [
        AsapEditComponent,
        SelectDialogComponent,
        SelectUserComponent
    ]
})
export class AsapCrudModule { }
