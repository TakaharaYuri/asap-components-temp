import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { LiveService } from '../live.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseApp } from '@angular/fire';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

declare var $: any;
declare const swal: any;

@Component({
  selector: 'app-modal-content',
  templateUrl: 'modal-content.component.html',
  styleUrls: ['../asap-feed.component.css', './modal-content.component.css'],
  providers: [ Ng2ImgMaxService, LiveService ]
})
export class ModalContentComponent implements OnInit {

  public form: FormGroup;
  public loggedInUser: any;
  public uploadedFileType: string;
  public uploadedFileUrl: string;
  public loading = true;
  public isFile = false;
  public item: any;

  public uploading: boolean;

  public streamVideoPath: string;

  tmpFilesArray = [];

  constructor(
    public dialogRef: MatDialogRef<ModalContentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb: FormBuilder,
    public sanitizer: DomSanitizer,
    private angularFirestore: AngularFirestore,
    private ng2ImgMaxService: Ng2ImgMaxService,
    public firebase: FirebaseApp,
    public liveService: LiveService,
  ) {
    this.item = data.data();
    this.buildForm();
  }

  ngOnInit() {
    this.getUsuarioLogado();

    setTimeout(() => {
      this.loading = false;
      setTimeout(() => {
        const elemSidebar = <HTMLElement>document.querySelector('.perfect-scroll');
        //const ps = new PerfectScrollbar(elemSidebar, {wheelSpeed: 2, suppressScrollX: true});
      }, 1);
    }, 1000);
  }

  removeEmbed() {
    this.form.patchValue({
      'embed': '',
      'embedTipo': ''
    });
  }

  removeImage() {
    this.form.patchValue({
      'image': ''
    });
  }

  getUsuarioLogado(): any {
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.loggedInUser = loggedUser.data;
    this.streamVideoPath = this.loggedInUser.link;
  }

  buildForm(): void {
    this.form = this.fb.group({
      id: this.item.id,
      title: this.item.title,
      embed: this.item.embed,
      embedTipo: this.item.embedTipo,
      image: this.item.image
    });
    this.tmpFilesArray.push(... this.item.files);
  }

  onSubmit(): any {

    const formModel = this.form.value;

    const data = {
      title: formModel.title,
      embedTipo: formModel.embedTipo,
      embed: formModel.embed,
      image: formModel.image,
      files: [],
      video_rendering: this.item.video_rendering,
      user_info: {
        id: this.loggedInUser.id,
        name: this.loggedInUser.name,
        picture: this.loggedInUser.picture
      }
    };

    if (this.tmpFilesArray.length > 0) {
      this.tmpFilesArray.map((tmpFile: any) => {
        data.files.push({
          type: tmpFile.type,
          url: tmpFile.url
        });
      });
    }

    const embed = this.checkIfHasEmbededVideo(formModel.title);

    if (typeof embed === 'object') {
      data.embed = embed.url;
    }

    if (this.uploadedFileUrl && this.uploadedFileType === 'video') {
      data.embedTipo = 'file';
      data.embed = this.uploadedFileUrl;
    }

    if (this.uploadedFileUrl && this.uploadedFileType === 'image') {
      data.image = this.uploadedFileUrl;
    }

    if (
      data.title === '' &&
      data.embed === '' &&
      data.image === '' &&
      data.files.length === 0
    ) {
      swal('Ops...', 'Você precisa colocar um texto, imagem ou adicionar anexo', 'error');
    } else {

      const videoFilter = data.files.filter(tmp => this.getTypeByMimeType(tmp.type) === 'video');

      if (videoFilter.length > 0) {
        data.video_rendering = true;

        this.liveService.status(videoFilter[0].url).subscribe((res:any) => {
          data.video_rendering = !res.p480;
          this.publish(data);
        });
      } else {
        this.publish(data);
      }

    }

  }

  publish(data: any) {
    if (data.video_rendering) {
      swal('', 'O vídeo da sua publicação está sendo processado. Assim que estiver pronto, ele aparecerá na postagem', 'info');
    }

    this.angularFirestore.doc(`feed/${this.data.id}`).update(data);

    this.form.reset();
    this.resetFilePicker();
    this.dialogRef.close({id: this.data.id, ...data});
  }

  checkIfHasEmbededVideo(value: string): any {
    if (value) {
      if (value.includes('vimeo')) {
        return {url: '//player.vimeo.com/video/' + value.split('/')[4]};
      } else if (value.includes('youtube')) {
        return {url: '//www.youtube.com/embed/' + value.split('=')[1]};
      } else {
        return value;
      }
    }
  }

  getTypeByMimeType(mime: string): string {
    const split = mime.split('/');
    console.log(split[0])
    return split[0];
  }

  resetFilePicker() {
    this.uploadedFileType = null;
    this.uploadedFileUrl = null;
    this.form.patchValue({
      'embed': '',
      'embedTipo': '',
      'image': ''
    });
    this.isFile = false;
  }

  removeTmpFile(file: any): void {
    console.log('removeTmpFile: file', file);
    const index = this.tmpFilesArray.indexOf(file);
    this.tmpFilesArray.splice(index, 1);
  }

  uploadFile($event: any) {
    const fileArray = $event.srcElement.files;
    const fileArrayLength = fileArray.length;

    for (let i = 0; i < fileArrayLength; i++) {
      const file = fileArray[i];

      const tmpFile = {
        file: file,
        type: this.getTypeByMimeType(file.type),
        status: 0,
        url: ''
      };

      let error = false;

      if (
        this.getTypeByMimeType(file.type) === 'video' &&
        this.tmpFilesArray.filter(
          (tmp: any) => tmp.type === 'video'
        ).length > 0
      ) {
        swal('Ops...', 'Você só pode enviar 1 vídeo por postagem', 'error');
        error = true;
      }

      if (
        this.getTypeByMimeType(file.type) === 'image' &&
        this.tmpFilesArray.filter(
          (tmp: any) => tmp.type === 'image'
        ).length > 8
      ) {
        swal(
          'Ops...',
          'Você só pode enviar até 9 imagens por postagem',
          'error'
        );
        error = true;
      }

      if (!error) {
        this.uploading = true;

        this.tmpFilesArray.push(tmpFile);

        if (this.getTypeByMimeType(fileArray[i].type) === 'image') {
          this.ng2ImgMaxService.resize([file], 800, 800).subscribe((result: any) => {
            tmpFile.file = result;
            this.uploadImage(tmpFile);
          });
        }
        if (this.getTypeByMimeType(fileArray[i].type) === 'video') {
          this.uploadVideo(tmpFile);
        }
      }
    }
  }

  uploadImage(tmpFile: any) {
    const file = tmpFile.file;
    const newName =
      Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, '')
        .substr(0, 10) +
      '_' +
      file.name;
    const storageRef = this.firebase
      .storage()
      .ref()
      .child('images/' + newName);
    const tarefa = storageRef.put(file);
    tarefa.on(
      'state_changed',
      snapshot => {
        const percent = Math.round(
          (tarefa.snapshot.bytesTransferred / tarefa.snapshot.totalBytes) * 100
        );
        tmpFile.status = percent;
      },
      error => {},
      () => {
        if (tarefa.snapshot.state === 'success') {
          storageRef.getDownloadURL().then(url => {
            tmpFile.url = url;
            this.uploading = this.checkUploading();
          });
        }
      }
    );
  }

  uploadVideo(tmpFile: any) {
    const item = tmpFile.file;

    this.liveService.uploadFile(item).subscribe(
      res => {
        if (res['loaded']) {
          tmpFile.status = ((res['loaded'] * 100) / res['total']).toFixed(0);
        }

        if (res['body']) {
          tmpFile.url = res['body']['uuidVideo'];
          this.uploading = this.checkUploading();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  checkUploading(): boolean {
    const tmpFileWithoutUrl = this.tmpFilesArray.filter((tmp: any) => tmp.url === '');
    return (tmpFileWithoutUrl.length > 0) ? true : false;
  }

  getVideoThumb(url: string): string {
    let thumb: string;
    if (this.isUrl) {
      thumb = this.getVideoId(url);
    } else {
      thumb = url;
    }
    return this.streamVideoPath + '/videos/thumb/' + thumb;
  }

  getVideoUri(url: string): SafeResourceUrl {
    if (this.isUrl(url)) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.streamVideoPath + '/videos/' + url + '/480');
  }

  getVideoId(url: string): string {
    const split = url.split('/');
    return split[4];
  }

  isUrl(s: string): boolean {
    const regexp = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    const match = s.match(regexp);
    return regexp.test(s);
  }

}
