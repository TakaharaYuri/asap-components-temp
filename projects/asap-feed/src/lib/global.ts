import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { MatDialog } from '@angular/material';

@Injectable()
export class Global {

    
    constructor(
        public dialog: MatDialog,
        public notification: NotificationsService
    ) {
        
    }

    public clientID() {
        return sessionStorage.getItem("client");
    }

    public loggedUser() {
        let user:any = sessionStorage.getItem("loggedUser");
        if (user) {
            user = JSON.parse(user).data;
            return user;
        }
        return null;
    }

    public getTypeByMimeType(mime: string): string {
        const split = mime.split('/');
        return split[0];
    }

    public checkIfHasEmbededVideo(value: string): any {
        if (value) {
            if (value.includes('vimeo')) {
                return { url: '//player.vimeo.com/video/' + value.split('/')[4] };
            } 
            else if (value.includes('youtube')) {
                return { url: '//www.youtube.com/embed/' + value.split('=')[1] };
            } 
            else {
                return value;
            }
        }
    }

    public isUrl(s: string): boolean {
        const regexp = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
        return regexp.test(s);
    }

}
