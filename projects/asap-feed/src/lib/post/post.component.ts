import { MatDialog } from '@angular/material';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ModalLikesComponent } from '../modal-likes/modal-likes.component';
import { ModalContentComponent } from '../modal-content/modal-content.component';
import { ModalFeedComponent } from '../modal-feed/modal-feed.component';
import { Global } from '../global';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'app-post',
  templateUrl: './new-post.html',
  styleUrls: ['../asap-feed.component.scss','./new-post.scss'],
  providers: [ AngularFirestore ]
})

export class PostComponent implements OnInit, OnChanges {

  @Input() item: any;
  @Input() listBackup: any;

  public list: Observable<any>;
  public isDone: boolean;

  public pulse: string;

  public loadingMore: boolean;
  public noMore: boolean;
  public doc:any;
  public loggedUser: any;
  public loading = false;
  public disabledLike = false;

  constructor(
    public db: AngularFirestore,
    public global: Global,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit() {

  }

  ngOnChanges(change:SimpleChanges) {
    if(change.item) {
      this.loading = false;
      console.log('ITEM', change.item);
    }
  }

  expandVideo(file: any): void {
    file.show = true;
  }

  addLike(data: any) {
    console.log('Data Like', data);
    this.db.collection('likes')
      .add({
        post_id: data.id,
        uid: this.global.loggedUser().id
      })
      .then(response => {
        this.db.collection(`likes`)
          .doc(response.id)
          .update({ id: response.id });
      });
  }

  showLikes(item: any): void {
    this.dialog.open(ModalLikesComponent, {
      data: this.getLikes(item.id),
      width: '400px'
    });
  }

  getLikes(id: any): Observable<any> {
    if (!id) {
      id = 1;
    }
    const starsRef = this.db.collection('likes', ref =>
      ref.where('post_id', '==', id)
    );

    console.log('GetLikes', starsRef);
    return starsRef.valueChanges();
  }

  deleteFeed(id: number) {
    Swal.fire({
      title: 'Você quer apagar o item?',
      text: 'Você não poderá reverter essa ação depois de confirmado',
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Sim",
      cancelButtonText: "Não"
    }).then(confirm => {
      if (confirm.value) {
        this.db
          .doc('feed/' + id)
          .delete()
          .then(response => {
            this.item.deleted = true;
          });
      }
    });
  }

  openUpdateModal(item: any) {
    const dialogRef = this.dialog.open(ModalContentComponent, {
      width: '650px',
      maxHeight: '80%',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // const find = this.listBackup.find(itemToFind => {
        //   return itemToFind.id === result.id;
        // });

        // const index = this.listBackup.indexOf(find);

        this.item.title = result.title;
        this.item.embedTipo = result.embedTipo;
        this.item.embed = result.embed;
        this.item.image = result.image;
        this.item.files = result.files;

        // this.listBackup[index] = find;
      }
    });
  }

  like(item: any) {
    this.disabledLike = true;

    if (item.like.isLiked) {
      item.like.isLiked = false;
      item.like.count = item.like.count - 1;
  
      this.db.doc('likes/' + item.like.id).delete();
    } else {
      this.pulse = 'pulse';

      item.like.isLiked = true;
      item.like.count = item.like.count + 1;
      this.addLike(item);
    }

    setTimeout(() => {
       this.disabledLike = false; 
    }, 800);
  }

  ExpandImg(image: any) {
    this.dialog.open(ModalFeedComponent, {
      panelClass: 'full-screen-dialog',
      data: { img: image }
    });
  }

  permitted(post) {
    if (post.permission) {
      if (post.permission.length > 0) {
        let result = false;
        let user = this.global.loggedUser();
        let extras = user.extras[0];
        for (const perm of post.permission) {
          if (extras[perm.field] == perm.value) {
            result = true;
            break;
          }
        }
        return result;
      }
    }
    return true;
  }

}