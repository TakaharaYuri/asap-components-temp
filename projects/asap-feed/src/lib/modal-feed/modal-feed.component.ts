import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-feed',
  templateUrl: 'modal-feed.component.html',
  styleUrls: ['../asap-feed.component.css'],
  providers: []
})
export class ModalFeedComponent implements OnInit {
  public image: string;

  constructor(
    public dialogRef: MatDialogRef<ModalFeedComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.image = this.data.img;
  }
}
