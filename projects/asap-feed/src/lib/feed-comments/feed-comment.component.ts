import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Global } from '../global';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-feed-comment',
  templateUrl: './feed-comment.component.html',
  styleUrls: ['../asap-feed.component.scss'],
})

export class FeedCommentComponent implements OnInit {

  @Input() data: any;

  public form: FormGroup;
  public loggedUser: any;
  public limitTo = 2;

  constructor(
    public db: AngularFirestore,
    public fb: FormBuilder,
    public global: Global
  ) {
    moment.locale('pt-br');

    this.form = this.fb.group({
      text: ['', Validators.minLength(1)]
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    const formModel = this.form.value;

    if (formModel.text === '') {
      return;
    }

    this.addMessage(this.data, formModel.text);

    this.form.reset();
  }

  addMessage(data: any, text: string) {
    console.log('addMessage', data, Date.now());
    const comment = {
      uid: parseInt(this.global.loggedUser().id, 10),
      updated_at: Date.now(),
      comment: text,
      likes:0,
      user_info: {
        id: this.global.loggedUser().id,
        name: this.global.loggedUser().name,
        picture: this.global.loggedUser().picture
      }
    };


    const commentsCollection = this.db.collection(
      'feed/' + data.id + '/comments'
    );

    commentsCollection.add(comment).then(r => {
      this.db.doc('feed/' + data.id + '/comments/' + r.id).update({ id: r.id });
    });

    // this.global.notification.success(null, 'Comentário enviado com Sucesso', {
    //   theClass:'bg-grey'
    // });
  }

  public doEdit(comment, event) {
    if (event.srcElement.value != "") {
      this.db.doc('feed/' + this.data.id + '/comments/' + comment.id).update({ comment: event.srcElement.value, edited:true, updated_at: Date.now()});
      // this.global.notification.success(null, 'Comentário editado com Sucesso', {
      //   theClass:'bg-grey'
      // });
    }
    comment.editing = false;
  }

  public willEdit(comment) {
    comment.editing = true;
  }

  public deleteComment(comment) {
    Swal.fire({
      title: "Remover?",
      text: "Você realmente deseja remover este comentário?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Sim",
      cancelButtonText: "Não"
    }).then(confirm => {
      if (confirm.value) {
        this.db.doc('feed/' + this.data.id + '/comments/' + comment.id).delete();
        let idx = this.data.comments.indexOf(comment);
        if (idx >= 0) {
          this.data.comments.splice(idx, 1);
        }
      }
    });
  }

  public likeComment(comment) {
    console.log('likeComment', comment);

    const like = {
      uid: parseInt(this.global.loggedUser().id, 10),
      updated_at: Date.now(),
      user_info: {
        id: this.global.loggedUser().id,
        name: this.global.loggedUser().name,
        picture: this.global.loggedUser().picture
      }
    };


    const commentsCollection = this.db.collection(
      'feed/' + this.data.id + '/comments/' + comment.id + '/likes'
    );

    commentsCollection.add(like).then(response => {
      this.db.doc('feed/' + this.data.id + '/comments/' + comment.id + '/likes/' + response.id).update({ id: response.id });
    });

  }

  public unlikeComment(comment) {
    console.log('unlikeComment', comment);


    const commentsCollection = this.db.collection('feed/' + this.data.id + '/comments/' + comment.id + '/likes',
      ref => ref.where('uid', '==', this.global.loggedUser().id));


      commentsCollection.valueChanges().subscribe((response: any) => {
        console.log('RESPONSE', response);
        if (response.length > 0) {
          commentsCollection.doc(response[0].id).delete();
        }
      });

      return comment.isLiked = false;

  }

  public loadMore() {
    this.limitTo = this.limitTo + 3;
  }
  
}
