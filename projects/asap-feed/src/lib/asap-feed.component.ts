import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Global } from './global';

declare var $: any;
declare var swal: any;



@Component({
  selector: 'asap-feed',
  templateUrl: './asap-feed.component.html',
  styleUrls: ['./asap-feed.component.scss'],
})

export class AsapFeedComponent implements OnInit, OnChanges {

  @Input() search: any;
  @Input() queryRef: any;
  @Input() queryFilter: any;
  @Input() limitTo = 2;
  @Input() debug: boolean = false;

  public list = [];
  public idList = [];
  public isDone: boolean;
  public listBackup: any[] = [];
  public page = 0;
  public pageComment = 0;
  public limitCommentTo = 3;
  public endList = false;
  public loadingFeed = false;

  public loadingMore: boolean;
  public noMore: boolean;

  constructor(
    public db: AngularFirestore,
    public global: Global
  ) {

  }

  ngOnInit() {
    this.getFeed();


    // Verificando vídeos processados para notificação
    // let notificationsCollection = this.db.collection('notification/' + this.global.loggedUser().id + '/messages', (ref) => {
    //   return ref.where('read','==', false).limit(1);
    // })

    // notificationsCollection.valueChanges().subscribe( (response: any) => {
    //   console.log('notificationsCollection response', response);

    //   if (response.length > 0) {

    //   }
    // });


  //   notificationsCollection.snapshotChanges().subscribe((response: any) => {
  //     if (response.length > 0) {
  //       let data = response[0].payload.doc.data();

  //       if (!data.read) {
  //         let id = response[0].payload.doc.ref.path;
  //         this.playNotification();

  //         this.global.notification.success(data.title, 'O seu vídeo foi processado e já está disponível no Feed', {
  //           timeOut:0,
  //           clickToClose: true,
  //           theClass:'bg-grey'
  //         });
          
  //         this.db.doc(id).delete();
  //       }
  //     }
  //   })
  }

  ngOnChanges(changes:SimpleChanges) {
    if (changes.search) {
      this.getFeed(this.search);
    }
  }

  filtered(data) {
    if (this.search != null) {
      this.endList = true;
      return (data.title.indexOf(this.search) >= 0)
    }
    return true;
  }


  public getFeed(search = null) {
      if (!this.loadingFeed) {
        this.loadingFeed = true;
        let feedCollection: AngularFirestoreCollection;

        if (this.page == 0) {
          feedCollection = this.db.collection('feed', ref => {
            if (this.queryRef) {
              ref = this.queryRef(ref);
            }

            if (search) {
              // ref.startAt(search).endAt(search);
            }

            return ref.orderBy('updated_at', 'desc').limit(this.limitTo);
          });
            
        }
        else {
          feedCollection = this.db.collection('feed', ref => {
            
            if (search) {
              // return ref.orderBy('title')
              //           .startAt(`%${search}%`)
              //           .endAt(`%${search}%`+"\uf8ff")
              //           .limit(this.limitTo);
            }

            if (this.queryRef) {
              ref = this.queryRef(ref);
            }

            return ref.orderBy('updated_at', 'desc').startAfter(this.page).limit(this.limitTo);
          });
            
        }
        
        feedCollection.valueChanges().subscribe((list:any) => {
            if (list.length < this.limitTo) this.endList = true;
            let items = 0;
            let filter = item => true;

            if (this.queryFilter) {
              filter = this.queryFilter;
            }
            
            let new_list = list.filter(item => {
                return (item.id) && (this.idList.indexOf(item.id) < 0) && this.permitted(item);
            }).map((item:any) => {
                this.idList.push(item.id);
                items ++;
                if ((this.page == 0) || (this.page > item.updated_at)) {
                    this.page = item.updated_at;
                };
                

                this.mountFeedItem(item);
                // if (items >= 5) {
                //   this.loadingFeed = false;
                // }

                return item;
            });

            this.list = this.list.concat(new_list).sort((a, b) => (b.updated_at - a.updated_at));
            this.loadingFeed = false;
        });
      }
  }

  permitted(post) {
    if (post.permission) {
      if (post.permission.length > 0) {
         let result = false;
         let user = this.global.loggedUser();
         if (user) {
            if (user.extras) {
               let extras = user.extras[0];
               for (const perm of post.permission) {
                  if (extras[perm.field] == perm.value) {
                    result = true;
                    break;
                  }
               }
            }
         }
         return result;
      }
    }
    return true;
  }

  mountFeedItem(item:any) {
    this.getComments(item.id).subscribe((comments) => {
      let feedKey = item.id;
      item.comments = comments;

      // console.log('Comments', item.comments);

      // item.comments.map((item: any) => { 

      //   this.getLikesComments(feedKey, item.id).subscribe(response => {
      //     item.users_likes = response;

      //     if (response.length > 0) {
      //       item.users_likes.map((subItem: any) => {
      //         if (subItem.user_info.id == this.global.loggedUser().id) {
      //           item.isLiked = true;
      //         }
      //       })
      //     }
      //   });
      //   return item
      // })
      
    });

    this.getLikes(item.id).subscribe(response => {
      if (!item.like) {
        item.like = {};
      }

      item.like.count = response.length;
      if (this.global.loggedUser()) {
        const find = response.find((item: any) => {
          return item.uid === this.global.loggedUser().id;
        });
  
        if (find) {
          item.like.isLiked = true;
          item.like.id = find.id;
        }
      }

    });
  }

  getComments(itemKey: any): Observable<any> {
    const starsRef = this.db
      .doc(`feed/${itemKey}`)
      .collection('comments', ref => ref.orderBy('updated_at', 'desc'));
      return starsRef.valueChanges();
  }

  getLikesComments(feedKey:any, commentKey: any): Observable<any>{
    const starsRef = this.db
      .doc(`feed/${feedKey}/comments/${commentKey}`)
      .collection('likes', ref => ref.orderBy('updated_at', 'desc'));
      return starsRef.valueChanges();
  }

  getLikes(id: any): Observable<any> {
    if (!id) {
      id = 1;
    }
    const starsRef = this.db.collection('likes', ref =>
      ref.where('post_id', '==', id)
    );

    window['like'] = starsRef;
    return starsRef.valueChanges();
  }

  playNotification() {
    let audio = new Audio();
    audio.src = "../../../assets/audio/notification.mp3";
    audio.load();
    audio.play();
  }
}
