import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { LiveService } from '../live.service';
import { FirebaseApp } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalFeedComponent } from '../modal-feed/modal-feed.component';
import { Global } from '../global';
import { AngularFireStorage } from '@angular/fire/storage';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['../asap-feed.component.scss'],
  providers: [ LiveService, Global, AngularFirestore ]
})

export class PublicationComponent implements OnInit {

  me: any;

  form: FormGroup;
  tmpFileArray: any;
  uploading: boolean;
  urlPreview: {
    link: string;
    image: string;
    site: string;
    title: string;
    description: string;
  };

  constructor(
    public global: Global,
    public sanitizer: DomSanitizer,
    public firebase: FirebaseApp,
    public liveService: LiveService,
    public formBuilder: FormBuilder,
    public db: AngularFirestore,
    public ng2ImgMaxService: Ng2ImgMaxService,
    public firebaseStorage: AngularFireStorage
  ) {
    this.tmpFileArray = [];
    this.form = this.formBuilder.group({
      texto: ''
    });
    this.uploading = false;
    this.form.get('texto').valueChanges.subscribe((texto: string) => {
      this.onTextChange(texto);
    });
  }

  ngOnInit() {
    this.getUserLogged();
  }

  onTextChange(texto: string): void {
    if (texto !== '' && this.isUrl(texto) && !this.urlPreview) {
      this.urlPreview = {
        link: texto,
        image: '',
        site: '',
        title: 'Carregando...',
        description: 'Carregando informações do link'
      };
      this.mountPreview(texto);
    }
  }

  getUserLogged(): any {
    if (this.global.loggedUser()) {
      this.me = {
        id: this.global.loggedUser().id,
        name: this.global.loggedUser().name,
        picture: this.global.loggedUser().picture
      };
    }

  }

  isUrl(s: string): boolean {
    const regexp = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    return regexp.test(s);
  }

  isYouTubeUrl(url: string): boolean {
    if (url !== undefined || url !== '') {
      const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
      const match = url.match(regExp);

      console.log(match);

      return match && match[2].length === 11;
    }
    return false;
  }

  mountPreview(url: string): void {
    this.uploading = true;
    // this.previewService.createResource({url: url}).subscribe(res => {
    //   const data = res._values;
    //   if (data.image && data.title) {
    //     this.urlPreview = {
    //       link: url,
    //       image: data.image,
    //       site: data.site_name,
    //       title: data.title,
    //       description: data.description
    //     };
    //   } else {
    //     this.urlPreview = null;
    //   }
    //   this.uploading = false;
    // }, error => {
    //   this.urlPreview = null;
    //   this.uploading = false;
    // });
  }

  removePreview() {
    this.urlPreview = null;
  }

  uploadFile($event: any) {
    const fileArray = $event.srcElement.files;
    const fileArrayLength = fileArray.length;

    for (let i = 0; i < fileArrayLength; i++) {
      const file = fileArray[i];
      const fileGenerated = window.URL.createObjectURL(file);
      const fileSanitized = this.sanitizer.bypassSecurityTrustUrl(
        fileGenerated
      );
      const tmpFile = {
        fileSanitized: fileSanitized,
        file: file,
        type: file.type,
        status: 0,
        url: ''
      };

      let error = false;

      if (
        this.getTypeByMimeType(file.type) === 'video' &&
        this.tmpFileArray.filter((tmp: any) => this.getTypeByMimeType(tmp.type) === 'video').length > 0
      ) {
        swal('Ops...', 'Você só pode enviar 1 vídeo por postagem', 'error');
        error = true;
      }

      if (
        this.getTypeByMimeType(file.type) === 'image' &&
        this.tmpFileArray.filter((tmp: any) => this.getTypeByMimeType(tmp.type) === 'image').length > 8
      ) {
        swal(
          'Ops...',
          'Você só pode enviar até 9 imagens por postagem',
          'error'
        );
        error = true;
      }

      if (!error) {
        this.uploading = true;

        this.tmpFileArray.push(tmpFile);

        if (this.getTypeByMimeType(fileArray[i].type) === 'image') {
          if (fileArray[i].type == 'image/gif') {
            this.uploadImage(tmpFile);
          }
          else {
            this.ng2ImgMaxService.resize([file], 800, 800).subscribe((result) => {
              const imageGenerated = window.URL.createObjectURL(result);
              const imageSanitized = this.sanitizer.bypassSecurityTrustUrl(imageGenerated);
  
              tmpFile.fileSanitized = imageSanitized;
              tmpFile.file = result;
  
              this.uploadImage(tmpFile);
            },
            
            (error:any) => {
              console.log('Erro aqui', error);
            });
          }
        }
        if (this.getTypeByMimeType(fileArray[i].type) === 'video') {
          this.uploadVideo(tmpFile);
        }
      }
    }
  }

  getTypeByMimeType(mime: string): string {
    const split = mime.split('/');
    return split[0];
  }

  checkUploading(): boolean {
    const tmpFileUploading = this.tmpFileArray.filter((tmp: any) => tmp.url === '');
    return (tmpFileUploading.length > 0) ? true : false;
  }

  uploadImage(tmpFile: any) {
    const file = tmpFile.file;
    const newName = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10) + '_' + file.name;
    const storageRef = this.firebaseStorage.storage.ref().child('images/' + newName);
    const tarefa = storageRef.put(file);

    tarefa.on(
      'state_changed',
      (snapshot: any) => {
        const percent = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100);
        tmpFile.status = percent;
      },
      error => {
        console.log(error);
      },
      () => {
        if (tarefa.snapshot.state === 'success') {
          storageRef.getDownloadURL().then(url => {
            tmpFile.url = url;
            this.uploading = this.checkUploading();
          });
        }
      }
    );
  }

  uploadVideo(tmpFile: any) {
    const item = tmpFile.file;

    this.liveService.uploadFile(item).subscribe(
      res => {
        if (res['loaded']) {
          tmpFile.status = ((res['loaded'] * 100) / res['total']).toFixed(0);
        }

        if (res['body']) {
          tmpFile.url = res['body']['uuidVideo'];
          this.uploading = this.checkUploading();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  removeTmpFile(tmpFile: any): void {
    const index = this.tmpFileArray.indexOf(tmpFile);
    this.tmpFileArray.splice(index, 1);
  }

  checkIfHasEmbededVideo(value: string): any {
    if (value) {
      if (value.includes('vimeo')) {
        return { url: '//player.vimeo.com/video/' + value.split('/')[4] };
      } else if (value.includes('youtube')) {
        return { url: '//www.youtube.com/embed/' + value.split('=')[1] };
      } else {
        return value;
      }
    }
  }

  onSubmit(): any {
    const formModel = this.form.value;
    this.getUserLogged();
    const data = {
      likes: 0,
      isLiked: false,
      user_info: this.me,
      updated_at: Date.now(),
      title: formModel.texto,
      embedTipo: '',
      embed: '',
      image: '',
      preview: null,
      files: [],
      video_rendering: false,
      video_low_rendering: false,
      thumb_rendering: false
    };

    const embed = this.checkIfHasEmbededVideo(formModel.texto);

    if (typeof embed === 'object') {
      data.embed = embed.url;
    }

    if (this.tmpFileArray.length > 0) {
      this.tmpFileArray.map(tmpFile => {
        data.files.push({
          type: this.getTypeByMimeType(tmpFile.type),
          url: tmpFile.url
        });
      });
    }

    if (this.urlPreview) {
      data.preview = this.urlPreview;
    }

    const videoFilter = data.files.filter(tmp => this.getTypeByMimeType(tmp.type) === 'video');

    if (videoFilter.length > 0) {
      data.video_rendering = true;
      data.video_low_rendering = true;
      data.thumb_rendering = true;

      this.liveService.status(videoFilter[0].url).subscribe((res:any) => {
        data.video_rendering = !res.p480;
        data.video_low_rendering = !res.p480_low;
        data.thumb_rendering = !res.thumb;

        this.publish(data);
      });
    } else {
      this.publish(data);
    }

  }

  publish(data: any) {
    const feedCollection = this.db.collection('feed');

    if ( data.title === '' && data.embed === '' && data.image === '' && data.files.length === 0 ) {
      swal('Ops...', 'Você precisa adicionar um texto, imagem ou vídeo.', 'error');
    } else {
      feedCollection.add(data).then(res => {
        this.db.doc(`feed/${res.id}`).update({ id: res.id });

        const imagesFiles = this.tmpFileArray.filter(
          (tmp: any) => this.getTypeByMimeType(tmp.type) === 'image'
        );

        let bigPicture = null;

        if (imagesFiles.length > 0) {
          bigPicture = imagesFiles[0].url;
        }

        // this._createService.createResource({
        //   big_picture: bigPicture,
        //   feed_id: res.id
        // }).subscribe(createNotificationRes => {
        //   console.log(createNotificationRes);
        // });
      });

      this.form.reset();
      this.tmpFileArray = [];
      this.urlPreview = null;
    }
  }

  ExpandImg(image: any) {
    this.global.dialog.open(ModalFeedComponent, {
      panelClass: 'full-screen-dialog',
      data: { img: image }
    });
  }

  isValid(): boolean {
    return !(this.form.value.texto === '' && this.tmpFileArray.length === 0);
  }

}
