import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapFeedComponent } from './asap-feed.component';

describe('AsapFeedComponent', () => {
  let component: AsapFeedComponent;
  let fixture: ComponentFixture<AsapFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
