import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Global } from '../global';

@Component({
  selector: 'app-single-post',
  templateUrl: './single.component.html',
  styleUrls: ['../../../app.component.css', '../feed.component.css'],
})

export class SingleComponent implements OnInit {
  loggedUser: any;

  id: string;

  list;
  isDone: boolean;
  listBackup: any[] = [];

  loadingMore: boolean;
  noMore: boolean;

  constructor(
    public global: Global,
    public db: AngularFirestore,
    public dialog: MatDialog,
    public _activatedRoute: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(param => {
      this.id = param['id'];
      this.getFeed();
    });
  }

  getFeed() {
    const feedCollection = this.db.collection('feed', ref =>
      ref.where('id', '==', this.id)
    );

    feedCollection.stateChanges(['added', 'modified', 'removed']).subscribe(actions => {
      this.list = actions.map(action => {
        const data = action.payload.doc.data();
        const id = action.payload.doc.id;

        const find = this.listBackup.find(itemToFind => {
          return itemToFind.id === id;
        });

        const item = this.mountFeedItem(id, data);

        if (!find) {
          if (this.isDone) {
            this.listBackup.unshift(item);
          } else if (!this.isDone) {
            this.listBackup.push(item);
          }
        } else {
          const index = this.listBackup.indexOf(find);
          this.listBackup[index] = item;
        }
      });

      this.isDone = true;

      return this.listBackup;
    });
  }

  mountFeedItem(id: any, data: any) {
    data.comments = [];
    data.like = {
      count: 0,
      isLiked: false,
      id: null
    };

    this.getLikes(id).subscribe(res => {
      data.like.count = res.length;

      const find = res.find((item: any) => {
        return (this.global.loggedUser()) && (item.uid === this.global.loggedUser().id);
      });

      if (find) {
        data.like.isLiked = true;
        data.like.id = find.id;
      }
    });

    //this.authService.userInfo(data.user_info.id).subscribe(user => {
      data.user_info.name = this.global.loggedUser().name;
      data.user_info.picture = this.global.loggedUser().picture;
    //});

    this.getComments(id).subscribe(comments => {
      comments.map((comm: any) => {
        if (!comm.id) {
          return;
        }

        const find = data.comments.find(item => {
          return item.id === comm.id;
        });

        if (!find) {
          comm.user_info = {
            name: null,
            picture: null
          };

          //this.authService.userInfo(comm.uid).subscribe(user_comment => {
            comm.user_info.name = this.global.loggedUser().name;
            comm.user_info.picture = this.global.loggedUser().picture;
            data.comments.push(comm);
          //});
        } else {
          find.comment = comm.comment;
        }
      });
    });

    return { id, ...data };
  }

  getComments(itemKey: any): Observable<any> {
    const starsRef = this.db
      .doc(`feed/${itemKey}`)
      .collection('comments', ref => ref.orderBy('updated_at', 'desc'));
    return starsRef.valueChanges();
  }

  getLikes(id: any): Observable<any> {
    if (!id) {
      id = 1;
    }
    const starsRef = this.db.collection('likes', ref =>
      ref.where('post_id', '==', id)
    );
    return starsRef.valueChanges();
  }

}
