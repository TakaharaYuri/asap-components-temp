import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LiveService {
    
    public streamVideoPath: string;

    constructor(public http: HttpClient) {
        if (sessionStorage.getItem('loggedUser')) {
            const lu = JSON.parse(sessionStorage.getItem('loggedUser'));
            this.streamVideoPath = lu.data.link;
        }
    }

    private getHeaders() {
        let headers = {
            'Accept': 'application/json'
        };
        if (sessionStorage.getItem('token')) {
            headers['Authorization'] = sessionStorage.getItem('token');
        }
        return headers;
    }

    public status(id: string) {
        return this.http.get(this.streamVideoPath + 'videos/' + id, {headers: this.getHeaders()});
    }

    public uploadFile(data: any) {
        const formData: FormData = new FormData();
        formData.append('file', data);

        const req = new HttpRequest('POST', this.streamVideoPath + 'videos/upload', formData, {
            reportProgress: true,
            responseType: 'json',
            headers: new HttpHeaders(this.getHeaders())
        });

        return this.http.request(req);
    }

}
