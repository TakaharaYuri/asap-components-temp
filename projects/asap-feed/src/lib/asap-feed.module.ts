import { NgModule } from '@angular/core';
import { AsapFeedComponent } from './asap-feed.component';
import { PostComponent } from './post/post.component';
import { ModalLikesComponent } from './modal-likes/modal-likes.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { ModalFeedComponent } from './modal-feed/modal-feed.component';
import { PublicationComponent } from './publication/publication.component';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatMenuModule, MatIconModule, MatListModule, MatDialogModule, MatButtonModule, MatCardModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { FeedCommentComponent } from './feed-comments/feed-comment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StreamPipe } from './stream.pipe';
import { MomentModule } from 'angular2-moment';
import { Global } from './global';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import {MatGridListModule} from '@angular/material/grid-list';
import {SlideshowModule} from 'ng-simple-slideshow';





@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatListModule,
        MatCardModule,
        MatDialogModule,
        MatGridListModule,
        InfiniteScrollModule,
        MomentModule,
        Ng2ImgMaxModule,
        SlideshowModule
    ],
    declarations: [
        StreamPipe,
        AsapFeedComponent,
        FeedCommentComponent,
        PostComponent,
        ModalLikesComponent,
        ModalContentComponent,
        ModalFeedComponent,
        PublicationComponent
    ],
    providers: [
        Global
    ],
    entryComponents:[
        ModalFeedComponent,
        ModalContentComponent
    ],
    exports: [
        AsapFeedComponent
    ]
})
export class AsapFeedModule { }
