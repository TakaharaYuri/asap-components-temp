import { TestBed } from '@angular/core/testing';

import { AsapFeedService } from './asap-feed.service';

describe('AsapFeedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapFeedService = TestBed.get(AsapFeedService);
    expect(service).toBeTruthy();
  });
});
