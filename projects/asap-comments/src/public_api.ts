/*
 * Public API Surface of asap-comments
 */

export * from './lib/asap-comments.component';
export * from './lib/asap-comments.module';
