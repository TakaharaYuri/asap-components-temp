import { Component, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ChatFirebaseService } from './chat-firebase/chat-firebase.service';

@Component({
    selector: 'asap-comments',
    templateUrl: './chat-firebase/chat-firebase.component.html',
    styleUrls: ['./chat-firebase/chat-firebase.component.css'],
    providers: [ ChatFirebaseService ]
})
export class AsapCommentsComponent implements OnChanges {

    @Input() owner;
    @Input() chatId;
    @Input() user;
    @Output() onMessages = new EventEmitter();

    public loggedUser = null;
    public chatData = null;
    public message = "";
    public answer = "";
    public messages = null;
    public snapShot = null;
    public loading = true;


    constructor(
        public chatService: ChatFirebaseService
    ) {
        
    }

    ngOnInit() {
        this.loggedUser = this.chatService.loggedUser();
    }

    ngOnDestroy() {
        if (this.snapShot) {
            this.snapShot();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.user) {
            this.chatService.setUser(this.user);
        }
        if ((changes.chatId) && (this.chatId != "")) {
            if (changes.chatId.currentValue != changes.chatId.previousValue) {
                this.chatService.openChatRoomById(this.chatId, (chatData) => {
                    this.chatData = chatData;
                    this.messages = [];
                    this.snapShot = this.chatData.messages.onSnapshot((data) => {
                        console.log("Firebase Data:",data);
                        data.docChanges().forEach((change) => {
                            this.loading = false;
                            let answers = change.doc.ref.collection("answers");
                            change.doc.answers = [];
                            answers.orderBy("date","desc").onSnapshot((answers) => {
                                answers.docChanges().forEach((answer) => {
                                    change.doc.answers.push(answer.doc);
                                });
                            });
                            this.messages.push(change.doc);
                            this.onMessages.emit(change.doc);
                            //console.log(change.type,change.doc.data());
                            setTimeout(() => {
                                //$(".chat-room .h-full").scrollTop($(".chat-room .h-full")[0].scrollHeight);
                            },100);
                        });
                    });
                });
            }
        }
        if (changes.owner) {
            if (changes.owner.currentValue != changes.owner.previousValue) {
                this.chatData = null;
                this.chatService.openChatRoom(this.owner, (chatData) => {
                    this.chatData = chatData;
                    this.messages = [];
                    this.snapShot = this.chatData.messages.onSnapshot((data) => {
                        console.log("Firebase Data:",data);
                        this.loading = false;
                        data.docChanges().forEach((change) => {
                            let answers = change.doc.ref.collection("answers");
                            change.doc.answers = [];
                            answers.orderBy("date","desc").onSnapshot((answers) => {
                                answers.docChanges().forEach((answer) => {
                                    change.doc.answers.push(answer.doc);
                                    //console.log("Answer",answer);
                                });
                            });
                            this.messages.push(change.doc);
                            this.onMessages.emit(change.doc);
                            //console.log(change.type,change.doc.data());
                            setTimeout(() => {
                                //$(".chat-room .h-full").scrollTop($(".chat-room .h-full")[0].scrollHeight);
                            },100);
                        });
                    });
                });
            }   
        }     
    }

    onKeydown(event) {
        if (event.key === "Enter") {
            this.sendMessage();
        }
    }

    public sendMessage() {
        if (this.message != "") {
            this.chatService.sendMessage(this.chatData.messagesCollection,this.message);
            this.message = "";
        }
    }

    public sendAnswer(message) {
        if (this.answer != "") {
            this.chatService.sendAnswer(message,this.answer);
            this.answer = "";
            message.answering = false;
        }
    }

}