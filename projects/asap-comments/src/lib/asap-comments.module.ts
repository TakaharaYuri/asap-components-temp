import { NgModule } from '@angular/core';
import { AsapCommentsComponent } from './asap-comments.component';
import { ChatFirebaseService } from './chat-firebase/chat-firebase.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    AsapCommentsComponent
  ],
  exports: [
    AsapCommentsComponent
  ],
  providers: [

  ]
})
export class AsapCommentsModule { 

    static forRoot(firebase, apiURL) {
      ChatFirebaseService.firebaseKey = firebase;
      ChatFirebaseService.apiURL = apiURL;
      return {
        ngModule:AsapCommentsModule, 
      };
    }

}
