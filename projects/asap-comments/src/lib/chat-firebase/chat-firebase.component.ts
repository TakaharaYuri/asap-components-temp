import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ChatFirebaseService } from './chat-firebase.service';

declare var $:any;

@Component({
    selector: 'app-chat-firebase',
    templateUrl: './chat-firebase.component.html',
    styleUrls: ['./chat-firebase.component.scss'],
    providers: [ ChatFirebaseService ]
})

export class ChatFirebaseComponent implements OnChanges {

    @Input() owner;
    @Input() chatId;
    @Output() onMessages = new EventEmitter();

    public loggedUser = null;
    public chatData = null;
    public message = "";
    public answer = "";
    public messages = null;
    public snapShot = null;
    public loading = true;


    constructor(
        public chatService: ChatFirebaseService
    ) {
        
    }

    ngOnInit() {
        this.loggedUser = JSON.parse( sessionStorage.getItem('loggedUser') ).data;
    }

    ngOnDestroy() {
        if (this.snapShot) {
            this.snapShot();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        //console.log("ngOnChanges",changes);
        if ((changes.chatId) && (this.chatId != "")) {
            if (changes.chatId.currentValue != changes.chatId.previousValue) {
                this.chatService.openChatRoomById(this.chatId, (chatData) => {
                    this.chatData = chatData;
                    this.messages = [];
                    this.snapShot = this.chatData.messages.onSnapshot((data) => {
                        console.log("Firebase Data:",data);
                        data.docChanges.forEach((change) => {
                            this.loading = false;
                            let answers = change.doc.ref.collection("answers");
                            change.doc.answers = [];
                            answers.orderBy("date","desc").onSnapshot((answers) => {
                                answers.docChanges.forEach((answer) => {
                                    change.doc.answers.push(answer.doc);
                                });
                            });
                            this.messages.push(change.doc);
                            this.onMessages.emit(change.doc);
                            //console.log(change.type,change.doc.data());
                            setTimeout(() => {
                                $(".chat-room .h-full").scrollTop($(".chat-room .h-full")[0].scrollHeight);
                            },100);
                        });
                    });
                });
            }
        }
        if (changes.owner) {
            if (changes.owner.currentValue != changes.owner.previousValue) {
                this.chatData = null;
                this.chatService.openChatRoom(this.owner, (chatData) => {
                    this.chatData = chatData;
                    this.messages = [];
                    this.snapShot = this.chatData.messages.onSnapshot((data) => {
                        console.log("Firebase Data:",data);
                        this.loading = false;
                        data.docChanges.forEach((change) => {
                            let answers = change.doc.ref.collection("answers");
                            change.doc.answers = [];
                            answers.orderBy("date","desc").onSnapshot((answers) => {
                                answers.docChanges.forEach((answer) => {
                                    change.doc.answers.push(answer.doc);
                                    //console.log("Answer",answer);
                                });
                            });
                            this.messages.push(change.doc);
                            this.onMessages.emit(change.doc);
                            //console.log(change.type,change.doc.data());
                            setTimeout(() => {
                                $(".chat-room .h-full").scrollTop($(".chat-room .h-full")[0].scrollHeight);
                            },100);
                        });
                    });
                });
            }   
        }     
    }

    onKeydown(event) {
        if (event.key === "Enter") {
            this.sendMessage();
        }
    }

    public sendMessage() {
        if (this.message != "") {
            this.chatService.sendMessage(this.chatData.messagesCollection,this.message);
            this.message = "";
        }
    }

    public sendAnswer(message) {
        if (this.answer != "") {
            this.chatService.sendAnswer(message,this.answer);
            this.answer = "";
            message.answering = false;
        }
    }




}
