import { Injectable } from '@angular/core';
import { _firebaseAppFactory } from '@angular/fire/firebase.app.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ChatFirebaseService {

    public static firebaseKey = {};
    public static apiURL = "";

    public user;
    public firebase:any;

    protected options() {
        let headers;
        let token = sessionStorage.getItem("token");
        if (token) {
            headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': token,
                'Access-Control-Allow-Origin': '*'
            });
        }
        else {
            headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            });
        }
        return {
            headers: headers
        }
    };

    constructor(
        public http: HttpClient
    ) {
        let fb = _firebaseAppFactory(this.getFirebase(),"FireOther");
        this.firebase = fb.firestore();
    }

    private clientID() {
        let client = sessionStorage.getItem("client");
        if (client) {
            return "?client_id=" + client;
        }
        return "";
    }

    private findUser(user_id):any {
       return this.http.get(ChatFirebaseService.apiURL + "/api/v2/admin/asap-user/dados/" + user_id + this.clientID(),this.options());
    }

    private getFirebase() {
        console.log("Firebasekey",ChatFirebaseService.firebaseKey);
        return ChatFirebaseService.firebaseKey;
    }

    public newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    public setUser(user) {
        this.user = user;
    }

    public loggedUser() {
        if (this.user) {
            return this.user;
        }
        else {
            let user = sessionStorage.getItem('loggedUser');
            if (user) {
                return JSON.parse(user).data;
            }
        }
        return null;
    }

    public findChat(owner):any {
        let ownerData = JSON.stringify(owner);
        return this.http.post(ChatFirebaseService.apiURL + '/api/v2/client/chat/find' + this.clientID(),{ owner:ownerData },this.options());
    }

    public registerChat(owner):any {
        let ownerData = JSON.stringify(owner);
        let ownerKey = this.newGuid();
        return this.http.post(ChatFirebaseService.apiURL + '/api/v2/client/chat/register' + this.clientID(),{ chat_id:ownerKey,owner:ownerData },this.options());
    }

    public openChatRoomById(chatId,callback=null) {
        let messagesCollection = this.firebase.collection('chatroom');
        let chatDoc = messagesCollection.doc(chatId);
        messagesCollection = chatDoc.collection("chat"); 
        let messages = messagesCollection.orderBy("date","desc");
        if (callback) {
            callback({
                messagesCollection: messagesCollection,
                messages: messages
            });
        }
    }

    public openChatRoom(owner,callback=null) {
        let loggedUser = this.loggedUser();
        this.findChat(owner).subscribe((response:any) => {
            //console.log("Find:",response);
            let data = response;
            if (data.ok) {
                let chatRoom = data.chat.chat_id;
                let messagesCollection = this.firebase.collection('chatroom');
                let chatDoc = messagesCollection.doc(chatRoom);
                messagesCollection = chatDoc.collection("chat"); 
                messagesCollection.add({
                    started_at: new Date(),
                    type:"data",
                    owner: owner
                });
                let messages = messagesCollection.orderBy("date","desc");
                if (callback) {
                    callback({
                        messagesCollection: messagesCollection,
                        messages: messages
                    });
                }
            }
            else {
                this.registerChat(owner).subscribe((register:any) => {
                    let chatRoom = register.chat.chat_id;
                    let messagesCollection = this.firebase.collection('chatroom');
                    let chatDoc = messagesCollection.doc(chatRoom);
                    owner.id = chatRoom;
                    if (loggedUser) {
                        owner.users = [{
                            user_id: loggedUser.id,
                            name: loggedUser.name,
                            picture: loggedUser.picture
                        }];
                    }
                    if (owner.user_id) {
                        this.findUser(owner.user_id).subscribe((response:any) => {
                            response = response.json()[0];
                            //console.log("findUser:Response",response);
                            owner.users = [{
                                user_id: response.id,
                                name: response.name,
                                picture: response.picture
                            }];
                            //console.log("findUser",owner);
                            chatDoc.update(owner);
                        });
                    }
                    chatDoc.set(owner);
                    messagesCollection = chatDoc.collection("chat");
                    messagesCollection.add({
                        date: new Date(),
                        type:"data",
                        owner: owner
                    });
                    let messages = messagesCollection.orderBy("date","desc");
                    if (callback) {
                        callback({
                            messagesCollection: messagesCollection,
                            messages: messages
                        });
                    }
                });
            }
        });
    }

    public sendMessage(chatRoom, message) {
        let loggedUser = this.loggedUser();
        let messageData = {
            type: "message",
            date: new Date(),
            message: message,
            user: {
                user_id: loggedUser.id,
                name: loggedUser.name,
                picture: loggedUser.picture
            }
        }
        chatRoom.parent.get().then((doc) => {
            let channel = doc.data();
            channel.lastMessage = messageData;
            if (channel.users) {
                if (channel.users.indexOf(messageData.user) < 0) {
                    channel.users.push(messageData.user);
                }
            }
            else {
                channel.users = [messageData.user];
            }
            doc.ref.set(channel);
        });
        //console.log("SendMessage:",messageData);
        chatRoom.add(messageData);
    }

    public sendAnswer(message, answer) {
        console.log("Answer to:", message);
        window["answer"] = message;
        let answers = message.ref.collection("answers");
        let loggedUser = this.loggedUser();
        let messageData = {
            type: "message",
            date: new Date(),
            message: answer,
            user: {
                user_id: loggedUser.id,
                name: loggedUser.name,
                picture: loggedUser.picture
            }
        }
        answers.add(messageData);
    }

}
