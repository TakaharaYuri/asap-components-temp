import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapCommentsComponent } from './asap-comments.component';

describe('AsapCommentsComponent', () => {
  let component: AsapCommentsComponent;
  let fixture: ComponentFixture<AsapCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
