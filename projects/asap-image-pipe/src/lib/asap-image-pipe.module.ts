import { NgModule } from '@angular/core';
import { AsapImagePipe } from './asap-image-pipe.component';

@NgModule({  
  imports: [

  ],
  declarations: [
      AsapImagePipe
  ],
  exports: [
      AsapImagePipe
  ]
})
export class AsapImagePipeModule { }
