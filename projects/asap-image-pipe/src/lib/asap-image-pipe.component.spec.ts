import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsapImagePipeComponent } from './asap-image-pipe.component';

describe('AsapImagePipeComponent', () => {
  let component: AsapImagePipeComponent;
  let fixture: ComponentFixture<AsapImagePipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsapImagePipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsapImagePipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
