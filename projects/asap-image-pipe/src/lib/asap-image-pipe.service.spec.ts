import { TestBed } from '@angular/core/testing';

import { AsapImagePipeService } from './asap-image-pipe.service';

describe('AsapImagePipeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsapImagePipeService = TestBed.get(AsapImagePipeService);
    expect(service).toBeTruthy();
  });
});
