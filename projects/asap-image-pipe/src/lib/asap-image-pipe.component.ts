import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({name: 'asapImagePipe'})
export class AsapImagePipe implements PipeTransform {
    constructor(private domSanitizer: DomSanitizer) {}

    transform(value: any, command: any, width: any): any {
        if ((value) && (value.toUpperCase() != "NULL")) {
            if (command) {
                if (command == 'resize') {
                    if (width) {
                        value = encodeURI(value);
                        return 'https://apiv3.plataformaasap.com.br/api/image/resize/'+width+'?image=' + value; 
                    }
                }
            }
        }
        return null;
    }
}

